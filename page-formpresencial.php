<?php /* Template Name: Página Cadastro Presencial */ 
get_template_part('templates/html', 'header'); ?>

<style>
    .mq-page--politica {
        padding-top: 126px;
    }

    .mq-politicas {
        margin: 60px 0px;
    }

    .mq-politicas-texto {
        width: 90%;
        margin: 0 auto;
        background: #fff;
        padding: 40px;
        border-radius: 4px;
    }

    .mq-politicas-texto h2,
    h3 {
        margin-bottom: 30px;
        font-family: 'Arial', sans-serif;
        color: #6e6f72;
    }

    .mq-politicas-texto ol {
        padding-left: 16px;
    }

    .mq-politicas-texto ol li::marker {
        font-family: 'Arial', sans-serif;
        color: #6e6f72;
        font-size: 14px;
    }

    .mq-politicas-texto ol li b,
    .mq-politicas-texto ul li b,
    .mq-politicas-texto ul li span {
        font-family: 'Arial', sans-serif;
        color: #6e6f72;
        font-size: 14px;
    }

    .mq-politicas-texto p {
        margin: 10px 0px 15px;
    font-family: 'Arial', sans-serif;
    color: #6e6f72;
    font-size: 22px;
    text-transform: uppercase;
    }

    h2.mq-header-localizacao__title {
        color: #ec6433;
    text-transform: uppercase;
    }

    div#hcrmAlertBox {
        display: none;
    }

    @media screen and (max-width: 640px) {
        .mq-header-localizacao {
            margin-bottom: 1.75rem;
        }
    }

    @media screen and (max-width: 480px) {


        .mq-page--politica {
            background: #f04f23;
            padding-top: 56px;
        }

        .mq-politicas-texto {
            width: 100%;
            padding: 20px;
        }

        .mq-politicas-texto h2,
        h3 {
            font-size: 21px;
            margin-bottom: 15px;
        }

    }
</style>

<?php while (have_posts()) : the_post(); ?>




    <div class="mq-page mq-page--politica">

        <section class="mq-politicas">
            <div class="container">

                <div class="mq-politicas-texto">
                    <header class="mq-header-localizacao">
                        <h2 class="mq-header-localizacao__title" style="margin-bottom: 0;">Inicie seu atendimento</h2>
                        <p class="mq-header-localizacao__end">dados cadastrais</p>

                    </header>
                    <div class="mq-geo__form form-pp">
                        <?php the_content(); ?>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <style>
        .form-pp {
            margin: 0 auto !important;
        }
    </style>
<?php endwhile; ?>
<?php get_template_part('templates/html', 'footer'); ?>