<?php /* Template Name: Vendas Terreno */ ?>
<?php get_template_part('templates/html', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>

    <div class="mq-page">

        <?php get_template_part('templates/contato', 'banner'); ?>

        
        <?php get_template_part('templates/contato', 'opcoes'); ?>


        <?php get_template_part('templates/venda-terreno', 'form'); ?>


        <?php get_template_part('templates/contato', 'informacoes'); ?>

    </div>


<?php endwhile;
wp_reset_postdata(); ?>
<?php get_template_part('templates/html', 'footer'); ?>