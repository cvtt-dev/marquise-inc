<?php /* Template Name: Página Termos */ ?>
<?php get_template_part('templates/html','header'); ?>

<style>
    .mq-page--termos {padding-top: 126px;}
    .mq-termos {margin:60px 0px;}
    .mq-termos-texto {width: 90%;margin: 0 auto;background: #fff; padding: 40px; border-radius: 4px;}
    .mq-termos-texto h2, h3 {margin-bottom: 20px;font-family: 'Arial',sans-serif;color: #6e6f72;}
    .mq-termos-texto ol {padding-left: 16px;}
    .mq-termos-texto ol li::marker{font-family: 'Arial',sans-serif;color: #6e6f72;font-size: 14px;}
    .mq-termos-texto ol li b, .mq-termos-texto ul li b, .mq-termos-texto ul li span {font-family: 'Arial',sans-serif;color: #6e6f72;font-size: 14px;}
    .mq-termos-texto p {margin: 15px 0px; font-family: 'Arial',sans-serif;color: #6e6f72;font-size: 14px;}

    @media screen and (max-width: 480px) {
    
        .mq-page--termos {
            background: #f04f23;
            padding-top: 56px;
        }

        .mq-termos-texto {
            width: 100%;
            padding: 20px;
        }

        .mq-termos-texto h2, h3 {
            font-size: 21px;
            margin-bottom: 15px;
        }

        .mq-termos-texto p {
            display: flex;
            flex-wrap: wrap;
        }

    }
</style>

<?php while (have_posts()) : the_post(); ?>

<div class="mq-page mq-page--termos">
    <section class="mq-termos">
        <div class="container">
            <div class="mq-termos-texto">
                <?php the_content(); ?>                    
            </div>
        </div> 
    </section>
</div>
<?php endwhile; ?>
<?php get_template_part('templates/html','footer');?>