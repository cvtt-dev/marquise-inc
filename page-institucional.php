<?php /* Template Name: Institucional */ ?>
<?php get_template_part('templates/html','header');?>

<?php while (have_posts()) :the_post(); ?>

<div class="mq-page">

	<?php get_template_part('templates/institucional','banner');?>
	
	<?php get_template_part('templates/institucional','sobre');?>
	
	<?php get_template_part('templates/institucional','certificacoes');?>

	<?php get_template_part('templates/institucional','objetivos');?>
	
</div>

<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>