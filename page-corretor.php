<?php /* Template Name: Corretor */ ?>
<?php get_template_part('templates/html', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>

	<div class="mq-page">

		<?php get_template_part('templates/corretor', 'banner'); ?>
	<?php endwhile;
wp_reset_postdata(); ?>

	<?php
	$destaques = get_post_meta(get_the_id(), 'corretor-destaques-group', true);
	$vantagens = get_post_meta(get_the_id(), 'corretor-vantagens-group', true);
	$materiais = get_post_meta(get_the_id(), 'corretor-materiais-group', true);
	$links = get_post_meta(get_the_id(), 'corretor-links-group', true);

	?>



	<section class="mq-section mq-corretor__vantegens">
		<div class="container">
			<header class="mq-certificacoes__header">
				<h2 class="title">Vantagens de Vender Marquise</h2>
			</header>
			<div class="grid">

				<?php
				foreach ($vantagens as $item) : ?>

					<div class="item">
						<img class="icone" src="<?php echo wp_get_attachment_url($item['corretor-vantagens-icon'][0]); ?>" alt="">
						<h3 class="title">
							<?php echo $item['corretor-vantagens-titulo']; ?>
						</h3>
						<!-- <p class="content">
							<?php //echo $item['corretor-vantagens-titulo']; 
							?>
						</p> -->
					</div>


				<?php endforeach; ?>
			</div>




		</div>
	</section>




	<section class="mq-section mq-corretor__destaques">
		<div class="container">
			<header class="mq-certificacoes__header mq-certificacoes__header--center">
				<h2 class="title">Empreendimentos em Destaque</h2>
			</header>
			<div class="wrap-slide">


				<?php foreach ($destaques as $item) : ?>



					<a href="<?php echo $item['corretor-destaques-url']; ?>" target="<?php echo $item['corretor-destaques-target']; ?>">
						<figure>
							<img src="<?php echo wp_get_attachment_url($item['corretor-destaques-capa'][0]) ?>" alt="">
						</figure>
					</a>



				<?php endforeach; ?>
			</div>
		</div>
	</section>





	<!-- 

	<section class="mq-section mq-contato-opcoes">
		<div class="container">
			<header class="mq-certificacoes__header">
				<h2 class="title">Links Importantes</h2>
			</header>
			<div class="mq-geo-options">

				<?php
				foreach ($links as $item) : ?>



					<a href="<?php echo $item['corretor-links-url']; ?>" target="<?php echo $item['corretor-links-target']; ?>" class="mq-geo-options__link">
						<div class="mq-geo-options__item links-area-do-corretor">
							<img src="<?php echo wp_get_attachment_url($item['corretor-links-icon'][0]); ?>" alt="Trabalhe conosco" title="Trabalhe conosco">
							<h3 class="title"><?php echo $item['corretor-links-title']; ?></h3>
						</div>
					</a>

				<?php endforeach; ?>




			</div>
		</div>
	</section> -->



	<section class="mq-section mq-corretor__vantegens mq-corretor__material">
		<div class="container">
			<header class="mq-certificacoes__header mq-certificacoes__header--center">
				<h2 class="title">Cadastre-se e tenha acesso a materiais exclusivos e novidades em primeira mão</h2>
			</header>
			<div class="grid">

				<?php
				foreach ($materiais as $item) : ?>

					<div class="item">
						<img class="icone" src="<?php echo wp_get_attachment_url($item['corretor-materiais-icon'][0]); ?>" alt="">
						<h3 class="title">
							<?php echo $item['corretor-materiais-titulo']; ?>
						</h3>
						<p class="content">
							<?php echo $item['corretor-materiais-content']; ?>
						</p>
					</div>


				<?php endforeach; ?>
			</div>




		</div>
	</section>




	<section id="form" class="mq-section mq-section--simulador">
		<div class="container">
			<div class="mq-suarenda">
				<header class="mq-header-suarenda mq-header-suarenda--simulador  mq-certificacoes__header--center">
					<h4 class="toptitle"></h4>
					<h2 class="title"><strong>Encontre o Marquise perfeito </br>para seu cliente.</strong></h2>
				</header>
				<div class="mq-suarenda-form">
					<?php echo do_shortcode('[contact-form-7 id="3570" title="Cadastro Corretor"]'); ?>
				</div>
			</div>
		</div>
	</section>






	</div>


	<style>
.form-group__termos.form-group__termos--interesse {
    margin-bottom: 20px;
}

.form-group .form-group__termos .texto,
.form-group .form-group__termos .texto a {
    color: #424242 !important;
}


		section.mq-section.mq-corretor__destaques {
			padding: 50px 0;
			margin: 0;
		}

		section.mq-section.mq-corretor__vantegens {
			background-color: #fdfdfd;
			margin: 0;
			padding: 80px 0 100px;
		}

		section.mq-section.mq-corretor__material {
			background-color: #fdfdfd;
			padding: 100px 0;
		}

		.mq-certificacoes__header--center {
			text-align: center;
		}

		.mq-contato-opcoes,
		.mq-corretor__destaques,
		.mq-corretor__vantegens {
			margin-bottom: 0;
		}

		.mq-banner-contato__content p {
			font-size: 1.5rem;
			line-height: 1.7;
		}

		.mq-suarenda-form {
			min-height: auto;
		}

		.mq-banner-contato {
			height: 34.875rem;
		}

		.owl-carousel .owl-dots {
			transform: translateY(100%);
		}

		.mq-corretor__destaques .wrap-slide {
			float: right;
			width: 100%;
		}

		.mq-corretor__destaques .wrap-slide a figure img {
			width: 100%;
			height: 100%;
			object-fit: contain;
		}

		.mq-corretor__vantegens .grid {
			display: grid;
			grid-template-areas: ". . . . .";
			grid-auto-columns: 1fr 1fr 1fr 1fr 1fr;
			float: left;
			width: 100%;
			grid-column-gap: 10px;
			margin-top: 50px;
		}

		.mq-corretor__material .grid {

			grid-template-areas: ". . . .";
			grid-auto-columns: 1fr 1fr 1fr 1fr;

		}

		.mq-corretor__vantegens .grid .item {
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
		}

		.mq-corretor__vantegens .grid .item img {
			width: 100%;
			height: 80px;
			object-fit: contain;
			max-width: 70px;
		}

		.mq-corretor__vantegens .item .title {
			font: 600 0.9rem Montserrat, Arial, helvetica, sans-serif;
			line-height: 1;
			color: #ff6515;
			text-align: center;
			margin-top: .9375rem;
		}

		.mq-corretor__vantegens .item .content {
			font: 100 .9rem Montserrat, Arial, helvetica, sans-serif;
			color: #ff6515;
			text-align: center;
			line-height: 1;
			margin-top: 10px;
		}

		.links-area-do-corretor img {
			max-width: 40px;
		}

		.radio-corretor {
			display: flex;
			width: 100%;
			justify-content: flex-start;
		}

		.radio-corretor label {
			display: flex;
			flex-direction: row-reverse;
			align-items: center;
			font-size: 23px;
			font: 100 1.1875rem Montserrat, Arial, helvetica, sans-serif;
		}

		.radio-corretor label input {
			width: 20px !important;
			margin-right: 40px;
			margin-left: 10px;
		}

		input.wpcf7-form-control.wpcf7-submit.envio-corretor {
			background-color: #ff6515;
			color: #fff;
			max-width: 230px;
			text-align: center;
			width: 100%;
			padding: 0;
			margin: 0 auto;
			display: flex;
			align-items: center;
			justify-content: center;
			cursor: pointer;
		}

		.mq-geo-options {
			margin-top: 30px;
		}

		@media only screen and (min-width: 1200px) {
			.mq-banner-contato {
				height: 40rem;
			}

		}

		@media only screen and (max-width: 869px) {
			.mq-corretor__vantegens .grid {
				grid-template-areas: ". .";
				grid-auto-columns: 1fr 1fr;
				grid-row-gap: 50px;
			}

		}

		@media only screen and (max-width: 640px) {
			.mq-geo-options__link {
				width: 70%;
				margin-bottom: 1.375rem;
			}

			.mq-corretor__vantegens .grid {
				grid-template-areas: ".";
				grid-auto-columns: 1fr;
				grid-row-gap: 50px;
			}
		}
	</style>

	<?php get_template_part('templates/html', 'footer'); ?>