<?php $settings = get_option('options_general'); ?>
    <?php foreach($settings['shows_image'] as $img):
    $url = wp_get_attachment_image_url($img,'full','',array('class'=>'calend')); ?>     
    <?php endforeach; ?>
<section class="section calendario" style="background:url(<?php echo $url; ?>);background-size: cover;background-position: center;background-repeat:no-repeat;">

    <h3 class="titulos border">Próximos Shows</h3>
    <div class="container">
        <div class="datas">
            <ul id="owl-shows" class="shows">

                <?php
                $get_data = date('Y-m-d');
                $args = array('posts_per_page'=> 15,'post_type' => 'agenda',
                    'meta_query' => array(
                        array(
                            'key'     => 'agenda_data',
                            'value'   => $get_data,
                            'compare' => '>=',
                        ),
                    ), 'orderby' => 'meta_value', 'order' => 'ASC'
                );

                $agenda = new WP_Query($args);
                while ( $agenda->have_posts()) : $agenda->the_post();
                    $meses = array(
                        "01" => "Janeiro",
                        "02" => "Fevereiro",
                        "03" => "Março",
                        "04" => "Abril",
                        "05" => "Maio",
                        "06" => "Junho",
                        "07" => "Julho",
                        "08" => "Agosto",
                        "09" => "Setembro",
                        "10" => "Outubro",
                        "11" => "Novembro",
                        "12" => "Dezembro"
                    );
                    $agenda_data   = get_post_meta($post->ID, 'agenda_data', true);
                    $agenda_local  = get_post_meta($post->ID, 'agenda_local', true);
                    $agenda_dia    = date('d', strtotime($agenda_data));
                    $agenda_data_m = date('m', strtotime($agenda_data));
                    $agenda_mes    = $meses[$agenda_data_m];
                ?>

                    <li class="shows__item">
                            <!-- <a href="<?php echo get_post_type_archive_link('agenda');?>"></a> -->
                            <div class="shows__dia"><?php echo $agenda_dia;?></div>
                            <div class="shows__mes"><?php echo $agenda_mes;?></div>
                            <div class="shows__local"><?php echo $agenda_local;?></div>
                            <!--</a>-->
                    </li>
                <?php endwhile; wp_reset_postdata(); ?>
            </ul>
        </div>
    </div>
</section>