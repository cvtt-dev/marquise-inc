<section class="mq-section mq-section--simulador">
    <div class="container">
        <div class="mq-suarenda">
            <header class="mq-header-suarenda mq-header-suarenda--simulador">
                <i class="icone-search"></i>
                <h4 class="toptitle">Descubra qual Marquise foi feito para você.</h4>
                <h2 class="title">Aqui você encontra o Marquise <strong> perfeito para você.</strong></h2>
            </header>
            <div class="mq-suarenda-form">

                <form class="form-simulador" method="POST" id="form-page-simulador" action="<?php echo get_bloginfo('url') ?>/simulador/empreendimentos.php">

                    <div class="form-simulador__item form-simulador__item--white">
                        <input name="nome" class="nome" id="page_sim_nome" type="text" placeholder="Nome" required />
                    </div>

                    <div class="form-simulador__item form-simulador__item--white">
                        <input name="email" class="email" id="sim_email" type="email" placeholder="E-mail" required />
                    </div>

                    <div class="form-simulador__item form-simulador__item--col2 form-simulador__item--white">
                        <div class="form-simulador__group">
                            <input name="fone" class="fone" id="page_sim_fone" type="text" placeholder="Telefone" required />
                        </div>
                        <div class="form-simulador__group">
                            <input name="cpf" class="cpf" type="text" id="page_sim_cpf" placeholder="CPF" required />
                        </div>
                    </div>

                    <div>
                    <!-- <div class="form-simulador__item form-simulador__item--white">
                        <select name="codEmp" class="" id="select_emp" required style="padding-left: 15px;">
                            <option value="">Escolha Algum Empreendimento</option>
                            <option value="4613" data-emp="Atlantis-Beira-Mar">Atlantis Beira Mar</option>
                            <option value="4044" data-emp="Bellatrix Residence">Bellatrix Residence</option>
                            <option value="4039" data-emp="Blue Residence">Blue Residence</option>
                            <option value="31322" data-emp="Centurion Business Center">Centurion Business Center</option>
                            <option value="4048" data-emp="Estação das Flores">Estação das Flores</option>
                            <option value="4750" data-emp="Gran Parc">Gran Parc</option>
                            <option value="4047" data-emp="Imperator Residence">Imperator Residence</option>
                            <option value="29294" data-emp="Infinity 600">Infinity 600</option>
                            <option value="4045" data-emp="Isla Jardin">Isla Jardin</option>
                            <option value="4615" data-emp="Mandara Kauai">Mandara Kauai</option>
                            <option value="31321" data-emp="Mandara Lanai">Mandara Lanai</option>
                            <option value="4046" data-emp="Palladium Business Center">Palladium Business Center</option>
                            <option value="4748" data-emp="Solaris Residence">Solaris Residence</option>
                            <option value="4749" data-emp="Splendido Residence">Splendido Residence</option>
                        </select>
                    </div> -->
                </div>

                    <div class="form-simulador__item form-simulador__item--col2 form-simulador__item--white">
                        <div class="form-simulador__group">
                            <input name="data" class="data" id="page_sim_data" type="text" placeholder="Data de Nascimento" required />
                        </div>
                        <div class="form-simulador__group">
                            <select name="tipo" class="tipo tipo--white" id="page_sim_tipo" required>
                                <option value="">Escolha seu Perfil</option>
                                <option value="Colaborador">Colaborador</option>
                                <option value="Cliente">Cliente</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-simulador__item form-simulador__item--matricula form-simulador__item--white">
                        <input name="matricula" class="matricula" id="page_sim_matricula" type="text" placeholder="Matrícula" required value="-" />
                    </div>


                    <div class="form-simulador__item form-simulador__item--white">
                        <h4 class="form-simulador__title form-simulador__title--gray">Renda Mensal Familiar Bruta</h4>
                        <div class="slidershell" id="slidershell">
                            <div class="sliderfill" id="sliderfill"></div>
                            <div class="slidertrack" id="slidertrack"></div>
                            <div class="sliderthumb" id="sliderthumb"></div>
                            <div class="slidervalue" id="slidervalue">R$ 0</div>
                            <input name="renda" class="slider slider--gray" id="slider" type="range" step="1000" min="0" max="100000" value="0" />
                        </div>
                    </div>

                    <div class="form-simulador__item form-simulador__item--aceite">
                        <div class="form-group">
                            <div class="form-group__termos">
                                <input type="checkbox" name="aceite" value="1" aria-invalid="false" class="iptAceite" required>
                                <span class="texto-gray">Aceito os <a href="https://www.marquiseincorporacoes.com.br/termos-de-uso/" title="termos de uso" target="_blank">termos de uso</a> e <a href="https://www.marquiseincorporacoes.com.br/politica-de-compartilhamento-de-dados/" title="política de compartilhamento de dados" target="_blank">política de compartilhamento de dados</a></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-simulador__item form-simulador__item--btn">
                        <button type="submit" class="btn-simular">Pesquisar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>