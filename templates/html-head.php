<!DOCTYPE HTML>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1 maximum-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    
    <meta name="facebook-domain-verification" content="28j81s8jazca5sxnjlx3lwfk7gllnf">
    <title><?php wp_title('');
            echo '';
            bloginfo('name'); ?></title>
    <?php wp_head(); ?>
    <link rel="alternate" href="<?php echo get_bloginfo('url'); ?>" hreflang="pt-BR" />
    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="">
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
<style>
 
    .mq-header-child:before{
        display: none !important;
    }
    .form-group--venda-terreno {margin-bottom: 30px;}
    .form-group .form-group__termos { display: flex; align-items: center; margin-top: 14px; padding-left: 12px; }
    .form-group .form-group__termos--interesse { margin-top: 0px; align-items: flex-start; }
    .form-group .form-group__termos--faleconosco { padding-left: 0; }
    .form-simulador__item--aceite {margin-top: 70px;}
    .form-group__termos--faleconosco .iptAceite{height: 20px!important; width: 20px!important;margin:0!important;}
    .form-simulador__item .form-group .form-group__termos input[type=checkbox],
    .form-group .form-group__termos .aceite .wpcf7-acceptance .wpcf7-list-item input[type=checkbox] { height: 15px; width: 15px; }
    .form-group .form-group__termos .texto-gray, .form-group .form-group__termos .texto { margin-left: 10px;}
    .form-group .form-group__termos .texto-gray, .form-group .form-group__termos .texto, .form-group .form-group__termos .texto a { color: #fff; font-family: 'Arial'; font-size: 13px; }
    .form-group .form-group__termos .texto-gray a, .form-group .form-group__termos .texto a, #wpcf7-f2857-p195-o1 .form-group .form-group__termos .texto a { font-weight: 600; color: #fff; text-decoration: underline; line-height: 20px; transition: all 500ms ease; }
    .form-group .form-group__termos .texto-gray {color:#333;}
    .form-group .form-group__termos .texto-gray a {color:#1e1e1e;}
    #wpcf7-f2857-p195-o1 .form-group .form-group__termos .texto,
    #wpcf7-f2857-p195-o1 .form-group .form-group__termos .texto a { color: #606060; transition: all 500ms ease; }
    #wpcf7-f2857-p195-o1 .form-group .form-group__termos .texto a:hover { color: #FF5722; }
    .wpcf7-form .form-group .form-group__termos .texto a:hover { color: #FF5722; }
    .has-cookie-bar #catapult-cookie-bar {padding: 21px 20px;}
    #catapult-cookie-bar a { font-weight: 700; }

</style>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M54XNR');</script>
<!-- End Google Tag Manager -->





<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M54XNR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


</head>

<body <?php body_class(); ?> data-session="<?= $_SESSION['xml']? 'true':'false'; ?>">

    <div id="fb-root"></div>