<?php 
    $empreendimento_caracteristicas = get_post_meta( get_the_id(), 'empreendimento_caracteristicas', false );
    $matrizGroup = get_post_meta( get_the_id(), 'matrizGroup', false );
?>

<?php if($matrizGroup): ?>

<section class="mq-section mq-section--emp-informacoes" id="info">
    <div class="container">
        <div class="mq-emp-info">
            <h2 class="title">Informações do Empreendimento</h2>
            <hr>
            <ul class="mq-emp-info__list">
            <div class="mq-emp-info__line"></div>
                <?php $i = 1; foreach ($matrizGroup as $tab) : foreach ($tab as $item) : ?>
                    <li><a data-w-tab="<?php echo $i; ?>" class="mq-emp-info__li">
                        <div class="mq-emp-info__circle" data="<?php echo $i; ?>"></div>
                        <div><?php echo $item['matrizTitulo'];?></div>
                    </a></li>
                <?php $i++; endforeach; endforeach; ?>
            </ul>
            
            <?php $j = 1; foreach ($matrizGroup as $tab) : foreach ($tab as $item) : ?>
                <div id="<?php echo $j; ?>" class="vantagens">
                    <ul class="vantagens__ul">
                        <?php foreach ($item['groupVantagem'] as $card) : ?>
                            
                            <li class="vantagens__li"><?php echo $card['matrizNomeVantagem']; ?></li>
        
                        <?php endforeach; ?>
                    </ul> 
                </div>
            <?php $j++; endforeach; endforeach; ?>
        
        </div>
    </div>
</section>
<?php endif; ?>