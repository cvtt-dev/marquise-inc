<?php
    $img = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

<section class="mq-banner mq-banner--medium" style="background-image: url('<?php echo $img; ?>'); ">
    <div class="container">

        <div class="mq-banner__content">
            <h3 class="title title--medium">Error 404 - Página não encontrada.</h3>
            <div class="desc">Desculpe, o endereço digitado não existe.</div>
        </div>

    </div>
</section>