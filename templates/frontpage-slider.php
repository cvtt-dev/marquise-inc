<section id="owl-slider-destaque">

    <?php
    $args = array('post_type' => 'slider', 'showposts' => -1, 'orderby' => 'date', 'order' => 'ASC');
    $slider = new WP_Query($args);

    while ($slider->have_posts()) : $slider->the_post();
        $bgImg = get_the_post_thumbnail_url(get_the_ID(), 'full');
        $slide_txt_btn = get_post_meta(get_the_ID(), 'slide_txt_btn', true);
        $slide_link = get_post_meta(get_the_id(), 'slide_link', true);
        $slide_link_position = get_post_meta(get_the_id(), 'slide_pos_link', true);


        ?>

        <div class="mq-slider" style="background-image: url('<?php echo $bgImg; ?>'); ">
            <div class="container">
                <div class="mq-slider__info-destaque">
                    <div class="mq-slider__txt">
                        <div class="mq-slider__info"><?php the_content(); ?></div>
                        <?php if ($slide_link_position == 'btn') : ?>
                            <a href="<?php echo $slide_link; ?>" title="<?php the_title_attribute(array('before' => 'Quero ver mais: ')); ?>" class="mq-btn mq-btn--medium mq-btn--orange mq-btn--clear mp-slider-buttom">
                                <?php echo $slide_txt_btn ? $slide_txt_btn : 'Conheça ' ?> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/seta-branca.png" class="mq-slider__seta"></a>
                        <?php else : ?>
                            <a href="<?php echo $slide_link; ?>" class="mq-slider__img-link"></a>
                        <?php endif; ?>
                    </div>
                    <div class="mouse">
                        <span class="mq-slider__span">veja mais</span>
                        <div class="mouse-wheel"></div>
                        <div class="arrows"></div>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile;
wp_reset_postdata(); ?>

</section>