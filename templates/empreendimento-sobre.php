<?php
    $tamanho = get_post_meta(get_the_id(), 'tamanho', true);
    $qtdquartos = get_post_meta(get_the_id(), 'qtdquartos', true);
    $qtdgaragem = get_post_meta(get_the_id(), 'qtdgaragem', true);
    $bairro = get_post_meta(get_the_id(), 'bairro', true);
    $url_tour = get_post_meta(get_the_ID(), 'tour', true);
    $condicoes_link = get_post_meta(get_the_ID(), 'condicoes_link', true);
    $stand_valor = get_post_meta(get_the_ID(), 'stand_valor', true);
    $txt_juridico = get_post_meta(get_the_ID(), 'txt_juridico', true);
    $arquivo_book = get_post_meta(get_the_ID(), 'arquivo_book', true);
?>
<section class="mq-section mq-section--overview" id="visao">
    <div class="container">

        <div class="mq-emp-informacao">
            <div class="mq-emp-informacao__txt">
                <hr>
                <h2 class="title"><?php the_title(); ?></h2>
                <div class="content"><?php the_content(); ?></div>
               

                <div class="detalhes">

                    <div class="item">
                        <h4 class="toptitle"><?php echo $bairro; ?></h4>
                        <div class="item-icone">
                            <!-- <i class="fa fa-map-marker" aria-hidden="true"></i> -->
                            <h5 class="title">Local</h5>
                        </div>
                    </div>
                    <div class="divisor divisor--sobre"></div>
                    <div class="item">
                        <h4 class="toptitle"><?php echo $tamanho; ?></h4>
                        <div class="item-icone">
                            <!-- <i class="cvtt-icon-ruler"></i> -->
                            <span class="title">Área</span>
                        </div>
                    </div>
                    <?php if($qtdquartos) : ?>
                    <div class="divisor divisor--sobre"></div>
                    <div class="item">
                        <h4 class="toptitle"><?php echo $qtdquartos; ?></h4>
                        <div class="item-icone">
                            <!-- <i class="cvtt-icon-bed" aria-hidden="true">
                            </i> -->
                                <span class="title">Quartos</span>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if($qtdquartos) : ?>
                    <div class="divisor divisor--sobre"></div>
                    <div class="item">
                        <h4 class="toptitle"><?php echo $qtdgaragem ?></h4>
                        <div class="item-icone">
                            <!-- <i class="cvtt-icon-car" aria-hidden="true">
                            </i> -->
                                <span class="title">Vagas</span>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="mq-emp-informacao__img">
                <?php  if($stand_valor): ?>
                    <div class="preco">
                        <div class="apartir">A partir de:</div>
                        <div class="valor">R$: <?php echo $stand_valor; ?></div>
                        <div class="juridico"><?php  echo $txt_juridico; ?></div>
                    </div>
                <?php endif; ?> 
                <div class="btns-options">
                    <a href="#emp-popup" class="open-modal-emp btns-options__btn" title="Tenho Interesse" data-effect="mfp-move-horizontal">Tenho Interesse <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_seta.png" class="icon-seta-sobre-empreedimento" alt=""></a>
                    <?php if ($url_tour) : ?>
                        <a href="<?php $url_tour; ?>" target="_blank" class="btns-options__btn">Tour Virtual <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_seta.png" class="icon-seta-sobre-empreedimento" alt="" data-effect="mfp-move-horizontal"></a>

                    <?php endif; ?>

                    <?php if( $arquivo_book ) : ?>
                        <a href="<?php $wp_get_attachment_url; ?>" target="_blank" class="btns-options__btn">Folder <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_seta.png" class="icon-seta-sobre-empreedimento" alt="" data-effect="mfp-move-horizontal"></a>

                    <?php endif; ?>
                    
                    <?php if($condicoes_link) :  ?>
                    <a href="#emp-popup-cond" class="open-modal-emp btns-options__btn" title="Tenho Interesse" data-effect="mfp-move-horizontal">Condições Especiais <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_seta.png" class="icon-seta-sobre-empreedimento" alt=""></a>
                    <div id="emp-popup-cond" class="mq-popup-emp mfp-modal mfp-modal--emp-mobile-cond mfp-hide">
                        <h3 class="mq-popup-emp__top-title mq-popup-emp__top-title--cond">Preencha o formulário abaixo e tenha acesso a preços e condições especiais</h3>
                        <?php echo do_shortcode('[contact-form-7 id="2580" title="FORM: POPUP - EMPREENDIMENTO CONDIÇÕES ESPECIAIS"]')?>
                    </div>
                    <?php endif; ?>
                </div>
           </div>
        </div>

    </div>
</section>