<section class="mq-banner-empreendimentos">
    <img class="mq-banner-empreendimentos__img" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-empreendimentos.jpg" alt="Marquise Incorporações" />
    <div class="mq-banner-empreendimentos__info-destaque">
        
        <div class="mq-banner-empreendimentos__infos">
            <h3 class="mq-banner-empreendimentos__top-title">Imóveis</h3>
            <h2 class="mq-banner-empreendimentos__title-destaque">Conheça todos os empreendimentos da <strong>Marquise Incorporações</strong></h2>
        </div>
        
    </div>
</section>