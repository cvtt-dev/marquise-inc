<section class="mq-section">
    <div class="container">
        <?php get_template_part('templates/empreendimento', 'busca'); ?>

        <div class="mq-empreendimentos">
            <?php                 
                $emp = new WP_Query(array('post_type' => 'mar_empreendimentos', 'showposts' => -1, 'orderby' => 'ASC',));
                while ($emp->have_posts()) : $emp->the_post();
                    get_template_part('templates/loop', 'empreendimento');
                endwhile;   
                wp_reset_postdata();                           
            ?>
        </div>

    </div>
</section>