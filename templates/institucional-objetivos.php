<?php

    $img_missao = get_post_meta(get_the_ID(), 'img_missao', false);
    $img_visao = get_post_meta(get_the_ID(), 'img_visao', false);
    $img_valores = get_post_meta(get_the_ID(), 'img_valores', false);

    $missao_titulo = get_post_meta(get_the_ID(), 'missao_titulo', true);
    $visao_titulo = get_post_meta(get_the_ID(), 'visao_titulo', true);
    $valores_titulo = get_post_meta(get_the_ID(), 'valores_titulo', true);

    $sobre_missao = get_post_meta(get_the_ID(), 'sobre_missao', true);
    $sobre_visao = get_post_meta(get_the_ID(), 'sobre_visao', true);
    $sobre_valores = get_post_meta(get_the_ID(), 'sobre_valores', true);



?>
<section class="mq-objetivos" id="objetivos">
    <div class="container">
        <div class="mq-obj" id="objetivo">
            <article class="mq-obj__tipo">
                
                <?php foreach ($img_missao as $img) : 
                    $caption = wp_get_attachment_caption( $img ); ?>
                    <figure class="thumb">
                        <?php echo wp_get_attachment_image($img, 'full'); ?>
                    </figure>
                <?php endforeach; ?>

                <h2 class="title"><?php echo $missao_titulo; ?></h2>
                <div class="info">
                    <?php echo $sobre_missao?>
                </div>
            </article>
            <article class="mq-obj__tipo">
                
                <?php foreach ($img_visao as $img) : 
                    $caption = wp_get_attachment_caption( $img ); ?>
                    <figure class="thumb">
                        <?php echo wp_get_attachment_image($img, 'full'); ?>
                    </figure>
                <?php endforeach; ?>
                <h2 class="title"><?php echo $visao_titulo; ?></h2>
                <div class="info">
                    <?php echo $sobre_visao?>
                </div>
            </article>
            <article class="mq-obj__tipo">
                 <?php foreach ($img_valores as $img) : 
                    $caption = wp_get_attachment_caption( $img ); ?>
                    <figure class="thumb">
                        <?php echo wp_get_attachment_image($img, 'full'); ?>
                    </figure>
                <?php endforeach; ?>
                <h2 class="title"><?php echo $valores_titulo?></h2>
                <div class="info">
                    <?php echo $sobre_valores?>
                </div>
            </article>
        </div>
    </div>
</section>