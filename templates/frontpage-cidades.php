<section class="section-cidades">
    <div class="container space-padding">
        <div class="section-cidades-flex">
            <div>
                <div class="section-cidades-text">
                    <h2>Navegue por cidade</h2>
                    <p>Conheça os empreedimentos Marquise na sua cidade</p>
                </div>
            </div>
            <div>
                <a href="#" onclick="getLocation()" class="section-cidades__butom automatic-location accent-primary"><img class="section-cidades__icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/location-black.png" alt=""> Localização Automática</a>
            </div>
        </div>
        <div class="cidades-card-div">
            <a href="#ce" class="" onclick="cookieLocation('brazil','ceara')">
                <div class="cidades-card">
                    <div class="cidades-card__text">
                        <h3 class="cidades-card__h3">Fortaleza</h3>
                        <p class="cidades-card__p">CE</p>
                    </div>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/location-fortaleza.png" alt="" class="cidades-card__thumb">
                </div>
            </a>
            <a href="#sp" class="" onclick="cookieLocation('brazil','sao-paulo')">
                <div class="cidades-card">
                    <div class="cidades-card__text">
                        <h3 class="cidades-card__h3">São Paulo</h3>
                        <p class="cidades-card__p">SP</p>
                    </div>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/spthumb.png" alt="" class="cidades-card__thumb">
                </div>
            </a>
        </div>
    </div>

</section>