<?php $settings = get_option('options_gerais'); 
    if($settings):
    foreach ($settings['popup_img'] as $img) :
        $thumb = wp_get_attachment_image_url( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
    endforeach; 
    endif;
?>


<?php if($thumb) : ?>
<div class="popup" id="popup">
    <div class="popup-imagem">
        <div class="close" id="close">x</div>
        <img class="thumb" src="<?php echo $thumb; ?>" alt="Marquise Incorporação" title="Marquise Incorporação" class="">
    </div>
</div> 
<?php endif; ?>