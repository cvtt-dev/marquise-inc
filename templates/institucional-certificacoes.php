
    <?php $sobre_certificacao = get_post_meta(get_the_ID(), 'group_certificados', true); ?>
    <section class="mq-certificacoes" id="premios">
        <div class="container">
            <header class="mq-certificacoes__header">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/premios.png" alt="Marquise Incorporação" title="Marquise Incorporação" class="">
                <h2 class="title">Prêmios e Certificações</h2>
            </header>
            <div class="cert-lista">

                <ul class="owl-certificados mq-empreendimento__owl" style="width:100%;padding: 0 50px;" id="owl-slider-certificado">

                    <?php foreach ($sobre_certificacao as $value) : ?>

                        <li class="item">
                            <figure class="medalha">
                                <img src="<?php echo $value['medalha']? wp_get_attachment_url($value['medalha']) : 'https://www.marquiseincorporacoes.com.br/static-images/selo-de-qualidade.png';?>" alt="">
                            </figure>
                            <div class="text">
                                <?php echo $value['texto']; ?>
                            </div>

                        </li>
                    <?php endforeach; ?>

                </ul>

            </div>
        </div>
    </section>
    <script>
        jQuery(function($) {
            $(document).ready(function() {
                $('.owl-certificados').owlCarousel({
                    rtl: true,
                    loop: true,
                    margin: 10,
                    nav: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 5
                        }
                    }
                })
            })
        })
    </script>

    <style>
        .owl-certificados .item .medalha img {
            width: 100%;
            height: 100%;
            object-fit: contain;

        }

        .owl-certificados .item .text{
            padding: 0 10px;
            color: dimgrey;
            font-size: 14px;
            line-height: 1.3;

        }

        .owl-certificados .item .medalha {
            width: 100%;
            max-width: 100px;
            margin: 0 auto 30px;
            display: flex;
        }

        .owl-carousel .owl-dots {

            transform: translateY(0%);
        }

        @media only screen and (min-width: 767px) {
            .owl-carousel .owl-dots {
                transform: translateY(100%);
            }
        }
    </style>