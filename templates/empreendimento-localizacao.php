<?php
$logradouro = get_post_meta(get_the_ID(), 'logradouro_emp', true);
$estado = get_post_meta(get_the_ID(), 'estado_emp', true);
$cidade = get_post_meta(get_the_ID(), 'cidade_emp', true);
$bairro = get_post_meta(get_the_ID(), 'bairro_emp', true);
$latlong = get_post_meta(get_the_ID(), 'lat_long_emp', true);
$descricao = get_post_meta(get_the_ID(), 'descript_emp', true);

$str_explode = explode(",", $latlong);
$lat         = $str_explode[0];
$long        = $str_explode[1];
$slug        = $post->post_name;
$link        = "https://www.google.com/maps/search/?api=1&query=" . $lat . "," . $long . "";
$link_waze   = "https://www.waze.com/livemap/directions?navigate=yes&latlng=" . $lat . "%2C" . $long . "&zoom=17";

?>



<section class="mq-section mq-section--localizacao">
    <div class="container">

        <div class="mp-section-localizacao__flex">
            
                  
            
            
                    <div class="mq-wrap-mapa">
                        <div class="map_mask" style="display: none;">
                            <div class="signal"></div>
                        </div>
            
            
            
                        <div id="map-leaflet" style="height: 28.5rem;">
            
                            <script>
                                var cities = L.layerGroup();
                                var greenIcon = L.icon({
                                    iconUrl: 'https://www.marquiseincorporacoes.com.br/static-images/pin-orange.png',
                                    shadowUrl: 'https://www.marquiseincorporacoes.com.br/static-images/pin-shadow.png',
                                    iconSize: [30, 42], // size of the icon
                                    shadowSize: [35, 14], // size of the shadow
                                    iconAnchor: [16, 41], // point of the icon which will correspond to marker's location
                                    shadowAnchor: [2, 14], // the same for the shadow
                                    popupAnchor: [-3, -50] // point from which the popup should open relative to the iconAnchor
                                });
                                L.marker([<?php echo $lat; ?>, <?php echo $long; ?>], {
                                    icon: greenIcon
                                }).bindPopup('<h3><?php echo get_the_title(); ?></h3><?php echo $logradouro . " - " . $bairro . "<br>" . $cidade . " - " . $estado; ?></br><a style="font: 600 .75rem Montserrat,Arial,helvetica,sans-serif;border-radius: 1.875rem;  margin-top: 10px;  background: #ee651a;    border: 0;    color: #fff;    cursor: pointer;    display: -ms-flexbox;    display: flex;    height: 2.5rem;    -ms-flex-pack: center;    justify-content: center;    padding: 12px 30px;    transition: all .6s;    text-transform: uppercase;    width: auto;"  target="_blank" href="<?php echo $link; ?>">Traçar Rota</a>').addTo(cities);
            
            
            
                                var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                                    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                                    mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
            
                                var grayscale = L.tileLayer(mbUrl, {
                                        id: 'mapbox/light-v9',
                                        tileSize: 512,
                                        zoomOffset: -1,
                                        attribution: mbAttr
                                    }),
                                    streets = L.tileLayer(mbUrl, {
                                        id: 'mapbox/streets-v11',
                                        tileSize: 512,
                                        zoomOffset: -1,
                                        attribution: mbAttr
                                    });
            
                                var map = L.map('map-leaflet', {
                                    center: [<?php echo $lat; ?>, <?php echo $long; ?>],
                                    zoom: 15,
                                    zoomControl: false,
                                    layers: [grayscale, cities]
                                });
            
                                var baseLayers = {
                                    "Cinza": grayscale,
                                    "Ruas": streets
                                };
            
            
            
                                L.control.layers(baseLayers).addTo(map);
            
            
            
                                // custom zoom bar control that includes a Zoom Home function
                                L.Control.zoomHome = L.Control.extend({
                                    options: {
                                        position: 'topright',
                                        zoomInText: '+',
                                        zoomInTitle: 'Zoom in',
                                        zoomOutText: '-',
                                        zoomOutTitle: 'Zoom out',
                                        zoomHomeText: '<i class="fa fa-home" style="line-height:1.65;"></i>',
                                        zoomHomeTitle: 'Zoom home'
                                    },
            
                                    onAdd: function(map) {
                                        var controlName = 'gin-control-zoom',
                                            container = L.DomUtil.create('div', controlName + ' leaflet-bar'),
                                            options = this.options;
            
                                        this._zoomInButton = this._createButton(options.zoomInText, options.zoomInTitle,
                                            controlName + '-in', container, this._zoomIn);
                                        this._zoomHomeButton = this._createButton(options.zoomHomeText, options.zoomHomeTitle,
                                            controlName + '-home', container, this._zoomHome);
                                        this._zoomOutButton = this._createButton(options.zoomOutText, options.zoomOutTitle,
                                            controlName + '-out', container, this._zoomOut);
            
                                        this._updateDisabled();
                                        map.on('zoomend zoomlevelschange', this._updateDisabled, this);
            
                                        return container;
                                    },
            
                                    onRemove: function(map) {
                                        map.off('zoomend zoomlevelschange', this._updateDisabled, this);
                                    },
            
                                    _zoomIn: function(e) {
                                        this._map.zoomIn(e.shiftKey ? 3 : 1);
                                    },
            
                                    _zoomOut: function(e) {
                                        this._map.zoomOut(e.shiftKey ? 3 : 1);
                                    },
            
                                    _zoomHome: function(e) {
                                        map.setView([<?php echo $lat; ?>, <?php echo $long; ?>], 15);
                                    },
            
                                    _createButton: function(html, title, className, container, fn) {
                                        var link = L.DomUtil.create('a', className, container);
                                        link.innerHTML = html;
                                        link.href = '#';
                                        link.title = title;
            
                                        L.DomEvent.on(link, 'mousedown dblclick', L.DomEvent.stopPropagation)
                                            .on(link, 'click', L.DomEvent.stop)
                                            .on(link, 'click', fn, this)
                                            .on(link, 'click', this._refocusOnMap, this);
            
                                        return link;
                                    },
            
                                    _updateDisabled: function() {
                                        var map = this._map,
                                            className = 'leaflet-disabled';
            
                                        L.DomUtil.removeClass(this._zoomInButton, className);
                                        L.DomUtil.removeClass(this._zoomOutButton, className);
            
                                        if (map._zoom === map.getMinZoom()) {
                                            L.DomUtil.addClass(this._zoomOutButton, className);
                                        }
                                        if (map._zoom === map.getMaxZoom()) {
                                            L.DomUtil.addClass(this._zoomInButton, className);
                                        }
                                    }
                                });
                                // add the new control to the map
                                var zoomHome = new L.Control.zoomHome();
                                zoomHome.addTo(map);
                            </script>
            
            
            
            
            
            
            
            
                            <!-- <div id="map" class="bg-mapa">
            
                                <div class="item_mapa" data-id="<?php //echo $slug; 
                                                                ?>" data-link="<?php //echo $link; 
                                                                                ?>" data-info="<h3><?php //echo get_the_title(); 
                                                                                                    ?></h3><?php //echo $logradouro . " - " . $bairro . "<br>" . $cidade . " - " . $estado; 
                                                                                                                                ?>" data-lat="<?php //echo $lat; 
                                                                                                                                                                    ?>" data-long="<?php //echo $long; 
                                                                                                                                                                                                                ?>" data-icone="<?php //echo get_template_directory_uri() . '/assets/images/marker.png'; 
                                                                                                                                                                                                                                                                                                                ?>"></div>
            
                            </div> -->
                        </div>

        
                    </div>  
                    
                    <?php if ($logradouro) : ?>
            
            <header class="mq-header-localizacao">
                <h2 class="mq-header-localizacao__title"><strong>Perto de tudo</strong> que você precisa</h2>
                <p class="mq-header-localizacao__end"><?php echo $logradouro . " - " . $cidade . " / " . $estado; ?></p>
                <p class="mp-header-localizacao__desc"><?php if($descricao): echo $descricao; else: echo ""; endif ?></p>
                <div class="mq-header-localizacao__options">
                    <a href="<?php echo $link; ?>" target="_blank" class="link">Navegar <img src="<?php echo get_template_directory_uri(); ?>/assets/images/seta-branca.png" alt="seta-branca   "></a>
                    <a href="<?php echo $link_waze; ?>" target="_blank" class="link">Abrir no Waze <img src="<?php echo get_template_directory_uri(); ?>/assets/images/seta-branca.png" alt="seta-branca"></a>
                </div>
            </header>

        <?php endif; ?>


        </div>
</section>