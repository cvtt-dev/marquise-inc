<style>
	.btn-map__link {
		font: 600 .75rem Montserrat, Arial, helvetica, sans-serif;
		border-radius: 1.875rem;
		margin-top: 10px;
		background: #ee651a;
		border: 0;
		color: #fff !important;
		cursor: pointer;
		display: -ms-flexbox;
		display: flex;
		height: 2.5rem;
		-ms-flex-pack: center;
		justify-content: center;
		padding: 12px 30px;
		transition: all .6s;
		text-transform: uppercase;
		width: auto;
	}
</style>



<section class="mq-section mq-section--maps">
	<div class="container">
		<header class="mq-header-empreendimentos mq-header-empreendimentos--maps">
			<i class="mq-icon mq-icon--sala-comercial"></i>
			<h2 class="mq-header-empreendimentos__title">Encontre o Marquise ideal em Fortaleza</h2>
		</header>
	</div>
</section>
<div class="mq-wrap-mapa-page">
	<div class="map_mask" style="display: none;">
		<div class="signal"></div>
	</div>

	<div id="map-leaflet" style="height: 37.5rem;">

		<script>
			var locations = [
				<?php
				$loop = new WP_Query(array('post_type' => 'mar_empreendimentos', 'posts_per_page' => -1));
				$c = 0;
				while ($loop->have_posts()) : $loop->the_post();
					$logradouro = get_post_meta(get_the_ID(), 'logradouro_emp', true);
					$estado = get_post_meta(get_the_ID(), 'estado_emp', true);
					$cidade = get_post_meta(get_the_ID(), 'cidade_emp', true);
					$bairro = get_post_meta(get_the_ID(), 'bairro_emp', true);
					$latlong = get_post_meta(get_the_ID(), 'lat_long_emp', true);

					$str_explode = explode(",", $latlong);
					$lat         = $str_explode[0];
					$long        = $str_explode[1];
					$slug        = $post->post_name;
					$link        = "https://www.google.com/maps/search/?api=1&query=" . $lat . "," . $long . "";
					$link_waze   = "https://www.waze.com/livemap/directions?navigate=yes&latlng=" . $lat . "%2C" . $long . "&zoom=17";

					if ($latlong) {


				?>["<?= $lat; ?>", "<?= $long; ?>", "<?= the_title(); ?>", "<?= $logradouro; ?>", "<?= $bairro ?>", "<?= $cidade ?>", "<?= $estado ?>", "<?= $link ?>", "<?= $link_waze ?>"],





				<?php }
				endwhile;
				wp_reset_query(); ?>
			];
			var cities = L.layerGroup();
			var greenIcon = L.icon({
				iconUrl: 'https://www.marquiseincorporacoes.com.br/static-images/pin-orange.png',
				shadowUrl: 'https://www.marquiseincorporacoes.com.br/static-images/pin-shadow.png',
				iconSize: [30, 42], // size of the icon
				shadowSize: [35, 14], // size of the shadow
				iconAnchor: [16, 41], // point of the icon which will correspond to marker's location
				shadowAnchor: [2, 14], // the same for the shadow
				popupAnchor: [-3, -50] // point from which the popup should open relative to the iconAnchor
			});
			var bounds = L.latLngBounds() // Instantiate LatLngBounds object

			for (var i = 0; i < locations.length; i++) {
				marker = new L.marker([
						locations[i][0],
						locations[i][1]
					], {
						icon: greenIcon
					})
					.bindPopup(
						"<h3>" + locations[i][2] + "</h3>" +
						locations[i][3] + " - " + locations[i][4] +
						"<br />" +
						locations[i][5] + " - " + locations[i][6] +
						'<a class="btn-map__link" target="_blank" href="' + locations[i][7] + '">Traçar Rota</a>'
					)
					.addTo(cities);
				bounds.extend([locations[i][0], locations[i][1]]); // Extend LatLngBounds with coordinates
			}

			console.log(bounds);




			var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

			var grayscale = L.tileLayer(mbUrl, {
					id: 'mapbox/light-v9',
					tileSize: 512,
					zoomOffset: -1,
					attribution: mbAttr
				}),
				streets = L.tileLayer(mbUrl, {
					id: 'mapbox/streets-v11',
					tileSize: 512,
					zoomOffset: -1,
					attribution: mbAttr
				});

			var map = L.map('map-leaflet', {
				center: [<?php echo $lat; ?>, <?php echo $long; ?>],
				zoom: 15,
				zoomControl: false,
				layers: [grayscale, cities]
			});

			var baseLayers = {
				"Cinza": grayscale,
				"Ruas": streets
			};

			map.fitBounds(bounds);

			L.control.layers(baseLayers).addTo(map);



			// custom zoom bar control that includes a Zoom Home function
			L.Control.zoomHome = L.Control.extend({
				options: {
					position: 'topright',
					zoomInText: '+',
					zoomInTitle: 'Zoom in',
					zoomOutText: '-',
					zoomOutTitle: 'Zoom out',
					zoomHomeText: '<i class="fa fa-home" style="line-height:1.65;"></i>',
					zoomHomeTitle: 'Zoom home'
				},

				onAdd: function(map) {
					var controlName = 'gin-control-zoom',
						container = L.DomUtil.create('div', controlName + ' leaflet-bar'),
						options = this.options;

					this._zoomInButton = this._createButton(options.zoomInText, options.zoomInTitle,
						controlName + '-in', container, this._zoomIn);
					this._zoomHomeButton = this._createButton(options.zoomHomeText, options.zoomHomeTitle,
						controlName + '-home', container, this._zoomHome);
					this._zoomOutButton = this._createButton(options.zoomOutText, options.zoomOutTitle,
						controlName + '-out', container, this._zoomOut);

					this._updateDisabled();
					map.on('zoomend zoomlevelschange', this._updateDisabled, this);

					return container;
				},

				onRemove: function(map) {
					map.off('zoomend zoomlevelschange', this._updateDisabled, this);
				},

				_zoomIn: function(e) {
					this._map.zoomIn(e.shiftKey ? 3 : 1);
				},

				_zoomOut: function(e) {
					this._map.zoomOut(e.shiftKey ? 3 : 1);
				},

				_zoomHome: function(e) {
					map.setView([<?php echo $lat; ?>, <?php echo $long; ?>], 15);
				},

				_createButton: function(html, title, className, container, fn) {
					var link = L.DomUtil.create('a', className, container);
					link.innerHTML = html;
					link.href = '#';
					link.title = title;

					L.DomEvent.on(link, 'mousedown dblclick', L.DomEvent.stopPropagation)
						.on(link, 'click', L.DomEvent.stop)
						.on(link, 'click', fn, this)
						.on(link, 'click', this._refocusOnMap, this);

					return link;
				},

				_updateDisabled: function() {
					var map = this._map,
						className = 'leaflet-disabled';

					L.DomUtil.removeClass(this._zoomInButton, className);
					L.DomUtil.removeClass(this._zoomOutButton, className);

					if (map._zoom === map.getMinZoom()) {
						L.DomUtil.addClass(this._zoomOutButton, className);
					}
					if (map._zoom === map.getMaxZoom()) {
						L.DomUtil.addClass(this._zoomInButton, className);
					}
				}
			});
			// add the new control to the map
			var zoomHome = new L.Control.zoomHome();
			zoomHome.addTo(map);
		</script>

	</div>

</div>












<div id="map-leaflet" style="height: 37.5rem;">