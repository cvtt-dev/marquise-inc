<?php
    $img = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

<section class="mq-banner-contato" style="background-image: url('<?php echo $img; ?>'); ">
        
   
        <div class="container">
            <div class="mq-banner-contato__content">

                <h3 class="title"><?php the_title(); ?></h3>
                <div class="desc"><?php the_content(); ?></div>
            </div>
        </div>
   
</section>


