<?php 
    $empreendimento_caracteristicas = get_post_meta( get_the_id(), 'empreendimento_caracteristicas', false );
?>
<section class="mq-section mq-section--ficha" id="ficha">
    <div class="container">
        <div class="mq-emp-ficha">
            <h2 class="title">Ficha técnica</h2>
            <div class="ficha">
                <?php foreach ($empreendimento_caracteristicas as $info) : 
                echo $info['ficha_tecnica'];
                endforeach; ?>
            </div>
        </div>
    </div>
</section>