<div class="mq-header-section-empreendimentos">

    <header class="mq-header-empreendimentos">
        <i class="mq-icon mq-icon--empreendimentos"></i>
        <h2 class="mq-header-empreendimentos__title">Empreendimentos</h2>
    </header>

    <div class="mq-empreendimentos-all__filter">
        <h3 class="title">Procure seu imóvel</h3>
        <form class="mq-form-empreendimento" role="search" method="get" id="searchform" action="<?php echo home_url(); ?>/busca-empreendimento/">
            <div class="mq-form-empreendimento__field">
                <?php wp_dropdown_categories(
                    array(
                        'show_option_none'  => 'Tipo',
                        'name'              => 'tipo-empreendimentos',
                        'show_count'        => 0,
                        'orderby'           => 'name',
                        'echo'              => 1,
                        'taxonomy'          => 'tipo-empreendimentos',
                        'class'             => 'emp_field',
                    )
                );
                ?>
            </div>
            <div class="mq-form-empreendimento__field">
                <?php wp_dropdown_categories(
                    array(
                        'show_option_none'  => 'Bairro',
                        'name'              => 'bairro-empreendimentos',
                        'show_count'        => 0,
                        'orderby'           => 'name',
                        'echo'              => 1,
                        'taxonomy'          => 'bairro-empreendimentos',
                        'class'             => 'emp_field',
                    )
                );
                ?>
            </div>
            <div class="mq-form-empreendimento__field">
                <?php wp_dropdown_categories(
                    array(
                        'show_option_none'  => 'Estágio da obra',
                        'name'              => 'status-empreendimentos',
                        'show_count'        => 0,
                        'orderby'           => 'name',
                        'order'             => 'ASC',
                        'echo'              => 1,
                        'taxonomy'          => 'status-empreendimentos',
                        'class'             => 'emp_field',
                    )
                );
                ?>
            </div>
            <div class="mq-form-empreendimento__field">
                <?php wp_dropdown_categories(

                    array(
                        'show_option_none'  => 'Dormitórios',
                        'name'              => 'qdtdomitorios-empreendimentos',
                        'show_count'        => 0,
                        'orderby'           => 'name',
                        'echo'              => 1,
                        'taxonomy'           => 'qdtdomitorios-empreendimentos',
                        'class'             => 'emp_field',
                    )

                   
                );
                ?>
            </div>
            <div class="mq-form-empreendimento__field">
                <button type="submit" class="emp_field btn-busca" id="searchsubmit" value="Search">Buscar <i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
</div>