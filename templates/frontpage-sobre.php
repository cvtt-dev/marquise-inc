<section class="home-section-sobre">
    <div class="container space-padding">
        <div class="home-section-sobre-content">
            <h4> SOBRE A MARQUISE</h4>
            <h1>Nosso Projeto é <strong>você</strong></h1>
            <hr>
            <p>Fundada em 1917 como uma construtora, a Marquise cresceu e se tornou 
                um grupo atuante em diferentes segmentos, entre eles o de incorporações
                sendo responsável po empreedimentos imobiliários que são ícones em
                todo o Brasil.
            </p>
            <a href="">Saiba Mais <img src="<?php echo get_template_directory_uri(); ?>/assets/images/seta-branca.png" class="icon-seta" alt=""></a>
        </div>
    </div>
    <div class="home-section__background"></div>
</section>