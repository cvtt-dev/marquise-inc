<?php $emp_galeria = get_post_meta(get_the_id(), 'img_emp_galeria', false); ?>

<?php if ($emp_galeria) : ?>
    <section class="empreedimentos-section-fotos" id="">
        <div class="container">
            <h2 class="empreedimentos-section-fotos__title">Galeria <span><?php echo get_the_title(); ?></span></h2>
        </div>
        <hr>

        <div class="container">
            <div class="owl-carousel owl-theme" id="emp-fotos">
                <?php 
                
                    for($d = 0; $d <= count($emp_galeria); $d = $d + 4):  ?>
                    
                    <?php if($emp_galeria[$d]): ?>
                    <div class="empreedimentos-section-fotos--grid modal-foto">
                        <?php $i = $d;
                            $j = $i + 3; // contador
                            $p = $j - 3; // primeiro item
                        while($i <= $j): ?>
                        <?php $url = wp_get_attachment_url($emp_galeria[$i], 'full'); ?>
                        <?php $caption = wp_get_attachment_caption($emp_galeria[$i]); ?>
                            <?php if($emp_galeria[$i]): ?>
                                <a class="<?php  if($i == $p): echo 'empreedimentos-section-fotos__full';elseif($i == $j): echo 'empreedimentos-section-fotos__large'; else: echo 'empreedimentos-section-fotos__normal'; endif; ?>" href="<?php echo wp_get_attachment_url($emp_galeria[$i], 'full'); ?>">
                                    <img src="<?php echo $url; ?>" alt="" class="empreedimentos-section-fotos__thumb">
                                    <figcaption class="empreedimentos-section-fotos__caption"><?php echo $caption; ?></figcaption>
                                </a>         
                            <?php endif; ?>                
                        <?php $i++; endwhile; ?>
                    </div>
                    <?php endif; ?> 
                    <?php endfor; ?>
            </div>

        </div>

    </section>
<?php endif; ?>