<?php
    $gal_plantas = get_post_meta(get_the_id(), 'plantas', true);
?>

<?php if($gal_plantas): ?>
<section class="mq-section mq-section--plantas" id="plantas">
    <div class="container">
        <div class="mp-section-plantas-title">
            <h2 class="">Opções da Planta</h2>
            <hr>
            <ul class="mp-section-plantas-title__item">
                <!-- <li>Opção 01</li>
                <li>Opção 02</li>
                <li>Opção 03</li> -->
            </ul>
        </div>

        <div class="mq-plantas" id="owl-plantas">

            <?php $i = 1; foreach ($gal_plantas as $info) : ?>
                <div class="mq-plantas__item" data-item="<?php echo $i; ?>">
                    <div class="mq-plantas__legend">
                        <h3 class="title"><?php echo $info['emp_plantas_title']; ?></h3>
                        <p class="desc"><?php echo $info['emp_plantas_desc']; ?></p>
                        <div class="mq-plantas__options">
                            <a href="#emp-popup" class="btns__popup open-modal-emp mq-btn mq-btn--orange mq-btn--medium mq-plantas__a" data-effect="mfp-move-horizontal">Tenho Interesse <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_seta.png" class="icon-seta-sobre-empreedimento" alt="" data-effect="mfp-move-horizontal"></a>
                            <a href="<?php echo wp_get_attachment_url( $info["emp_plantas"][0], 'full' ); ?>" download class="mq-plantas__donwload mq-btn mq-btn--orange mq-btn--medium">Baixar Planta <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-dowload.png" class="icon-seta-sobre-empreedimento" alt="" data-effect="mfp-move-horizontal"></a>
                        </div>
                      
                    </div>
                    <figure class="mq-plantas__img-destaque">

                        <?php 
                            $url_planta = wp_get_attachment_url($info["emp_plantas"][0], 'thumb-home-galeria');
                            $image_attributes = wp_get_attachment_image_src($info["emp_plantas"][0], 'thumb-315x400');

                        if ($image_attributes) :
                            echo '<a class="link" href="' . $url_planta . '">';
                            echo '<img class="thumb" src="' . $image_attributes[0] . '" width="' . $image_attributes[1] . '" height="' . $image_attributes[2] . '" />';
                            echo '</a>';
                        endif; ?>



                    </figure>
                </div>
            <?php $i++; endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>