<div class="mq-scrollbar" id="scrollbar">
    <div class="container">
        <div class="mq-block-info">
            <div class="mq-block-info__item">
                <a href="tel:8532480065" class="" target="_blank" title="Ligue Agora">
                    <i class="mq-icon mq-icon--phone"></i>
                    <div class="mq-block-info__content">
                        <h4 class="title">Ligue agora</h4>
                        <h3 class="info">85 3248.0065</h3>
                    </div>
                </a>
            </div>
            <div class="mq-block-info__item">
                <?php $grupoCrm = get_post_meta(get_the_id(), 'empreendimento_dados_crm_anapro', true);
                if ($grupoCrm && !is_archive()) {
                    $chatCrm = $grupoCrm['campanha_emp_anapro_chat'];
                } else {
                    $chatCrm = 'https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=tUoYb_Nr5xA1&keyIntegradora=A1140F73-8DC5-4842-A37D-19C290686661&keyAgencia=947fc514-c40b-42db-9a96-be2903465043&strDir=marquise&campanha=DDW22iWmT041&canal=SqNI3zlxGsw1&produto=pXlAYk2_uU41&strmidia=Site+Marquise&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=true&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=';
                }
                ?>
                <a href="#" data-link="<?= $chatCrm; ?>" class="open-window" title="Atendimento Online">
                    <i class="mq-icon mq-icon--chat"></i>
                    <div class="mq-block-info__content">
                        <h4 class="title">Atendimento</h4>
                        <h3 class="info">Online</h3>
                    </div>
                </a>
            </div>
            <div class="mq-block-info__item">
                <a href="<?php echo get_permalink(get_page_by_path('fale-com-a-marquise')); ?>" class="" title="Atendimento por e-mail">
                    <i class="mq-icon mq-icon--mail"></i>
                    <div class="mq-block-info__content">
                        <h4 class="title">Atendimento</h4>
                        <h3 class="info">Por E-mail</h3>
                    </div>
                </a>
            </div>
            <!-- <div class="mq-block-info__item">
                <i class="mq-icon mq-icon--support"></i>
                <div class="mq-block-info__content">
                    <h4 class="title">Ligamos</h4>
                    <h3 class="info">Para Você</h3>
                </div>
            </div> -->
            <div class="mq-block-info__item">
                <a href="https://api.whatsapp.com/send?phone=5585992164148" class="" target="_blank" title="Whatsapp">
                    <i class="mq-whatsapp"></i>
                    <div class="mq-block-info__content">
                        <h4 class="title">Whatsaspp</h4>
                        <h3 class="info">85 99216.4148</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>