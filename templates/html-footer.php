<?php
if (isset($_GET['admin'])) {
    echo '<button class="cvtt-btn-trigger_sent">enviar</button>';
}
?>


</main>

<?php $settings = get_option('options_gerais'); ?>

<?php

get_template_part('templates/modal', 'scrollbar');


?>
<style>
    .mq-popup-emp .form-group__termos * {
        color: #505050 !important;
    }

    .mq-popup-emp .form-group__termos .texto,
    .mq-popup-emp .form-group__termos .texto a {
        font-size: 13px;
        text-align: left;
    }

    .notificationCookies {
        font-family: 'Arial', sans-serif;
        font-size: 14px;
    }



    @media only screen and (min-width: 767px) {

        .mq-btn--medium {
            max-width: 230px;
        }
    }

    .mq-btn--medium {
        border-radius: 7px;
        box-shadow: 0px 0px 10px #00000080;
    }
</style>
<footer class="mq-footer" id="footer">
     <div class="component-footer">
        <div class="container space-padding">
            <div class="mq-options__elements" style="display:flex;flex-direction:row;align-items:center;">
                <h1 class="">
                    <a href="<?php echo get_site_url(); ?>" title="Marquise Incorporação">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-new.png" alt="Marquise Incorporação" title="Marquise Incorporação" class="" style="width:60px">
                    </a>
                </h1>
                <p>Uma empresa do <a href="https://www.marquiseincorporacoes.com.br/">Grupo Marquise</a></p>
            </div>
        </div>
    </div>
    <div class="mq-footer__marquise">
        <div class="container space-padding">
            <div class="">
                <nav class="mq-nav-footer">

                    <div class="mq-nav-footer__menu <?php echo (wp_is_mobile() ? 'mq-nav-footer__menu--mobile' : ''); ?>">
                        <div class="mq-menu grid-footer">
                            <div class="footer-marquise_info">
                                <ul>
                                    <li>Av. Pontes Vieira, 1790</li>
                                    <li>Dionízio Torrens - Fortaleza = Ceará</li>
                                    <li>PABX: +55 85 4008.3322</li>
                                </ul>
                                <ul>
                                    <li>Av. Brigadeiro Faria Lima, 3015</li>
                                    <li>Conj. 41 - Jardim Paulista</li>
                                    <li>São Paulo - 01452-000</li>
                                    <li>+55 11 2985.1782</li>
                                </ul>
                            </div>
                            <?php wp_nav_menu(array('theme_location' => 'menu_2', 'menu_class' => 'mq-menu-rdp')); ?>
                        </div>
                        <div class="mq-nav-footer__div">
                            <a href="">Visite nosso Show Room</a>
                            <a href="">Central de Vendas +55 85 3248.0065</a>
                            <a href="">Relacionamento +55 85 4008.3331/3432</a>
                        </div>
                    </div>

                </nav>
                <!-- <div class="mq-grupo-empresas">
                    <h3 class="mq-grupo-empresas__title">Empresa pertencente ao Grupo Marquise:</h3>
                    <div class="mq-empresa">
                        <?php
                        $args = array('post_type' => 'empresas', 'showposts' => 14, 'orderby' => 'date', 'order' => 'DESC');
                        $empresas = new WP_Query($args);
                        while ($empresas->have_posts()) : $empresas->the_post();
                            $link = get_post_meta(get_the_id(), 'empd_url', true);
                            $target = get_post_meta(get_the_id(), 'empd_target', true);
                            $emp_thumb = get_the_post_thumbnail_url(get_the_ID(), 'full');
                        ?>
                            <?php if ($link) : ?>
                                <a class="mq-empresa__thumb" href="<?php echo $link; ?>" target="<?php echo $target; ?>">
                                    <figure>
                                        <img src="<?php echo $emp_thumb; ?>" title="<?php echo the_title(); ?>" alt="<?php the_title_attribute(array('before' => 'Saiba mais ')); ?>">
                                    </figure>
                                </a>
                            <?php else : ?>
                                <figure class="mq-empresa__thumb">
                                    <img src="<?php echo $emp_thumb; ?>" title="<?php echo the_title(); ?>" alt="<?php the_title_attribute(array('before' => 'Saiba mais ')); ?>">
                                </figure>
                            <?php endif; ?>

                        <?php endwhile;
                        wp_reset_postdata(); ?>
                    </div>
                </div> -->
            </div>

        </div>
    </div>
    <!-- <div class="mq-options mq-options--end">
        <div class="container">
            <div class="mq-options__elements">
                <h1 class="mq-footet-logo">
                    <a href="<?php echo get_site_url(); ?>" title="Marquise Incorporação">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/m-incorporacoes.svg" alt="Marquise Incorporação" title="Marquise Incorporação" class="" style="width:200px">
                    </a>
                </h1>

                <address class="mq-footer-endereco">


                    <h4 class="texto">Visite nosso Show Room</h4>
                    <h4 class="end">Av. Pontes Vieira. 1790 - Dionisio Torres - Fortaleza - Ceará</h4>
                    <div class="fones">
                        <div class="tel tel--rel">
                            <strong>Central de vendas</strong>
                            <span class="number">
                                <i class="fa fa-volume-control-phone"></i>
                                +55 85 3248.0065
                            </span>
                        </div>
                        <a class="btn" href="https://www.google.com/maps/dir//''/@-3.7518406,-38.5074672,17z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x7c748bfe321f091:0xe04463c3e60af8f8!2m2!1d-38.5052785!2d-3.751846!3e3" target="_blank">Como Chegar</a>
                        <div class="tel">
                            <strong>Relacionamento</strong>
                            <span class="number">
                                <i class="fa fa-volume-control-phone"></i>
                                +55 85 4008.3343
                                +55 85 4008.3331/3432
                            </span>
                        </div>
                    </div>

                            
                </address>

                <div class="mq-footer-midias">
                    <?php foreach ($settings['group_redes'] as $info) : ?>

                        <a href="<?php echo $info['link_midia']; ?>" class="mq-footer-midias__icone" title="Visite-nos"><i class="fa <?php echo $info['icone_midia'] ?>"></i></a>

                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div> -->
    <div class="mq-footer__marquise mq-footer__marquise--cvtt">
        <div class="container space-padding">
            <div class="footer-component-cvtt">
                <p>2021 Copyright - Todos os direitos reservados.</p>
                <a href="https://www.convertte.com.br/" title="Convertte">
                    <div class="mq-footer__marquise-thumb-cvtt"></div>
                </a>
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- <script src="https://marquise.housecrm.com.br/track/origem.js"></script> -->
<script>
    // submitSimulacao() - função invocada no submit do form simulação da home e pagina simulação
    function submitWebhook() {





        var nome = document.querySelector('[name=nome]').value;
        var email = document.querySelector('[name=email]').value;
        var fone = document.querySelector('[name=telefone]').value;
        var cpf = document.querySelector('[name=cpf]').value;
        var empreendimento = document.querySelector('[name=empreendimento]').value;
        var corretor = document.querySelector('[name=corretor]').value;
        var aceite = document.querySelector('.iptAceite').value;

        //var x      = hc_empreendimento ? hc_empreendimento : nome_empreendimento;
        var form = 'Cadastro Presencial';


        $.ajax({
            url: 'https://webhook.site/78e6f2f9-7875-4be4-a4e9-5d6006a0552e',
            type: "POST",
            data: {
                "origem": 'Simulador',
                "nome": nome,
                "email": email,
                "fone": fone,
                "cpf": cpf,
                "emp": empreendimento,
                "corretor": corretor,
                "data": form,
                "aceite": aceite,
            },
            complete: function() {
                console.log('Simulação realizada com sucesso!');
            }
        });
    }

    // ALEX - Esse form é de uma página de cadastro presencial, ainda não foi finalizada
    // document.addEventListener('wpcf7submit', function(event) {
    //     if ('3945' == event.detail.contactFormId) {
    //         formCadastroPresencial();
    //     }
    // }, false);
</script>
<?php wp_footer(); ?>
<script>
    new WOW().init();
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyCFAagbLnjO5MuDzViOOiqttGvfPz1wV6c"></script>
<script>
    jQuery(function($) {
        $(document).ready(function() {



            /*=========================================================================================
              // EQUAL HEIGHT
              =========================================================================================*/

            function equalizeHeights(selector) {
                var heights = new Array();

                // Loop to get all element heights
                $(selector).each(function() {

                    // Need to let sizes be whatever they want so no overflow on resize
                    $(this).css('min-height', '0');
                    $(this).css('max-height', 'none');
                    $(this).css('height', 'auto');

                    // Then add size (no units) to array
                    heights.push($(this).height());
                });

                // Find max height of all elements
                var max = Math.max.apply(Math, heights);

                // Set all heights to max height
                $(selector).each(function() {
                    $(this).css('height', max + 'px');
                });
            }



            $(window).on('load resize', function() {

                equalizeHeights('.owl-certificados .item .text');

            });

        })
    })


    $(document).ready(function() {
        $('.owl-carousel').owlCarousel();
        //destaques
        $('.mq-corretor__destaques .wrap-slide').owlCarousel({
            mouseDrag: true,
            navText: false,
            loop: true,
            autoplay: false,
            margin: 25,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                    dots: true
                },
                600: {
                    items: 2,
                    nav: true,
                    dots: true
                },
            }
        });
    });
</script>



</body>

</html>