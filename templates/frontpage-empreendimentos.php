<section class="mq-section">
    <div class="container space-padding">
        <div class="mq-empreendimentos-all">

            <?php get_template_part('templates/empreendimento', 'busca'); ?>

            <div class="mq-empreendimentos">
                <?php $emp = new WP_Query(array('post_type' => 'mar_empreendimentos', 'meta_key' => 'emp_destaque', 'meta_value' =>  1, 'showposts' => 7, 'order' => 'DESC',array(
                            'taxonomy' => 'tipo-empreendimentos',
                            'terms' => 39,
                            'field' => 'comercial',
                        )));
                while ($emp->have_posts()) : $emp->the_post();
                    include(locate_template('templates/loop-empreendimento.php'));
                endwhile;
                wp_reset_postdata();  ?>
                <div class="mq-empreendimentos__ver-todos"><a href="<?php echo get_post_type_archive_link('mar_empreendimentos'); ?>" title="Ver todos" class="mq-btn mq-btn--orange mq-btn--medium component-saiba-mais">Ver Todos <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_seta.png" class="icon-seta" alt=""></a></div>
            </div>

        </div>
    </div>
</section>