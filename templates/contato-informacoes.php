<section class="mq-info-contato">
    <div class="container">
        <div class="mq-informacoes">
            <div class="mq-informacoes__sugestao">
                <header class="mq-informacoes__head">
                    <div class="border"></div>
                    <h3 class="title">Gostaria de fazer solicitações, sugestões ou reclamações?</h3>
                    <h4 class="subtitle">Escolha uma forma de contato</h4>
                </header>
                <div class="mq-informacoes-geral">
                    <div class="mq-informacoes-geral__option">
                        <i class="mq-geo-icon mq-geo-icon--phone"></i>
                        <p class="mq-geo__txt">PABX: <span>+55 85 4008.3322</span></p>
                    </div>
                    <div class="mq-informacoes-geral__option">
                        <i class="mq-geo-icon mq-geo-icon--support"></i>
                        <p class="mq-geo__txt">Central de vendas: <span>+55 85 3248.0065</span></p>
                    </div>
                    <div class="mq-informacoes-geral__option">
                        <i class="mq-geo-icon mq-geo-icon--chat"></i>
                        <!-- <p class="mq-geo__txt">Relacionamento <span>+ 55 85 4008.3343</span></p> -->
                        <p class="mq-geo__txt">Relacionamento <span>+55 85 4008.3331/3432</span></p>
                    </div>
                    <div class="mq-informacoes-geral__option">
                        <i class="mq-geo-icon mq-geo-icon--mail"></i>
                        <p class="mq-geo__txt">comercial<span>@construtoramarquise.com.br</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>