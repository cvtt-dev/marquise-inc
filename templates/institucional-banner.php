<?php      

    $img = get_the_post_thumbnail_url(get_the_ID(), 'full');  
    $title_destaque = get_post_meta(get_the_ID(), 'title_destaque', true);
    $txtBtn = get_post_meta(get_the_ID(), 'txtBtn', true);
    $linkBtn = get_post_meta( get_the_id(), 'linkBtn', true);    

?>

<section class="mq-banner-institucional" style="background-image: url('<?php echo $img; ?>'); ">
    <div class="mq-banner-institucional__wrap">
        <div class="container">
            <div class="mq-banner-institucional__content">
                <h3 class="mq-banner-institucional__title-destaque"><?php the_title(); ?></h3>
                <h2 class="mq-banner-institucional__title"><?php echo $title_destaque; ?></h2>
                <div class="mq-banner-institucional__content"><?php the_content(); ?></div>
                <a href="<?php echo $linkBtn; ?>" class="mq-btn mq-btn--auto mq-btn--orange"><?php echo $txtBtn; ?></a>
            </div>
        </div> 
    </div> 
</section>

