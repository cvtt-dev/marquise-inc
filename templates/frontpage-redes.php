<section class="mq-section">
    <div class="container">
        <header class="mq-header-redes">
            <i class="icone-redes"></i>
            <h2 class="mq-header-redes__title">Novidades em nossas redes sociais</h2>
        </header>
        <div class="mq-galeria-instagram">
        <?php 
            if(wp_is_mobile()):
                echo do_shortcode('[instagram-feed]');
            else:
                echo do_shortcode('[instagram-feed]');
            endif;
        ?>
        </div>
    </div>
</section>