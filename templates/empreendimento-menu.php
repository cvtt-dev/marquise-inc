<div class="mq-emp-overview">
    <div class="container">

        <nav class="mq-emp-options__nav">
            <ul>
                <li class="item"><a class="link" href="#visao" title="Visão Geral">Visão Geral</a></li>
                <div class="divisor divisor--gray"></div>
                <li class="item"><a class="link" href="#video" title="Vídeo">Vídeo</a></li>
                <div class="divisor divisor--gray"></div>
                <li class="item"><a class="link" href="#info" title="Info">Detalhes</a></li>
                <div class="divisor divisor--gray"></div>
                <li class="item"><a class="link" href="#fotos" title="Fotos">Imagens</a></li>
                <div class="divisor divisor--gray"></div>
                <li class="item"><a class="link" href="#plantas" title="Plantas">Plantas</a></li>
                <div class="divisor divisor--gray"></div>
                <li class="item"><a class="link" href="#ficha" title="Plantas">Ficha Técnica</a></li>
            </ul>
        </nav>
    </div>
</div>
