<?php
$emp_galeria = get_post_meta(get_the_id(), 'img_emp_galeria', false);
?>

<?php if ($emp_galeria) : ?>
    <section class="mq-section mq-section--emp-galeria" id="fotos">
        <div class="container">
            <h2 class="galeria-title">Galeria <span><?php echo get_the_title(); ?></span></h2>
        </div>
        <div class="mq-emp-galeria owl-galeria-emp" id="emp-galeria">

            <?php 
                //$count = 0;
                foreach ($emp_galeria as $img) : 
                
                $caption = wp_get_attachment_caption($img); ?>

                <figure class="mq-emp-galeria__figure">

                    <?php echo '<a class="link-img" href="' . wp_get_attachment_url($img, 'full') . '" title="' . ($caption ? $caption : 'Marquise Incorporações') . '">';
                        echo wp_get_attachment_image($img, 'thumb-galeria');
                        echo '<legend class="legend"><p class="desc">' . $caption . '</p></legend>';
                        echo '</a>';
                    ?>

                   

                </figure>



            <?php 
            //$count++; if( $count == -1 ): break; endif; 
            endforeach; ?>

        </div>

    </section>
<?php endif; ?>