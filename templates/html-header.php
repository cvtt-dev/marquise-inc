<?php get_template_part('templates/html', 'head'); ?>


<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Manrope:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
<script>
    window.addEventListener("scroll", (event) => {
        let scroll = this.scrollY;
        var element = document.querySelector(".mq-header");
        if (scroll > 40) {
            element.classList.add("redu");
        } else {

            element.classList.remove("redu");
        }

    });
</script>

<button class="btn-nav-mobile">
    <span></span>
</button>
<nav class="navmobile">
    <?php bem_menu('menu_3', 'mq-menu', 'mq-menu--mobile'); ?>
</nav>

<header class="mq-header">

    <div class="mq-menu-bar-bg">


        <div class="container">

            <div class="mq-header-child">

                <h1 class="mq-header-child__logo">
                    <a href="<?php echo get_site_url(); ?>" title="Marquise Incorporação">
                        <img style="height: auto; width:160px;" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-marquise.png" alt="Marquise Incorporação" title="Marquise Incorporação" class="">
                    </a>
                </h1>



                <nav id="menu" class="mq-header-child__nav">
                    <?php wp_nav_menu(array('theme_location' => 'menu_1', 'menu_class' => 'mq-header-child__menu')); ?>

                    <span class="header-child__location">
                        <img class="header-child__img" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-location.png" alt="">
                        <select name="" class="mq-header-select">
                            <option value="<?php  echo get_site_url().'#ce'; ?>" data-url="ceara" onclick="cookieLocation('brazil','ceara')" >Fortaleza</option>
                            <option value="<?php  echo get_site_url().'#sp'; ?>" data-url="sao-paulo" onclick="cookieLocation('brazil','sao-paulo')">São Paulo</option>
                        </select>
                    </span>

                </nav>

                <div class="mq-header-child__informacoes">



                    <?php session_start(); ?>
                    <?php if ($_SESSION['xml']) : ?>
                        <a href="<?php echo get_bloginfo('url'); ?>/area-do-cliente" title="Área do Cliente" class="btn-area-menu link-top"><i class="icone fa fa-user-circle-o" aria-hidden="true"></i>Área do Cliente</a>

                    <?php else : ?>

                        <a href="#cliente_popup" title="Área do Cliente" class="btn-area-menu link-top open-modal-cliente" data-effect="mfp-zoom-in">
                            <i class="icone fa fa-user-circle-o" aria-hidden="true"></i>
                            <h4 class="title">Área do Cliente</h4>

                            </li>
                        <?php endif; ?>

                        <a href="http://parceriasmarquise.housecrm.com.br/parceiro/" title="Área do Corretor" class="btn-area-menu  link-top" target="_blank">
                            <i class="icone fa fa-briefcase" aria-hidden="true"></i>
                            <h4 class="title">Área do Corretor</h4>
                        </a>

                        <a href="#busca-emp" id="busca" title="Buscar" class="search open-modal-emp search-form-header" data-effect="mfp-move-horizontal">
                            <i class="fa fa-search"></i>
                            <span class="search-form-header__span"> buscar</span>
                        </a>
                        

                        <div id="cliente_popup" class="mfp-modal mfp-modal--formCliente mfp-hide">
                            <h3 class="title-form-cliente">Área do cliente</h3>
                            <form id="area_cliente" role="form" class="mq-modal-cliente" method="POST" onsubmit="return false;">
                                <div class="login_msg"></div>
                                <div class="mq-modal-cliente__form-group">
                                    <input type="text" class="form-control" id="login" name="login" placeholder="CPF ou Código de Cliente">
                                </div>
                                <div class="mq-modal-cliente__form-group">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Senha">
                                </div>
                                <button type="submit" class="mq-modal-cliente__btn btn">Entrar <i id="cliente-login-load" class="fa fa-cog fa-spin hidden"></i></button>
                            </form>
                            <a href="#" class="mq-modal-cliente__reset-password" id="reset-password">Esqueci minha senha</a>
                            <form id="form-reset-password" role="form" action="" class="mq-modal-cliente mq-modal-cliente--forget-password" method="POST" onsubmit="return true;">
                                <div class="login_msg"></div>
                                <div class="mq-modal-cliente__form-group mq-modal-cliente__form-group-pass">
                                    <input type="text" class="form-control" id="email_reset" name="email" placeholder="Digite seu e-mail">
                                </div>
                                <h3 class="mq-modal-cliente__title-pass">Digite seu e-mail para receber informações sobre o login</h3>
                                <button type="submit" class="mq-modal-cliente__btn btn" id="reset">Enviar <i id="cliente-login-load" class="fa fa-cog fa-spin hidden"></i></button>
                            </form>
                        </div>


                        <div id="busca-emp" class="mfp-modal mfp-modal--emp mfp-hide">
                            <h3 class="title">Busque seu empreendimento</h3>
                            <form class="mq-form-empreendimento mq-form-empreendimento--busca" role="search" method="get" id="searchform" action="<?php echo home_url(); ?>/busca-empreendimento/">
                                <div class="mq-form-empreendimento__field">
                                    <?php wp_dropdown_categories(
                                        array(
                                            'show_option_none'  => 'Tipo',
                                            'name'              => 'tipo-empreendimentos',
                                            'show_count'        => 0,
                                            'orderby'           => 'name',
                                            'echo'              => 1,
                                            'taxonomy'          => 'tipo-empreendimentos',
                                            'class'             => 'emp_field',
                                        )
                                    );
                                    ?>
                                </div>
                                <div class="mq-form-empreendimento__field">
                                    <?php wp_dropdown_categories(
                                        array(
                                            'show_option_none'  => 'Dormitórios',
                                            'name'              => 'qdtdomitorios-empreendimentos',
                                            'show_count'        => 0,
                                            'orderby'           => 'name',
                                            'echo'              => 1,
                                            'taxonomy'          => 'qdtdomitorios-empreendimentos',
                                            'class'             => 'emp_field',
                                        )
                                    );
                                    ?>
                                </div>
                                <div class="mq-form-empreendimento__field">
                                    <?php wp_dropdown_categories(
                                        array(
                                            'show_option_none'  => 'Estágio da obra',
                                            'name'              => 'status-empreendimentos',
                                            'show_count'        => 0,
                                            'orderby'           => 'name',
                                            'echo'              => 1,
                                            'taxonomy'          => 'status-empreendimentos',
                                            'class'             => 'emp_field',
                                        )
                                    );
                                    ?>
                                </div>
                                <div class="mq-form-empreendimento__field">
                                    <?php wp_dropdown_categories(
                                        array(
                                            'show_option_none'  => 'Bairro',
                                            'name'              => 'bairro-empreendimentos',
                                            'show_count'        => 0,
                                            'orderby'           => 'name',
                                            'echo'              => 1,
                                            'taxonomy'          => 'bairro-empreendimentos',
                                            'class'             => 'emp_field',
                                        )
                                    );
                                    ?>
                                </div>
                                <div class="mq-form-empreendimento__field">
                                    <button type="submit" class="emp_field btn-busca" id="searchsubmit" value="Search">Buscar <i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- <form action="" class="search-form-header">
                        <button class="form-header-search__button" type="submit" value="Search"><i class="fa fa-search"></i></button>
                        <input class="form-header-search__input" type="search" placeholder="Buscar">  
                    </form> -->
                    <!-- <a href="#busca-emp" id="busca" title="Buscar" class="search open-modal-emp pesquisa-menu" data-effect="mfp-move-horizontal"><i class="fa fa-search" aria-hidden="true"></i>Buscar</a> -->
                </div>
            </div>
            
        </div>
        
    </header>
    
<main class="main" role="main">