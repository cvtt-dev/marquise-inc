<?php
$short_code_trab = get_post_meta(get_the_id(), 'short_code_form', true);
?>
<section class="mq-trabalhe-conosco-form">
    <div class="container">
        <header class="mq-header-geo">
            <h2 class="title">Venha trabalhar na Marquise</h2>
            <h3 class="desc">Cadastre seu currículo</h3>
        </header>
        <div class="mq-geo">

            <div class="mq-geo__form">
                <?php // echo do_shortcode($short_code_trab); ?>
                <a href="https://www.catho.com.br/empregos/construtoramarquise" target="_blank" class="mq-btn-curriculum">Cadastrar currículo</a>
            </div>

            <div class="mq-geo__maps">
                <iframe class="mq-geo__iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.267052087427!2d-38.507264784721684!3d-3.75192314434444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c748bfe23c832b%3A0x15f53174759ff47b!2sGrupo+Marquise!5e0!3m2!1spt-BR!2sbr!4v1555956393889!5m2!1spt-BR!2sbr" width="100%" height="650" frameborder="0" style="border:0" allowfullscreen></iframe>
                <div class="mq-geo__endereco">
                    <i class="mq-geo-icon mq-geo-icon--local"></i>
                    <p class="mq-geo__txt">Av. Pontes Vieira, 1790 - Dionisio Torres - <span>Fortaleza - Ceará - PABX: +55 85 4008.3322</span></p>
                </div>
                <div class="mq-geo__btns-options">
                    <a href="https://goo.gl/maps/eDESpYPu1UC9vhmu6" target="_blank" class="btn-geo btn-geo--maps">Abrir no Maps</a>
                    <a href="#" class="btn-geo">Abrir no Waze</a>
                </div>
            </div>
        </div>
    </div>
</section>