<?php
$bairro = get_post_meta(get_the_id(), 'bairro', true);
$img_banner = get_post_meta(get_the_ID(), 'img_thumb', false);
$url_tour = get_post_meta(get_the_ID(), 'tour', true);
$descricao = get_post_meta(get_the_ID(), 'descricao', true);

// -----
$tamanho = get_post_meta(get_the_id(), 'tamanho', true);
$qtdquartos = get_post_meta(get_the_id(), 'qtdquartos', true);
$qtdgaragem = get_post_meta(get_the_id(), 'qtdgaragem', true);
$bairro = get_post_meta(get_the_id(), 'bairro', true);


$groupAnapro = get_post_meta(get_the_ID(), 'empreendimento_dados_crm_anapro', true);

?>
<?php foreach ($img_banner as $img) :
    $url = wp_get_attachment_url($img);
?>
<?php endforeach; ?>

<section id="empDataAnapro" data-codemp="<?= $groupAnapro['cod_emp_anapro']; ?>" data-codcamp="<?= $groupAnapro['campanha_emp_anapro']; ?>"  class="mq-banner-empreendimento" style="background-image: url('<?php echo $url; ?>'); ">

    <div class="mq-banner-empreendimento__destaque">
        <div class="container">

            <div class="mq-banner-empreendimento__content">
                <div class="mq-banner-empreendimento__info">
                    <hr>
                    <h4 class="local"><?php echo $bairro; ?></h4>
                    <h3 class="title"><?php the_title(); ?></h3>
                    <div class="descricao">
                        <?php echo $descricao; ?>
                    </div>
                    <div class="btns">

                        <?php if ($url_tour) :
                            echo '<a href="' . $url_tour . '" target="_blank" class="mq-btn--tour mq-btn mq-btn--orange mq-btn-medium"><i class="icone360"></i>Tour Virtual</a>';
                        endif; ?>

                        <a href="#emp-popup" class="btns__popup open-modal-emp mq-btn mq-btn--orange mq-btn-medium" data-effect="mfp-move-horizontal">Tenho Interesse</a>

                        <div id="emp-popup" class="mq-popup-emp mfp-modal mfp-modal--emp-mobile mfp-hide">
                            <h3 class="mq-popup-emp__top-title">Mais Informações</h3>
                            <h4 class="mq-popup-emp__title">sobre seu <?php echo get_the_title(); ?></h4>
                            <?php echo do_shortcode('[contact-form-7 id="2578" title="FORM: POPUP - EMPREENDIMENTO MOBILE"]'); ?>
                        </div>
                    </div>
                </div>
                <div class="mq-banner-empreendimento__form">
                    <div class="mq-emp-form">
                        <h3 class="mq-emp-form__top-title">Mais Informações</h3>
                        <h4 class="mq-emp-form__title">sobre seu <span class='mq-emp-form__text'><?php echo get_the_title(); ?></span></h4>
                        <?php echo do_shortcode('[contact-form-7 id="2477" title="FORM: INFORMAÇÕES DO EMPREENDIMENTO"]'); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- <div class="mq-banner-empreendimento__barra">
        <div class="container">

            <div class="detalhes">
                <div class="item">

                    <div class="item-icone">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <h5 class="title">Local</h5>
                    </div>

                    <h4 class="toptitle"><?php //echo $bairro; ?></h4>
                </div>
                <div class="divisor"></div>
                <div class="item">

                    <div class="item-icone">
                        <i class="cvtt-icon-ruler"></i>
                        <span class="title">Área</span>
                    </div>
                    <h4 class="toptitle"><?php //echo $tamanho; ?></h4>

                </div>
                <?php //if ($qtdquartos) : ?>
                    <div class="divisor"></div>
                    <div class="item">
                        <div class="item-icone">
                            <i class="cvtt-icon-bed" aria-hidden="true">
                                <span class="title">Quartos</span>
                            </i>
                        </div>
                        <h4 class="toptitle"><?php //echo $qtdquartos; ?></h4>
                    </div>
                <?php //endif; ?>
                <?php //if ($qtdgaragem) : ?>
                    <div class="divisor"></div>
                    <div class="item">
                        <div class="item-icone">
                            <i class="cvtt-icon-car" aria-hidden="true">
                                <span class="title">Vagas</span>
                            </i>
                        </div>
                        <h4 class="toptitle"><?php //echo $qtdgaragem ?></h4>
                    </div>
                <?php //endif; ?>
            </div>

        </div>
    </div> -->


</section>