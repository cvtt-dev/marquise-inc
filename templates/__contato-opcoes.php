<section class="mq-contato-opcoes">
    <div class="container">
        <div class="mq-geo-options">
            <a href="https://api.whatsapp.com/send?phone=5585992164148" class="mq-geo-options__link" target="_blank">
                <div class="mq-geo-options__item">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/whats.png" alt="Whatsapp" title="Whatsapp">
                    <h3 class="title">Whatsapp</h3>
                </div>
            </a>

            <?php if (is_page('venda-seu-terreno')) : ?>
                <a href="<?php echo get_permalink(get_page_by_path('fale-com-a-marquise')); ?>" class="mq-geo-options__link">
                    <div class="mq-geo-options__item">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/fale-conosco.png" alt="Fale conosco" title="Fale conosco">
                        <h3 class="title">Fale Conosco</h3>
                    </div>
                </a>
            <?php else : ?>
                <a href="<?php echo get_permalink(get_page_by_path('venda-seu-terreno')); ?>" class="mq-geo-options__link">
                    <div class="mq-geo-options__item">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/metro.png" alt="Metro" title="Metro">
                        <h3 class="title">Venda seu terreno</h3>
                    </div>
                </a>
            <?php endif; ?>

            <?php if (is_page('trabalhe-conosco')) : ?>
                <a href="<?php echo get_permalink(get_page_by_path('fale-com-a-marquise')); ?>" class="mq-geo-options__link">
                    <div class="mq-geo-options__item">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/fale-conosco.png" alt="Fale conosco" title="Fale conosco">
                        <h3 class="title">Fale Conosco</h3>
                    </div>
                </a>
            <?php else : ?>
                <a href="<?php echo get_permalink(get_page_by_path('trabalhe-conosco')); ?>" class="mq-geo-options__link">
                    <div class="mq-geo-options__item">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/trab-conosco.png" alt="Trabalhe conosco" title="Trabalhe conosco">
                        <h3 class="title">Trabalhe Conosco</h3>
                    </div>
                </a>
            <?php endif; ?>

            <a href="<?php echo get_permalink(get_page_by_path('simulacao')); ?>" class="mq-geo-options__link">
                <div class="mq-geo-options__item">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/simulador.png" alt="Simulador" title="Simulador">
                    <h3 class="title">Simulador</h3>
                </div>
            </a>
        </div>
    </div>
</section>