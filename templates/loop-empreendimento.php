<?php $tipo = wp_get_post_terms(get_the_id(), 'tipo-empreendimentos'); ?>
<?php $status = wp_get_post_terms(get_the_id(), 'status-empreendimentos'); ?>
<?php $tamanho = get_post_meta(get_the_id(), 'tamanho', true); ?>
<?php $qtdquartos = get_post_meta(get_the_id(), 'qtdquartos', true); ?>
<?php $qtdgaragem = get_post_meta(get_the_id(), 'qtdgaragem', true); ?>
<?php $bairro = get_post_meta(get_the_id(), 'bairro', true); ?>
<?php $tabela = get_post_meta(get_the_id(), 'emp_tabela', true); ?>
<?php $emp_percentual = get_post_meta(get_the_id(), 'emp_percentual', true); ?>
<?php $img_emp_galeria = get_post_meta(get_the_id(), 'img_emp_galeria', false); ?>
<?php $proximities = get_post_meta(get_the_id(), 'proximities', true); ?>



<div class="mq-empreendimento">
    <?php if(wp_is_mobile()): ?>
    <div class="mq-empreendimento__img">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(array('before' => 'Saiba mais sobre o empreendimento ')); ?>">
            <?php the_post_thumbnail('thumb-emp', array("class" => "mq-empreendimento__thumb", 'alt' => get_the_title(), 'title' => get_the_title())); ?>
            <div class="status">
                <?php echo $status[0]->name; ?>
            </div>
        </a>
    </div>
    <?php endif; ?>
    <div class="mq-empreendimento__info">
        <div class="mq-empreendimento-component__flex">
        <a class="overlay-link-emp" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(array('before' => 'Saiba mais sobre o empreendimento ')); ?>">  </a>
            <div class="head">
                <div class="title">
                    <h3 class="mq-empreendimento-component__h3"><?php the_title(); ?></h3>
                    <hr>
                    <!-- <div class="tipo">
                        <span><?php //echo $tipo[0]->name ?></span>
                    </div> -->
                </div>
            </div>
            <div class="informacoes">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(array('before' => 'Saiba mais sobre o empreendimento ')); ?>">
                    <span class="local">
                        <!-- <i class="fa fa-map-marker" aria-hidden="true"></i> -->
                        <?php echo $bairro; ?>
                    </span>
                </a>
            </div>

            <span class="proximities"><?php echo $proximities; ?></span>
            
            <div><a href="<?php the_permalink(); ?>"  class="mq-btn--saiba-mais" target="_blank">Conheça <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_seta.png" class="icon-seta" alt=""></a></div>   

    </div>

        <div class="desc">
                
                <?php  if($tamanho) : ?>
                <div class="desc__option desc__option--tamanho">
                    <div class="componente-empreendimentos-new__icons">
                        <!-- <div class="metragem"> -->
                            <!-- <i class="cvtt-icon-ruler" aria-hidden="true">
                                <span class="title">Área</span>
                            </i> -->
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_mtwo.png" alt="">
                        <!-- </div> -->
                        <div class="subtitle"><?php echo $tamanho; ?></div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($qtdquartos) : ?>
                <div class="desc__option desc__option--suites">
                    <div class="componente-empreendimentos-new__icons">
                    <!-- <i class="cvtt-icon-bed" aria-hidden="true">
                        <span class="title">Quartos</span>
                    </i> -->
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_bath.png" alt="">
                        <div class="subtitle"><?php echo $qtdquartos.' Quartos'; ?></div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($qtdquartos) : ?>
                <div class="desc__option desc__option--vagas">
                    <div class="componente-empreendimentos-new__icons">
                    <!-- <i class="cvtt-icon-car" aria-hidden="true">
                        <span class="title">Vagas</span>
                    </i> -->
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_car.png" alt="">
                    <div class="subtitle"><?php echo $qtdgaragem.' Vagas'; ?></div>
                    </div>
                </div>
                <?php endif; ?>
            </div>

    </div>
    
    <div class="mq-empreendimento__owl">
        
    <?php if( $emp_percentual ): ?><div class="status"><?php echo $emp_percentual; ?></div><?php endif;?>
        <div class="mq-emp-owl">
            
            <div class="mq-emp-owl__galeria owl-galeria-emp">
                
                <?php //$count = 0; 
                    foreach ($img_emp_galeria as $img) : 
                    $caption = wp_get_attachment_caption( $img ); ?>
                
                   
                    <?php echo '<a class="mq-emp-owl__link" href="' . wp_get_attachment_url($img, 'thumb-home-galeria') . '" title="' . ($caption ? $caption : 'Marquise Incorporações') . '">';
                          echo wp_get_attachment_image($img, 'thumb-home-galeria');
                          echo '<legend class="legend"><p class="desc">' . $caption . '</p></legend>'; 
                          echo '</a>';
                          ?>

                   
                <?php //$count++; if( $count == -1 ): break; endif;  
                endforeach; ?>                    
            </div>

        </div>
    </div>
    
</div>