<?php      

    $img_destaque = get_post_meta(get_the_ID(), 'img_destaque', false);
    $id_video = get_post_meta(get_the_ID(), 'id_video', true);
    $title_sobre = get_post_meta(get_the_ID(), 'title_sobre', true);
    $desc_sobre = get_post_meta(get_the_ID(), 'desc_sobre', true);


?>

<section class="mq-sobre" id="sobre">
    <div class="container">
        <div class="mq-sobre__content">

            <figure class="thumb">
            
                <?php foreach ($img_destaque as $img) : 
                    
                    $caption = wp_get_attachment_caption( $img ); ?>
                        
                    <a href="#video_popup" class="open-modal-video play" data-effect="mfp-zoom-in" data-video="<?php echo $id_video; ?>">
                        <?php echo wp_get_attachment_image($img, 'full'); ?>
                    </a>
                    
                <?php endforeach; ?>	
                        
            </figure>

            <div id="video_popup" class="mfp-modal mfp-modal--video mfp-hide"></div>
            
            <div class="legend">
                <h2 class="title"><?php echo $title_sobre; ?></h2>
                <div class="txt"><?php echo $desc_sobre; ?></div>
            </div>
            
            
        </div>
    </div>
</section>