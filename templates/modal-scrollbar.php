<a href="#iconContato" class="mp-slider--icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-message.png" class="mp-slider-icon__img" alt="icon-message"><pan class="mp-slider-icon__span">1</span></s></a>

<div id="iconContato" class="modal-contato-icon mfp-hide">
    <div class="modal-contato-icon__line"></div>
    <h2 class="modal-contato-icon__title">Se interessou ?</h2>
    <h2 class="modal-contato-icon__h2">Fale com a gente</h2>
    <p class="modal-contato-icon__p">Escola a melhor forma de contato para falarmos com você</p>

    <div>
        <a href="tel:8532480065"  target="_blank" title="Ligue Agora">
            <div class="modal-contato-icon-item">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-phone.png" alt="" class="moda-contato-icon-item__img">
                <div class="modal-contato-icon-div">
                    <span class="modal-contato-icon__span">Ligue agora</span>
                    <h3 class="modal-contato-icon__h3">85 3248.0065</h3>
                </div>
            </div>
        </a>
        <?php $grupoCrm = get_post_meta(get_the_id(), 'empreendimento_dados_crm_anapro', true);
        if ($grupoCrm && !is_archive()) {
            $chatCrm = $grupoCrm['campanha_emp_anapro_chat'];
        } else {
            $chatCrm = 'https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=tUoYb_Nr5xA1&keyIntegradora=A1140F73-8DC5-4842-A37D-19C290686661&keyAgencia=947fc514-c40b-42db-9a96-be2903465043&strDir=marquise&campanha=DDW22iWmT041&canal=SqNI3zlxGsw1&produto=pXlAYk2_uU41&strmidia=Site+Marquise&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=true&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=';
        }
        ?>
        <a href="" data-link="<?= $chatCrm; ?>" >
            <div class="modal-contato-icon-item">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-chat.png" alt="" class="moda-contato-icon-item__img">
                <div class="modal-contato-icon-div">
                    <span class="modal-contato-icon__span">Atendimento online</span>
                </div>
            </div>
        </a>
        <a href="<?php echo get_permalink(get_page_by_path('fale-com-a-marquise')); ?>" data-link="<?= $chatCrm; ?>" >
            <div class="modal-contato-icon-item">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-email.png" alt="" class="moda-contato-icon-item__img">
                <div class="modal-contato-icon-div">
                    <span class="modal-contato-icon__span">Atendimento por e-mail</span>
                </div>
            </div>
        </a>
        <a href="https://api.whatsapp.com/send?phone=5585992164148" data-link="<?= $chatCrm; ?>" >
            <div class="modal-contato-icon-item">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-wpp.png" alt="" class="moda-contato-icon-item__img">
                <div class="modal-contato-icon-div">
                    <span class="modal-contato-icon__span">Whatsapp</span>
                    <h3 class="modal-contato-icon__h3">85 99216.4148</h3>
                </div>
            </div>
        </a>
    </div>

</div>