<?php
    $img = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

<section class="mq-banner-simulacao" style="background-image: url('<?php echo $img; ?>'); ">
    <div class="mq-banner-simulacao__wrap">
        <div class="container">
            <div class="mq-banner-simulacao__content">
                <h4 class="mq-banner-simulacao__title-destaque">Ideal para você</h4>
                <h3 class="mq-banner-simulacao__title"><?php the_title(); ?></h3>
                <div class="mq-banner-simulacao__desc"><?php the_content(); ?></div>
            </div>
            <div class="mouse">
                <div class="mouse-wheel"></div>
                <div class="arrows"></div>
            </div>
        </div>
    </div>
</section>