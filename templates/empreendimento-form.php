<section class="mq-section mq-section--interesse-form">
    <div class="container">
        <div class="mq-emp-form-interesse">

            <header class="mq-emp-form-interesse__header">
                <h4 class="toptitle">Tem interesse neste <strong>empreendimento ?</strong></h4>
                <h2 class="title">Deixe seu contato para que possamos enviar mais informações e condições especiais</h2>
            </header>
            <div class="mq-emp-form-interesse__contato">
                <?php echo do_shortcode('[contact-form-7 id="2488" title="FORM: DETALHES DO EMPREENDIMENTO"]'); ?>
            </div>
        </div>
    </div>
</section>