<?php
    $play_video = get_post_meta(get_the_ID(), 'play_video', true);
    $title_conceito = get_post_meta(get_the_ID(), 'title-conceito', true);
    $desc_conceito = get_post_meta(get_the_ID(), 'desc_conceito', true);

?>
                
<?php if ( $play_video == true ) : ?>
    <section class="section-clip">
        <div class="container">
            <div class="section-clip-flex">
                <div class="section-clip-text">
                    <h2><?php echo $title_conceito;?></h2>
                    <div></div>
                    <p><?php echo $desc_conceito; ?></p>
                </div>
                <?php if ($play_video) : ?>
                    <div class="section-clip-video">
                        <a href="#video_popup" class="open-modal-video play" data-effect="mfp-zoom-in" data-effect="mfp-zoom-in" data-video="<?php echo $play_video; ?>"><img class="section-clip-video__play" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-play.png"></a>
                    </div>
                <?php endif; ?>
            </div>
            <div id="video_popup" class="mfp-modal mfp-modal--video mfp-hide"></div>
        </div>
    </section>
<?php endif; ?>