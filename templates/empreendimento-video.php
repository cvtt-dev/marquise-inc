<?php
    $play_video = get_post_meta(get_the_ID(), 'play_video', true);
    $title_conceito = get_post_meta(get_the_ID(), 'title-conceito', true);
    $desc_conceito = get_post_meta(get_the_ID(), 'desc_conceito', true);
    $img_trab = get_post_meta(get_the_ID(), 'img_trab', false);
    $emp_detalhes = get_post_meta(get_the_ID(), 'empreendimento_caracteristicas', false);

?>
<?php if ( $play_video == true ||  $img_trab == true ) : ?>
    <section class="mq-section mq-section--conceito" id="video">
        <div class="mq-emp-video">

            <?php if ($play_video) : ?>
                <figure class="thumb">
                    <?php foreach ($img_trab as $img) :
                        $caption = wp_get_attachment_caption($img); ?>
                        <a href="#video_popup" class="open-modal-video play" data-effect="mfp-zoom-in" data-video="<?php echo $play_video; ?>">
                            <?php echo wp_get_attachment_image($img, 'full', "", array("class" => "img", 'alt' => get_the_title(), 'title' => get_the_title())); ?>
                        </a>
                    <?php endforeach; ?>
                </figure>
                <div id="video_popup" class="mfp-modal mfp-modal--video mfp-hide"></div>
            <?php else: ?>
                <figure class="thumb">
                    <?php foreach ($img_trab as $img) :
                        $caption = wp_get_attachment_caption($img); ?>
                        <?php echo wp_get_attachment_image($img, 'full', "", array("class" => "img", 'alt' => get_the_title(), 'title' => get_the_title())); ?>
                    <?php endforeach; ?>
                </figure>
            
            <?php endif; ?>

                <div class="mq-emp-video__txt">
                
                    <h4 class="title"><?php echo $title_conceito;?></h4>
                    <div class="content"><?php echo $desc_conceito; ?></div>

                    <?php if($play_video): ?>
                    <a href="#video_popup" class="btn-video open-modal-video mq-btn mq-btn--orange mq-btn--small" data-effect="mfp-zoom-in"  data-video="<?php echo $play_video; ?>">Ver Vídeo</a>
                    <?php endif; ?>
                </div>

        </div>
    </section>
<?php endif; ?>