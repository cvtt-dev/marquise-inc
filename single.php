<?php get_template_part('templates/html','header'); ?>
<?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('templates/empreendimento','slider');?>
    
    <div class="mq-page mq-page--emp-destalhes" id="emp-single" data-emp="<?php global $post; echo $post->post_name;?>">

       
        <?php get_template_part('templates/empreendimento','menu');?>

        <?php get_template_part('templates/empreendimento','sobre');?>

        <?php //get_template_part('templates/empreendimento','video');?>    
        
        <?php get_template_part('templates/empreendimento','clip');?>
        
        <?php get_template_part('templates/empreendimento','informacoes');?>

        <?php //get_template_part('templates/empreendimento','galeria');?>
        
        <?php get_template_part('templates/empreendimento','fotos');?>

        <?php get_template_part('templates/empreendimento','plantas');?>

        <?php get_template_part('templates/empreendimento','ficha');?>

        <?php get_template_part('templates/empreendimento','localizacao');?>

        <?php get_template_part('templates/empreendimento','form');?>
        
    </div>

<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>
