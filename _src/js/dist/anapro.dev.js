"use strict";

function enviaLeadAnapro() {
  var $form = document.querySelector('.wpcf7'),
      $nome = document.getElementById('emp_nome').value,
      $email = document.getElementById('emp_email').value,
      $telefone = document.getElementById('fone').value,
      $aceite = document.querySelector('.iptAceite').value,
      $codEmp = document.getElementById('empDataAnapro').getAttribute('data-codemp'),
      $campanha = document.getElementById('empDataAnapro').getAttribute('data-codcamp'),
      numero = $telefone.split(')'),
      ddd = numero[0].split('(');

  if (!$campanha) {
    $campanha = 'DDW22iWmT041';
  }

  var data = JSON.stringify({
    "Key": "tUoYb_Nr5xA1",
    "TagAtalho": "",
    "CampanhaKey": $campanha,
    "ProdutoKey": $codEmp,
    "CanalKey": "Ljvx0gwFgWw1",
    "PoliticaPrivacidadeKey": "1tHp0-ZXRgA1",
    "Midia": "Marquise - Site",
    "Peca": "",
    "UsuarioEmail": "",
    "GrupoPeca": "",
    "CampanhaPeca": "",
    "PessoaNome": $nome,
    "PessoaSexo": "",
    "ValidarEmail": "false",
    "PessoaEmail": $email,
    "ValidarTelefone": "false",
    "PessoaTelefones": [{
      "Tipo": "OUTR",
      "DDD": ddd[1],
      "Numero": numero[1],
      "Ramal": null
    }],
    "Observacoes": "termos de LGPD: " + $aceite,
    "KeyExterno": "",
    "UsarKeyExterno": "false",
    "KeyIntegradora": "A1140F73-8DC5-4842-A37D-19C290686661",
    "KeyAgencia": "947fc514-c40b-42db-9a96-be2903465043",
    "ListHashTag": null
  });
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(this.responseText);
    }
  });
  xhr.open("POST", "http://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(data);
}

function enviaLeadPopupAnapro() {
  var $form = document.querySelector('.wpcf7'),
      $nome = document.getElementById('emp_popup_nome').value,
      $email = document.getElementById('emp_popup_email').value,
      $telefone = document.getElementById('emp_popup_telefone').value,
      $aceite = document.querySelector('.iptAceite').value,
      $contactar = document.querySelector('input[name="contactar"]:checked').value,
      $codEmp = document.getElementById('empDataAnapro').getAttribute('data-codemp'),
      $campanha = document.getElementById('empDataAnapro').getAttribute('data-codcamp'),
      numero = $telefone.split(')'),
      ddd = numero[0].split('(');

  if (!$campanha) {
    $campanha = 'DDW22iWmT041';
  }

  var data = JSON.stringify({
    "Key": "tUoYb_Nr5xA1",
    "TagAtalho": "",
    "CampanhaKey": $campanha,
    "ProdutoKey": $codEmp,
    "CanalKey": "Ljvx0gwFgWw1",
    "PoliticaPrivacidadeKey": "1tHp0-ZXRgA1",
    "Midia": "Marquise - Site (popup)",
    "Peca": "",
    "UsuarioEmail": "",
    "GrupoPeca": "",
    "CampanhaPeca": "",
    "PessoaNome": $nome,
    "PessoaSexo": "",
    "ValidarEmail": "false",
    "PessoaEmail": $email,
    "ValidarTelefone": "false",
    "PessoaTelefones": [{
      "Tipo": "OUTR",
      "DDD": ddd[1],
      "Numero": numero[1],
      "Ramal": null
    }],
    "Observacoes": "termos de LGPD: " + $aceite + ', contato preferencial via: ' + $contactar,
    "KeyExterno": "",
    "UsarKeyExterno": "false",
    "KeyIntegradora": "A1140F73-8DC5-4842-A37D-19C290686661",
    "KeyAgencia": "947fc514-c40b-42db-9a96-be2903465043",
    "ListHashTag": null
  });
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(this.responseText);
    }
  });
  xhr.open("POST", "http://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(data);
}

function enviaLeadFooterAnapro() {
  var $form = document.querySelector('.wpcf7'),
      $nome = document.getElementById('emp_interesse_nome').value,
      $email = document.getElementById('emp_interesse_email').value,
      $telefone = document.getElementById('emp_interesse_tel').value,
      $msg = document.getElementById('emp_interesse_mensagem').value,
      $aceite = document.querySelector('.iptAceite').value,
      $codEmp = document.getElementById('empDataAnapro').getAttribute('data-codemp'),
      $campanha = document.getElementById('empDataAnapro').getAttribute('data-codcamp'),
      numero = $telefone.split(')'),
      ddd = numero[0].split('(');

  if (!$campanha) {
    $campanha = 'DDW22iWmT041';
  }

  var data = JSON.stringify({
    "Key": "tUoYb_Nr5xA1",
    "TagAtalho": "",
    "CampanhaKey": $campanha,
    "ProdutoKey": $codEmp,
    "CanalKey": "Ljvx0gwFgWw1",
    "PoliticaPrivacidadeKey": "1tHp0-ZXRgA1",
    "Midia": "Marquise - Site (Rodapé)",
    "Peca": "",
    "UsuarioEmail": "",
    "GrupoPeca": "",
    "CampanhaPeca": "",
    "PessoaNome": $nome,
    "PessoaSexo": "",
    "ValidarEmail": "false",
    "PessoaEmail": $email,
    "ValidarTelefone": "false",
    "PessoaTelefones": [{
      "Tipo": "OUTR",
      "DDD": ddd[1],
      "Numero": numero[1],
      "Ramal": null
    }],
    "Observacoes": "termos de LGPD: " + $aceite + ', Mensagem: ' + $msg,
    "KeyExterno": "",
    "UsarKeyExterno": "false",
    "KeyIntegradora": "A1140F73-8DC5-4842-A37D-19C290686661",
    "KeyAgencia": "947fc514-c40b-42db-9a96-be2903465043",
    "ListHashTag": null
  });
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(this.responseText);
    }
  });
  xhr.open("POST", "http://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(data);
}