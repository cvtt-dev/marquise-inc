/*=========================================================================================
// INICIO AJAX JS
========================================================================================= */

jQuery(function($) {
    $(document).ready(function() {

/*=========================================================================================
//AJAX GLOBAL VARS
=========================================================================================*/

    var SITE_URL = $('.mq-header-child__logo a').attr('href'),
    AJAX_URL  = SITE_URL + '/wp-admin/admin-ajax.php',
    LOGIN_URL = SITE_URL + '/area-do-cliente';

/*=========================================================================================
//IR FORM
=========================================================================================*/

// $('.ir-form-action').submit(function(){
//     $('#ir-menu').dropdown('toggle');
//     $('.ir-form-action')[0].reset();
// });

$('.dropdown-menu').find('form').click(function (e) {
    e.stopPropagation();
});

/*=========================================================================================
//FORMULÁRIO EDITAR DADOS
=========================================================================================*/

function formDados() {


    var nome       = $('.cliente_nome').val(),
        cpf        = $('.cliente_cpf').val(),
        rg         = $('.cliente_rg').val(),
        telefone   = $('.cliente_telefone').val(),
        email      = $('.cliente_email').val(),
        endereco   = $('.cliente_endereco').val(),
        logradouro = $('cliente_endereco_logradouro').val(),
        numero     = $('.cliente_endereco_numero').val(),
        compl      = $('.cliente_endereco_compl').val(),
        bairro     = $('.cliente_bairro').val(),
        cep        = $('.cliente_cep').val();
        cidade     = $('.cliente_cidade').val(),
        uf         = $('.cliente_uf').val(),
        pais       = $('.cliente_endereco_pais').val();

        if(compl != '') {
            var full_address = logradouro+', '+numero+ ' - '+compl;
        } else {
            var full_address = logradouro+', '+numero;
        }

        $('.wpcf7 .form-editar').find('.nome input').val(nome);
        $('.wpcf7 .form-editar').find('.cpf input').val(cpf);
        $('.wpcf7 .form-editar').find('.rg input').val(rg);
        $('.wpcf7 .form-editar').find('.email input').val(email);
        $('.wpcf7 .form-editar').find('.telefone input').val(telefone);
        $('.wpcf7 .form-editar').find('.endereco input').val(endereco);
        $('.wpcf7 .form-editar').find('.bairro input').val(bairro);
        $('.wpcf7 .form-editar').find('.cep input').val(cep);
        $('.wpcf7 .form-editar').find('.cidade input').val(cidade);
        $('.wpcf7 .form-editar').find('.uf input').val(uf);
        $('.wpcf7 .form-editar').find('.pais input').val(pais);

        console.log(full_address);
}

// modal form editar dados do cliente
$('.modal-alterar').magnificPopup({
    removalDelay: 500,
    callbacks: {
        beforeOpen: function () {
            this.st.mainClass = this.st.el.attr('data-effect');
            formDados();
        },
        close: function () {

        }
    },
    midClick: true
});

/*=========================================================================================
//FORMULÁRIO ASSISTÊNCIA TÉCNICA
=========================================================================================*/


function clearDados() {
    $('.wpcf7 .form-assistencia').find('.nome input').val("");
    $('.wpcf7 .form-assistencia').find('.cpf input').val("");
    $('.wpcf7 .form-assistencia').find('.rg input').val("");
    $('.wpcf7 .form-assistencia').find('.email input').val("");
    $('.wpcf7 .form-assistencia').find('.telefone input').val("");
    //$('.wpcf7 .form-assistencia').find('.empreendimento input').val("");
    //$('.wpcf7 .form-assistencia').find('.unidade input').val("");
   // $('.wpcf7 .form-assistencia').find('.codigo input').val("");
    //$('.wpcf7 .form-assistencia').find('.contrato input').val("");
    $('.wpcf7 .form-assistencia').find('.endereco_emp input').val(""); 
}

function formAssitencia(div) {

    var nome = $('.cliente_nome').val(),
        cpf = $('.cliente_cpf').val(),
        rg = $('.cliente_rg').val(),
        telefone = $('.cliente_telefone').val(),
        email = $('.cliente_email').val(),
        imovel_nome = $('.' + div + '_imovel_nome').val(),
        imovel_codigo = $('.' + div + '_imovel_codigo').val(),
        imovel_unidade = $('.' + div + '_imovel_unidade').val(),
        imovel_endereco = $('.' + div + '_imovel_endereco_emp').val(),
        imovel_contrato = $('.' + div + '_imovel_contrato').val();

        $('.wpcf7 .form-assistencia').find('.nome input').val(nome);
        $('.wpcf7 .form-assistencia').find('.cpf input').val(cpf);
        $('.wpcf7 .form-assistencia').find('.rg input').val(rg);
        $('.wpcf7 .form-assistencia').find('.email input').val(email);
        $('.wpcf7 .form-assistencia').find('.telefone input').val(telefone);
        $('.wpcf7 .form-assistencia').find('.empreendimento input').val(imovel_nome);
        $('.wpcf7 .form-assistencia').find('.unidade input').val(imovel_unidade);
        $('.wpcf7 .form-assistencia').find('.codigo input').val(imovel_codigo);
        $('.wpcf7 .form-assistencia').find('.contrato input').val(imovel_contrato);
        $('.wpcf7 .form-assistencia').find('.endereco_emp input').val(imovel_endereco); 
    
    var inquilino = $('input[type=radio][name=tipo]');

    inquilino.change(function () {
        if (this.value == 'Proprietário') {

            var nome = $('.cliente_nome').val(),
                cpf = $('.cliente_cpf').val(),
                rg = $('.cliente_rg').val(),
                telefone = $('.cliente_telefone').val(),
                email = $('.cliente_email').val(),
                imovel_nome = $('.' + div + '_imovel_nome').val(),
                imovel_codigo = $('.' + div + '_imovel_codigo').val(),
                imovel_unidade = $('.' + div + '_imovel_unidade').val(),
                imovel_endereco = $('.' + div + '_imovel_endereco_emp').val(),
                imovel_contrato = $('.' + div + '_imovel_contrato').val();

            $('.wpcf7 .form-assistencia').find('.nome input').val(nome);
            $('.wpcf7 .form-assistencia').find('.cpf input').val(cpf);
            $('.wpcf7 .form-assistencia').find('.rg input').val(rg);
            $('.wpcf7 .form-assistencia').find('.email input').val(email);
            $('.wpcf7 .form-assistencia').find('.telefone input').val(telefone);
            $('.wpcf7 .form-assistencia').find('.empreendimento input').val(imovel_nome);
            $('.wpcf7 .form-assistencia').find('.unidade input').val(imovel_unidade);
            $('.wpcf7 .form-assistencia').find('.codigo input').val(imovel_codigo);
            $('.wpcf7 .form-assistencia').find('.contrato input').val(imovel_contrato);
            $('.wpcf7 .form-assistencia').find('.endereco_emp input').val(imovel_endereco); 
            
             

        }
        else if (this.value == 'Inquilino') {
            
            clearDados();
            
        }
    });
    
        
}

// modal form editar dados do cliente
$('.modal-assistencia').magnificPopup({
    removalDelay: 500,
    callbacks: {
        beforeOpen: function () {
            this.st.mainClass = this.st.el.attr('data-effect');
            var div = this.st.el.attr('data-cod');
            //console.log(div);
            formAssitencia(div);
        },
        close: function () {

        }
    },
    midClick: true
});




$('#reset-password').on('click', function (e) {
    e.preventDefault();
    $('#form-reset-password').slideToggle();
})

function resetPassword() {
    
    var submit = $('#reset').on('click', function (e) {
        e.preventDefault();
        
        var data = $("#email_reset").val();
        console.log(data);

        emailResetPassword(data);

        
    });
}
resetPassword();


function emailResetPassword(data) {

    $.ajax({
        url: AJAX_URL,
        type: 'POST',
        data: "action=send_reset_password&email=" + data,
        cache: false,
        dataType: 'json',

        beforeSend: function () {
            $('.msg-popup').fadeIn(250);
        },

        success: function (data) {
           
            var rsp = data.rsp;
            //var qtd = data.cart;
            console.log(rsp);

            switch (rsp) {
                case 1:
                    var msg = 'Um E-mail foi enviado para você com as instruções de acesso.';
                    var css = 'msg-cart--add';
                    window.location.href = SITE_URL + '/solicitacao-enviada-com-sucesso/?enviado-com-sucesso=obrigado';
                    //window.location.href = SITE_URL + '/cotacao/pedido-enviado/?cotacao=true';
                    //captura_mail();
                    break;

                case 2:
                    var msg = 'Verifique se preencheu o campo corretamente';
                    var css = 'msg-cart--erro';
                    break;



                default:
                    var msg = 'Ocorreu um erro ao tentar processar sua cotação. Favor tente novamente.';
                    var css = 'msg-cart--erro';
            }

           
        },

        error: function () {
           console.log(data);
        }

        
    });

    return false;
}









/*=========================================================================================
//TABS
=========================================================================================*/

$('.panel-heading').click(function() {
    if ($(this).hasClass('on')) {
      $(this).removeClass('on');
    } else {
      $('.panel-heading').removeClass('on');
      $(this).addClass('on');
      activeBars();
    }
});

/*=========================================================================================
// BARRA ANIMADA
=========================================================================================*/

var bar = $('.bar-progress');
var index = 0;

function activeBars(){
    bar.each(function(index, item){
        // index = 0; index < bar.length; index++
        var atributos = $(item).attr('data-total');
        $(item).css('width', atributos + '%');
    });
}

activeBars();

/*=========================================================================================
//LOGIN JavaScript
=========================================================================================*/

$('#area_cliente .mq-modal-cliente__btn').on('click', function() {
    var data    = {
        'action'  : 'getUser',
        'login'   : $('#login').val(),
        'senha'   : $('#password').val(),
    }
    
    getClienteAjax(data);
});

function getClienteAjax(data){
    
    $.ajax({
        url: AJAX_URL,
        type: "POST",
        data: data,
        beforeSend: function() {
            $('.login_msg').attr('class','login_msg');
            $('#cliente-login-load').removeClass('hidden');
            $('#area_cliente .btn').attr('disabled','disabled');
        },
        success: function(response) {

            var rsp = response;
            console.log('rsp: '+rsp);

            if(rsp == 1) {
                var msg = "Login realizado com sucesso! Iremos redirecioná-lo para a área de clientes.",
                    css = 'login__msg--sucesso';
                    window.location.href = LOGIN_URL;
            } else if(rsp == 2) {
                var msg = "Dados incorretos. Tente novamente",
                    css = 'login__msg--erro';
            } else {
                var msg = "Nosso sistema está em manutenção. Favor tente novamente em alguns minutos.",
                    css = 'login__msg--erro';
            }

            $('.login_msg').delay(200).addClass(css).html(msg).fadeIn(500);
            $('.login_msg').delay(1300).fadeOut(800);
            $('#area_cliente .btn').removeAttr('disabled');
            $('#cliente-login-load').addClass('hidden');
        },
        error: function() {
            var msg = 'Nosso sistema está com problemas. Favor tente novamente em alguns minutos.',
                css = 'login_msg--erro';
                $('.login_msg').delay(200).addClass(css).html(msg).fadeIn(500);
                $('.login_msg').delay(1300).fadeOut(800);
                $('#area_cliente .btn').removeAttr('disabled');
                $('#cliente-login-load').addClass('hidden');
        }
    });
}




        
/*=========================================================================================
// CLOSE FUNCTION
=========================================================================================*/
    });
});
