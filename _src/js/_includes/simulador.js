/*=========================================================================================
// INICIO MAIN JS
========================================================================================= */

jQuery(function($) {
    $(document).ready(function() {

/*=========================================================================================
// INPUT RANGE SLIDE
========================================================================================= */

/* Code by Steven Estrella. http://www.shearspiremedia.com */
/* we need to change slider appearance oninput and onchange */

$('#slider').on('input change', function(){

    var vertical   = false;
    var valor      = $(this).val();
    var pc         = valor/($(this).attr('max') - $(this).attr('min')); /* the percentage slider value */
    var thumb      = document.getElementById("sliderthumb");
    var shell      = document.getElementById("slidershell");
    var track      = document.getElementById("slidertrack");
    var fill       = document.getElementById("sliderfill");
    var rangevalue = document.getElementById("slidervalue");
    var slider     = document.getElementById("slider");
    var thumbsize  = 10; /* must match the thumb size in your css */
    var bigval     = $('.slidershell').width(); /* widest or tallest value depending on orientation */
    var renda      = $('.slidervalue').width(); /* widest or tallest value depending on orientation */

    var smallval   = 40; /* narrowest or shortest value depending on orientation */
    var tracksize  = bigval - thumbsize;
    var fillsize   = 16;
    var filloffset = 10;
    var bordersize = 2;
    var loc        = vertical ? (1 - pc) * tracksize : pc * tracksize;
    var all        = loc + thumbsize;
    var degrees = 360 * pc;
    var rotation = "rotate(" + degrees + "deg)";

    rangevalue.innerHTML        = 'R$ ' + valor;
    thumb.style.webkitTransform = rotation;
    thumb.style.MozTransform = rotation;
    thumb.style.msTransform = rotation;

    rangevalue.style.left       = (vertical ? 0 : loc) + "px";
    rangevalue.style.marginLeft = (all == bigval ? '-'+(renda + 13)+'px' : '-'+(renda/2)+'px');
    console.log('Loc:'+ loc + 'Big: '+bigval + 'All:'+all);

    fill.style.opacity = pc + 0.2 > 1 ? 1 : pc + 0.2;

    // thumb.style.top      =  (vertical ? loc : 0) + "px";
    thumb.style.left        = (vertical ? 0 : loc) + "px";
    // fill.style.top       = (vertical ? loc + (thumbsize/2) : filloffset + bordersize) + "px";
    // fill.style.left         = (vertical ? filloffset + bordersize : 0) + "px";
    fill.style.width        = (vertical ? fillsize : loc + (thumbsize/2)) + "px";
    // fill.style.height    = (vertical ? bigval - filloffset - fillsize - loc : fillsize) + "px";
    // shell.style.height   = (vertical ? bigval : smallval) + "px";
    shell.style.width       = (vertical ? smallval : bigval) + "px";
    // track.style.height   = (vertical ? bigval - 4 : fillsize) + "px"; /* adjust for border */
    track.style.width       = (vertical ? fillsize : bigval - 4) + "px"; /* adjust for border */
    track.style.left        = (vertical ? filloffset + bordersize : 0) + "px";
    // track.style.top      = (vertical ? 0 : filloffset + bordersize) + "px";
});

/*=========================================================================================
// SELECT TIPO
=========================================================================================*/

var tipo = $('.form-simulador__group .tipo');
    
if(tipo.val() == 'Colaborador'){
    $('.form-simulador__item--matricula').html('<input name="matricula" class="matricula" type="text" placeholder="Matrícula" required />').fadeIn(500);
}

tipo.on('focus',function(){
    tipo.addClass('open');

}).on('change', function(){

    if(tipo.val() == 'Colaborador'){

        $('.form-simulador__item--matricula').html('<input name="matricula" class="matricula" type="text" placeholder="Matrícula" required />').delay(200).fadeIn(500);
        $('.matricula').focus();

    } else {

        $('.form-simulador__item--matricula').fadeOut(500, function(){
            $('.form-simulador__item--matricula').delay(500).html('<input name="matricula" class="matricula" type="text" placeholder="Matrícula" value="-" required />');
            $('.slider').focus();
        });
    }

}).blur(function(){
    $('.tipo').removeClass('open');
});

/*=========================================================================================
// MASKS CONTATO
=========================================================================================*/

$("input.cpf").mask("999.999.999-99");
$("input.data").mask("99/99/9999");
$("input.fone").mask("(99)9999-9999?9");
/*=========================================================================================
// LAZY
=========================================================================================*/

var bLazy = new Blazy({
    offset: 0, // Loads images 100px before they're visible
    error: function(ele){
        var original = ele.getAttribute('data-src');
        ele.src = original;
    }
});

/*=========================================================================================
// AJAX JSON REQUEST
=========================================================================================*/

var palco = $('.empreendimentos__lista'),
    count = $('.empreendimentos__item--total'),
    form  = $('.empreendimentos__form');

$.ajax({
    // url      : 'http://www.marquiseincorporacoes.com.br/wp-json/wp/v2/mar_empreendimentos/',
    url      : 'http://www.marquiseincorporacoes.com.br/wp-json/wp/v2/mar_empreendimentos?per_page=100',
    type     : "GET",
    dataType : 'json',
    beforeSend: function( xhr ) {
        $('.empreendimentos .container').addClass('load');
    },
    success  : function (data) {

        var renda = $('.empreendimentos').attr('data-renda');
        var tipo  = $('.empreendimentos').attr('data-tipo');
        var empreendimentos = 0;

        var bLazy = new Blazy();
        bLazy.revalidate();

        $.each(data, function (i, item) {

            var renda_atual = parseFloat(renda);
            var faixa_renda = parseFloat(item.wpcf_faixa_renda);
            var renda_desc  = parseFloat(item.wpcf_desconto_renda);

            if(tipo === 'Colaborador' && renda_desc != '') {
                var faixa_renda = faixa_renda - (faixa_renda * renda_desc)/100;
            }

            // if (parseFloat(renda) >= parseFloat(item.wpcf_faixa_renda)) {
            if (renda_atual >= faixa_renda && faixa_renda !== '') {
                empreendimentos++;
                mar_empreendimentos =
                '<div class="empreendimentos__item">'+
                    '<div class="empreendimentos__thumb">'+
                        // '<img class="empreendimentos__image" src="'+item.featured_image+'" />'+
                        '<img class="empreendimentos__image b-lazy fade" data-src="'+item.featured_image+'" alt="Fachada: '+item.title.rendered+'" title="Fachada: '+item.title.rendered+'">'+
                        '<noscript><img src="'+item.featured_image+'" alt="Fachada: '+item.title.rendered+'" title="Fachada: '+item.title.rendered+'"></noscript>'+
                        (item.id != 1500 ? '<span class="empreendimentos__small">Fachada '+item.title.rendered+'</span>' : '')+
                    '</div>'+
                    //'+item.wpcf_faixa_renda+' - '+renda+
                    '<div class="empreendimentos__infos">' +
                        '<div>'+
                            '<div class="empreendimentos__logo"><img class="empreendimentos__image" src="http://www.marquiseincorporacoes.com.br/'+item.empreendimento_caracteristicas.logo+'" /></div>'+
                            '<div class="empreendimentos__wraptitle">'+
                                (item.status_emp ? '<div class="empreendimentos__status">'+item.status_emp+'</div>' : '')+
                                '<h3 class="empreendimentos__title">'+item.title.rendered+'</h3>'+
                                (item.empreendimento_caracteristicas.bairro ?
                                    '<h4 class="empreendimentos__subtitle"><i class="fa fa-map-marker"></i> '+
                                        item.empreendimento_caracteristicas.bairro+
                                    '</h4>' : '')+
                            '</div>'+
                        '</div>'+
                        '<div class="empreendimentos__icones">'+
                        (item.empreendimento_caracteristicas.qtdquartos ?
                            '<div class="empreendimentos__icon empreendimentos__icon--suites">'+
                                '<i></i>'+
                                '<p>'+item.empreendimento_caracteristicas.qtdquartos+'</p>'+
                            '</div>' : '')+
                        (item.empreendimento_caracteristicas.qtdgaragem ?
                            '<div class="empreendimentos__icon empreendimentos__icon--vagas">'+
                                '<i></i>'+
                                '<p>'+item.empreendimento_caracteristicas.qtdgaragem+'</p>'+
                            '</div>' : '')+
                        (item.empreendimento_caracteristicas.tamanho ?
                            '<div class="empreendimentos__icon empreendimentos__icon--area">'+
                                '<i></i>'+
                                '<p>'+item.empreendimento_caracteristicas.tamanho+'</p>'+
                            '</div>' : '')+
                        '</div>'+
                        '<div class="empreendimentos__texto">'+
                        item.empreendimento_caracteristicas.conceito+
                        '</div>'+
                        '<div class="empreendimentos__center">'+
                            '<a class="empreendimentos__link" href="'+item.link+'" target="_blank">Saiba Mais</a>'+
                        '</div>'+
                    '</div>'+
                '</div>';
                palco.append(mar_empreendimentos);
            }

            $('.empreendimentos .container').removeClass('load');
            palco.fadeIn(500);
        });

        if(empreendimentos == 0) {
            var msg = '<b>No momento não dispomos de empreendimentos com esse perfil.<br>Fique atento a futuros lançamentos.</b><br><br><a class="empreendimentos__link empreendimentos__link--small" href="http://www.marquiseincorporacoes.com.br/empreendimentos/">Nossos empreendimentos</a>';
        }
        else if(empreendimentos == 1) {
            var msg = 'Encontramos <b>'+empreendimentos+' empreendimento</b> ideal para você!';
        } else {
            var msg = 'Encontramos <b>'+empreendimentos+' empreendimentos</b> ideais para você!';
        }

        count.append(msg);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
        console.log(textStatus, errorThrown)
        $('.empreendimentos .container').removeClass('load');
        var msg = '<b>Não foi possível concluir sua pesquisa. Por favor tente novamente</b>';
        form.fadeIn(500);
        count.append(msg);
    }
});

/*=========================================================================================
// CLOSE FUNCTION
=========================================================================================*/
    });
});