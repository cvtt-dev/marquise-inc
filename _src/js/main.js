var Marquise = (function () {
    function init() {
      menu();
      slideDestaque();
      galeriaCorretor();
      galeriaEmpreendimentoDestaque();
      galeriaEmpreendimento();
      galeriaEmpreendimentoHome();
      fotosEmpreendimentoHome();
      galeriaPlantas();
      galeriaPlantasDestaques();
      detalhesGaleria();
      accordionAreaCliente();
      opcoesAreaCliente();
      modalAreaCliente();
      modalBuscaEmpreendimento();
      modalVideoInstitucional();
      mascaraForm();
      empreendimentoEmail();
      redirectsForms();
      popup();
      fotosEmpreendimentoDestaque();
    }
  
    function menu() {
      // MENU
      $(".btn-nav-mobile").click(function () {
        var nav = $(".navmobile");
  
        if ($(nav).hasClass("active")) {
          $(nav).removeClass("active").fadeOut();
          $(this).removeClass("toggled");
        } else {
          $(nav).addClass("active").fadeIn();
          $(this).addClass("toggled");
        }
      });
    }
    /*=========================================================================================
    // EQUAL HEIGHT
    =========================================================================================*/
  
    function equalizeHeights(selector) {
      var heights = new Array();
  
      // Loop to get all element heights
      $(selector).each(function () {
        // Need to let sizes be whatever they want so no overflow on resize
        $(this).css("min-height", "0");
        $(this).css("max-height", "none");
        $(this).css("height", "auto");
  
        // Then add size (no units) to array
        heights.push($(this).height());
      });
  
      // Find max height of all elements
      var max = Math.max.apply(Math, heights);
  
      // Set all heights to max height
      $(selector).each(function () {
        $(this).css("height", max + "px");
      });
    }
  
    $(window).on("load resize", function () {
      equalizeHeights(".owl-certificados .item .text");
    });
  
    function slideDestaque() {
      // SET OWL - DESTAQUE
      $("#owl-slider-destaque").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        loop: false,
        mouseDrag: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        navText: false,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 1,
            nav: false,
            dots: false
          },
          1000: {
            items: 1,
            nav: true,
            dots: false
          }
        }
      });
    }
  
    function galeriaEmpreendimentoDestaque() {
      // SET OWL - GALERIA DESTAQUE
      $("#emp-galeria").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        loop: false,
        margin: 20,
        responsive: {
          1000: {
            center: true,
            dots: true,
            items: 2
          }
        }
      });
    }
  
    function fotosEmpreendimentoDestaque() {
      // SET OWL - GALERIA DESTAQUE
      $("#emp-fotos").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        merge:false,
        responsive: {
          1000: {
            dots: false,
            items: 1
          }
        }
      });
    }
  
    function galeriaCorretor() {
      $(".mq-corretor__destaques .wrap-slide").owlCarousel({
        mouseDrag: true,
        navText: false,
        loop: true,
        autoplay: false,
        margin: 25,
        responsive: {
          0: {
            items: 1,
            nav: false,
            dots: true
          },
          600: {
            items: 2,
            nav: true,
            dots: true
          }
        }
      });
    }
  
    function galeriaEmpreendimento() {
      // SET OWL - GALERIA EMPREENDIMENTO
      $(".owl-galeria-emp").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        loop: true,
        mouseDrag: false,
        margin: 0,
        autoplay: true
      });
    }
  
    $(".mq-menu__item--btn-mob-cli a").on("click", function (e) {
      e.preventDefault();
  
      var session = $("body").data("session");
      console.log(session);
  
      if (session == false) {
        $.magnificPopup.open({
          items: {
            src: "#cliente_popup",
            type: "inline"
          }
        });
      } else {
        window.location.href =
          "https://www.marquiseincorporacoes.com.br/area-do-cliente/";
      }
    });
    function galeriaEmpreendimentoHome() {
      // SET POPUP - GALERIA DESTAQUE
      $(".owl-galeria-emp").each(function () {
        $(this).magnificPopup({
          delegate: "a", // the selector for gallery item
          type: "image",
          tLoading: "Carregando imagem #%curr%...",
          closeOnContentClick: false,
          closeBtnInside: false,
          mainClass: "mfp-with-zoom mfp-img-mobile",
          image: {
            verticalFit: true,
            titleSrc: function (item) {
              return item.el.attr("title");
            }
          },
          gallery: {
            enabled: true,
            tCounter:
              '<span class="mfp-counter contador-galeria">%curr% de %total%</span>' // markup of counter
          },
          zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
              return element.find(".mq-emp-galeria__figure");
            }
          }
        });
      });
    }
  
    function fotosEmpreendimentoHome() {
      // SET POPUP - GALERIA DESTAQUE
      $(".modal-foto").each(function () {
        $(this).magnificPopup({
          delegate: "a", // the selector for gallery item
          type: "image",
          tLoading: "Carregando imagem #%curr%...",
          closeOnContentClick: false,
          closeBtnInside: false,
          mainClass: "mfp-with-zoom mfp-img-mobile",
          image: {
            verticalFit: true,
            titleSrc: function (item) {
              return item.el.attr("title");
            }
          },
          gallery: {
            enabled: true,
            tCounter:
              '<span class="mfp-counter contador-galeria">%curr% de %total%</span>' // markup of counter
          },
          zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
              return element.find(".mq-emp-galeria__figure");
            }
          }
        });
      });
    }
  
    function galeriaPlantas() {
      // SET OWL - GALERIA PLANTAS
      $("#owl-plantas").owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        loop: false,
        mouseDrag: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        navText: false,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 1,
            nav: false,
            dots: true
          },
          1000: {
            items: 1,
            nav: false,
            dots: true
          }
        }
      })
      .find('.owl-item')
      .each(function(i){
        var attr = $(this).children().attr('data-item');
        var element = $('<span class="owl-item-options">Opção '+attr+'</span>');
        $('.owl-carousel .owl-dot').eq(i).append(element);
      });
      
    }
  
    function galeriaPlantasDestaques() {
      $(".mq-plantas__img-destaque").each(function () {
        $(this).magnificPopup({
          delegate: "a", // the selector for gallery item
          type: "image",
          tLoading: "Carregando imagem #%curr%...",
          closeOnContentClick: false,
          closeBtnInside: false,
          mainClass: "mfp-with-zoom mfp-img-mobile",
          image: {
            verticalFit: true,
            titleSrc: function (item) {
              return item.el.attr("title");
            }
          },
          image: {
            verticalFit: true,
            tCounter:
              '<span class="mfp-counter contador-galeria">%curr% de %total%</span>' // markup of counter
          },
          zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
              return element.find("img");
            }
          }
        });
      });
    }
  
    function detalhesGaleria() {
      // OPÇÕES DOS DETALHES DA GALERIA
      $('a[href*="#"]:not([href="#"])').click(function () {
        if (
          location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
          location.hostname == this.hostname
        ) {
          var target = $(this.hash);
  
          target = target.length
            ? target
            : $("[name=" + this.hash.slice(1) + "]");
  
          if (target.length) {
            $("html, body").animate(
              {
                scrollTop: target.offset().top
              },
              1000
            );
  
            return false;
          }
        }
      });
    }
  
    function close_accordion_section(target) {
      target.removeClass("active");
      target.next().removeClass("open");
      target.next().slideUp();
    }
  
    function accordionAreaCliente() {
      // ACORDION CONTRATOS - AREA DO CLIENTE
      var barActive = $(".accordion-title");
  
      $(barActive.attr("href")).slideDown();
  
      $(".accordion-title").click(function (e) {
        e.preventDefault();
  
        var currentAttrValue = $(this).attr("href");
  
        if ($(e.target).is(".active")) {
          close_accordion_section($(e.target));
        } else {
          $(this).addClass("active");
  
          $(".accordion " + currentAttrValue).slideDown();
        }
      });
    }
  
    function opcoesAreaCliente() {
      // OPÇÕES NO PAINEL DO CLIENTE
      $("#drop3, #drop4").on("click", function (e) {
        e.preventDefault();
  
        var subMenu = $(this).parent().children("#ir-menu, #boleto");
        $(subMenu).slideToggle();
      });
    }
  
    function modalAreaCliente() {
      // modal form área do cliente
      $(".open-modal-cliente").magnificPopup({
        removalDelay: 500,
        callbacks: {
          beforeOpen: function () {
            this.st.mainClass = this.st.el.attr("data-effect");
          },
          close: function () {}
        },
        midClick: true
      });
    }
  
    function modalBuscaEmpreendimento() {
      // modal form busca empreendimento header
      $(".open-modal-emp").magnificPopup({
        removalDelay: 500,
        callbacks: {
          beforeOpen: function () {
            this.st.mainClass = this.st.el.attr("data-effect");
          },
          close: function () {}
        },
        midClick: true
      });
    }

    function modalcontato() {
      // modal form busca empreendimento header
      $(".mp-slider--icon").magnificPopup({
        removalDelay: 500,
        callbacks: {
          beforeOpen: function () {
            this.st.mainClass = this.st.el.attr("data-effect");
          },
          close: function () {}
        },
        midClick: true
      });
    }modalcontato()
  
    function modalVideoInstitucional() {
      // modal vídeo page institucional
      $(".open-modal-video").magnificPopup({
        removalDelay: 500,
        callbacks: {
          beforeOpen: function () {
            this.st.mainClass = this.st.el.attr("data-effect");
            var video = this.st.el.attr("data-video");
            var html =
              '<div class="youtube__iframe"><iframe width="560" height="315" src="https://www.youtube.com/embed/' +
              video +
              '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe></div>';
            $(this.st.el.attr("href")).html(html).fadeIn(1000);
          },
          close: function () {
            $(this.st.el.attr("href")).html("");
          }
        },
        midClick: true
      });
    }

    function locationselect(){
      var select = $('.mq-header-select');
      select.on('change', function () {
        var selecionada = this.options[this.selectedIndex];
        var url = selecionada.getAttribute('data-url');
        // console.log(url);
        
        if(url == 'ceara'){
          cookieLocation('brazil','ceara');
        }
        
        if( url == 'sao-paulo'){
          cookieLocation('brazil','sao-paulo');
        }
        
      });
    }locationselect()
  
    function mascaraForm() {
      $("#telefone").mask("(99) 99999-9999");
      $("#fone").mask("(99) 99999-9999");
      $("#emp_interesse_tel").mask("(99) 99999-9999");
  
      $(".phone-mask-pattern").mask("(99) 99999-9999");
    }
  
    function empreendimentoEmail() {
      var empreendimento = $("#emp-single").data("emp");
      var emp_name = $(".emp_name").attr("value", empreendimento);
    }
  
    function redirectsForms() {
      // REDIRECTS FORMULARIOS DOS EMPREENDIMENTOS.
      var url_forms = $(".mq-header-child__logo a").attr("href");
  
      document.addEventListener(
        "wpcf7mailsent",
        function (event) {
          // ID do formulário do Banner Destaque.
          if ("2477" == event.detail.contactFormId) {
            // leadEmailSuaHouse();
            enviaLeadAnapro();
  
            var empreendimento = $("#emp-single").data("emp");
            location =
              url_forms +
              "/solicitacao-enviada-com-sucesso/?origem=empreendimento=" +
              empreendimento;
          }
  
          // ID do formulario do btn Tenho Interesse.
          else if ("2857" == event.detail.contactFormId) {
            // leadEmailFormPopUp();
            enviaLeadPopupAnapro();
  
            var empreendimento = $("#emp-single").data("emp");
            location =
              url_forms +
              "/solicitacao-enviada-com-sucesso/?origem=empreendimento=" +
              empreendimento;
          }
  
          // ID do formulario do maps.
          else if ("2488" == event.detail.contactFormId) {
            // leadEmailFormMaps();
            enviaLeadFooterAnapro();
  
            var empreendimento = $("#emp-single").data("emp");
            location =
              url_forms +
              "/solicitacao-enviada-com-sucesso/?origem=empreendimento=" +
              empreendimento;
          } else {
            location =
              url_forms +
              "/solicitacao-enviada-com-sucesso/?enviado-com-sucesso=obrigado";
          }
        },
        false
      );
    }
  
    
    function informaçõesEmpreedimentos(){
    $("#1").addClass('data-w-tab__current');
    $('.mq-emp-info__li').eq(0).addClass('current');
      $('.mq-emp-info__li').on('click', function(e){
        e.preventDefault();
        var t =  $(this).attr('data-w-tab');
        if($('.mq-emp-info__li').hasClass('current')){
            $('.mq-emp-info__li').removeClass('current');
        }
        $(this).addClass('current');
        $('.vantagens').removeClass('data-w-tab__current');
        $("#" + t).addClass('data-w-tab__current');
      })
    }informaçõesEmpreedimentos()
  
    function PopupCenter(url, title, w, h) {
      // Fixes dual-screen position                         Most browsers      Firefox
      var dualScreenLeft =
        window.screenLeft != undefined ? window.screenLeft : window.screenX;
      var dualScreenTop =
        window.screenTop != undefined ? window.screenTop : window.screenY;
  
      var width = window.innerWidth
        ? window.innerWidth
        : document.documentElement.clientWidth
        ? document.documentElement.clientWidth
        : screen.width;
      var height = window.innerHeight
        ? window.innerHeight
        : document.documentElement.clientHeight
        ? document.documentElement.clientHeight
        : screen.height;
  
      var systemZoom = width / window.screen.availWidth;
      var left = (width - w) / 2 / systemZoom + dualScreenLeft;
      var top = (height - h) / 2 / systemZoom + dualScreenTop;
      var newWindow = window.open(
        url,
        title,
        "scrollbars=yes, width=" +
          w / systemZoom +
          ", height=" +
          h / systemZoom +
          ", top=" +
          top +
          ", left=" +
          left
      );
  
      // Puts focus on the newWindow
      if (window.focus) newWindow.focus();
    }
  
    function popup() {
      $(".btn-simular").click(function () {
        enviaLeadSimuladorAnapro();
      });
      $("#close").click(function (e) {
        e.preventDefault();
        $("#popup").hide();
      });
      $(".cvtt-btn-trigger-sent").click(function (e) {
        enviaLeadAnapro();
      });
  
      $(".open-window").on("click", function (evt) {
        evt.preventDefault();
        PopupCenter($(this).data("link"), "", "700", "900");
      });
    }
  
    return {
      init: init
    };
  })(jQuery);
  
  Marquise.init();
  