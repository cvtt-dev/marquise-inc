<?php /* Template Name: Trabalhe Conosco */ ?>
<?php get_template_part('templates/html', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>

    <div class="mq-page">

        <?php get_template_part('templates/contato', 'banner'); ?>

        <?php get_template_part('templates/contato', 'opcoes'); ?>
        
        <?php get_template_part('templates/trabalhe-conosco', 'form'); ?>


        <?php get_template_part('templates/contato', 'informacoes'); ?>

    </div>





<?php endwhile;
wp_reset_postdata(); ?>
<?php get_template_part('templates/html', 'footer'); ?>