<?php /* Template Name: Busca Empreendimento */ ?>
<?php $args = array(
    'post_type' => 'mar_empreendimentos',
    'showposts' => -1,
    'tax_query' => array(
        'relation' => 'OR',

        array(
            'taxonomy'     => 'tipo-empreendimentos',
            'field'     => 'term_id',
            'terms'     => $_GET['tipo-empreendimentos']
        ),
        array(
            'taxonomy'     => 'qdtdomitorios-empreendimentos',
            'field'     => 'term_id',
            'terms'     => $_GET['qdtdomitorios-empreendimentos']
        ),
        array(
            'taxonomy'     => 'status-empreendimentos',
            'field'     => 'term_id',
            'terms'     => $_GET['status-empreendimentos']
        ),
        array(
            'taxonomy'     => 'bairro-empreendimentos',
            'field'     => 'term_id',
            'terms'     => $_GET['bairro-empreendimentos']
        )
    )
);
$empreendimento = new WP_Query($args);
?>

<?php get_template_part('templates/html', 'header'); ?>


<?php get_template_part('templates/empreendimentos', 'banner-busca'); ?>

<div class="mq-page">

    <section class="mq-section">
        <div class="container">
            <div class="mq-empreendimentos-all">

                <?php get_template_part('templates/empreendimento', 'busca'); ?>

                <?php if ($empreendimento->have_posts()) : ?>
                    <?php while ($empreendimento->have_posts()) : $empreendimento->the_post();
                        get_template_part('templates/loop', 'empreendimento');
                    endwhile;
                    wp_reset_query(); ?>
                <?php else : ?>

                    <div class="mq-nada-encontrado">
                        <i class="icone fa fa-ban" aria-hidden="true"></i>
                        <h2 class="title">Ops! Empreendimento não encontrado.</h2>
                    </div>

                <?php endif ?>
            </div>
        </div>
    </section>
</div>



<?php get_template_part('templates/html', 'footer'); ?>