<?php /* Template Name: Location */ ?>
<?php get_template_part('templates/html', 'header'); ?> 
    <style>
        .modal-window {
        position: fixed;
        background-color: rgba(0, 0, 0, 0.25);
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 99999;
        visibility: hidden;
        opacity: 0;
        pointer-events: none;
        transition: all 0.3s;
        }
        .modal-window.is-visible {
        visibility: visible;
        opacity: 1;
        pointer-events: auto;
        }
        .modal-window > div {
        width: 400px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 2em;
        background: white;
        }
        .modal-window header {
        font-weight: bold;
        }
        .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
        }

        .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
        }
        .modal-close:hover {
        color: black;
        }
        .accent-primary{
            color: #ff6515;
        }
        .text-center-popup{
            text-align: center;
        }
        .button-location{
            padding: 18px 15px;
            border: 1px solid #d7d7d7;
            background: #fff;
            color: #6d6d70;
            border-radius: 20px;
            text-decoration: none;
            margin-bottom: 13px;
            text-align: center;
            width: 100%;
            max-width: 200px;
            margin: 7px auto;
        }
        .buttons-wrapper{
            display: flex;
            flex-flow: column;
        }
        .side-btn-location{
            position: fixed;
            font: 600 .875rem Montserrat,Arial,helvetica,sans-serif;
            right: 0;
            bottom: 50%;
            z-index: 9999;
            writing-mode: vertical-rl;
            text-orientation: mixed;
            background-color: #fff;
            color:#ff6600; 
            padding: 13px;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            letter-spacing: -.0027777778rem;
            filter: drop-shadow(0 0 0.75rem rgba(0, 0, 0, 0.3));
            transition: all 250ms;
        }
        .side-btn-location:hover, .side-btn-location:focus{
            padding-right: 20px;
        }
    </style>
    <script> 
        function string_to_slug(str)	{
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to   = "aaaaeeeeiiiioooouuuunc------";

            for (var i=0, l=from.length ; i<l ; i++)
            {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;
        }
        function cookieLocation(pais, estado){
            // cria/atualize cookie da localização durante um período e refresh na página
            var formatPais = string_to_slug(pais);
            var formatEstado = string_to_slug(estado);
            const d = new Date();
            d.setTime(d.getTime() + (1*24*60*60*1000));
            let expires = "expires=" + d.toGMTString();
            document.cookie = 'pais' + "=" + formatPais + ";" + expires + ";path=/";
            document.cookie = 'estado' + "=" + formatEstado + ";" + expires + ";path=/";
            location.reload();
        }
        function getCookie(cname) {
            let name = cname + "=";
            let decodedCookie = decodeURIComponent(document.cookie);
            let ca = decodedCookie.split(';');
            for(let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        function toggleModal(){
            $('#open-modal').toggleClass('is-visible');
        }
        function checkCookieModal(){
            let estado = getCookie("estado");
            if (estado != "") {
                jQuery(function($) {
                    $('#open-modal').toggleClass('is-visible');
                });
            }

        } checkCookieModal();
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);                
            } else { 
                document.getElementById("status").innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        function showPosition(position) {
            getReverseGeocodingData(position.coords.latitude, position.coords.longitude);
        }
        function getReverseGeocodingData(lat, lng) {
            let myRequestURL = "https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat="+lat+"&lon="+lng;
            fetch(myRequestURL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
            })
            .then(response => response.json())
            .then(data => {
                $('#open-modal').toggleClass('is-visible');
                var pais = data.address.country;
                var estado = data.address.state;
                cookieLocation(JSON.stringify(pais),JSON.stringify(estado));
            })
            .catch(err => console.log(err.message));
        }
    </script>
    <a class="side-btn-location" href="#open-modal" onclick="toggleModal()">
        <i class="fa fa-map-marker" aria-hidden="true"></i>
        <span id="location-cookie">Localização</span>
    </a>        
    <input type="hidden" id="input-estado" value="">
    <input type="hidden" id="input-pais" value="">
    <div id="open-modal" class="modal-window is-visible">
        <div class="text-center-popup">
            <a href="#" title="Fechar" class="modal-close" onclick="toggleModal()">Fechar</a>
            <div>
                <h1>Onde você está?</h1>
                <p style="margin-bottom:15px">Dessa forma você terá acesso aos produtos e ofertas da sua região.</p>
                <a href="#" class="automatic-location accent-primary" onclick="getLocation()" style="margin-bottom:15px">Utilizar localização automática</a>
                <p><small>Você também pode escolher a localização:</small></p>
            </div>
            <div class="buttons-wrapper">
                <a href="#ce" class="button-location" onclick="cookieLocation('brazil','ceara')">Fortaleza - CE</a>
                <a href="#sp" class="button-location" onclick="cookieLocation('brazil','sao-paulo')">São Paulo - SP</a>
            </div>
        </div>
    </div>
    <?php 
        get_template_part( 'templates/slider', 'home');
    ?>
<?php get_template_part('templates/html', 'footer'); ?>


<!-- "gulp-uglify": "^3.0.2", -->