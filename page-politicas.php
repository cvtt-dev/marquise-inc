<?php /* Template Name: Página Políticas */ ?>
<?php get_template_part('templates/html','header'); ?>

<style>
    .mq-page--politica {padding-top: 126px;}
    .mq-politicas {margin:60px 0px;}
    .mq-politicas-texto {width: 90%;margin: 0 auto;background: #fff; padding: 40px; border-radius: 4px;}
    .mq-politicas-texto h2, h3 {margin-bottom: 30px;font-family: 'Arial',sans-serif;color: #6e6f72;}
    .mq-politicas-texto ol {padding-left: 16px;}
    .mq-politicas-texto ol li::marker{font-family: 'Arial',sans-serif;color: #6e6f72;font-size: 14px;}
    .mq-politicas-texto ol li b, .mq-politicas-texto ul li b, .mq-politicas-texto ul li span {font-family: 'Arial',sans-serif;color: #6e6f72;font-size: 14px;}
    .mq-politicas-texto p {margin: 15px 0px; font-family: 'Arial',sans-serif;color: #6e6f72;font-size: 14px;}

    @media screen and (max-width: 480px) {
    
        .mq-page--politica {
            background: #f04f23;
            padding-top: 56px;
        }

        .mq-politicas-texto {
            width: 100%;
            padding: 20px;
        }

        .mq-politicas-texto h2, h3 {
            font-size: 21px;
            margin-bottom: 15px;
        }

    }
</style>

<?php while (have_posts()) : the_post(); ?>

<div class="mq-page mq-page--politica">
    <section class="mq-politicas">
        <div class="container">
            <div class="mq-politicas-texto">
                <?php the_content(); ?>                    
            </div>
        </div> 
    </section>
</div>
<?php endwhile; ?>
<?php get_template_part('templates/html','footer');?>