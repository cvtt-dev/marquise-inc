<?php /* Template Name: Página Sucesso */ ?>
<?php get_template_part('templates/html','header'); ?>

<?php while (have_posts()) : the_post(); 
    $var = get_post_meta( get_the_id(), '', true);
   
?>

<div class="mq-page mq-page--interna">

    <section class="mq-banner-sucesso">
        <img class="img" src="<?php echo get_template_directory_uri(); ?>/assets/images/suarenda.png" alt="Solicitação recebida">
        <div class="mq-banner-sucesso__wrap">
            <div class="container">
                <div class="mq-banner-sucesso__content">
                    <h2 class="mq-banner-sucesso__title"><?php the_title(); ?></h2>
                    <div class="mq-banner-sucesso__content"><?php the_content(); ?></div>                    
                </div>
            </div> 
        </div> 
    </section>
    
</div>
<?php endwhile; ?>

<?php get_template_part('templates/html','footer');?>