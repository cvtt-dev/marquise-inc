<?php /* Template Name: Simulador */ ?>
<?php get_template_part('templates/html', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>

    <div class="mq-page">

        <?php get_template_part('templates/simulador', 'banner'); ?>

        <?php get_template_part('templates/simulador', 'form'); ?>

    </div>





<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html', 'footer'); ?>