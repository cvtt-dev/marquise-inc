<?php /* Template Name: Cliente - form */ ?>
<?php get_template_part('templates/html', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>

	<div class="mq-page">

		<?php get_template_part('templates/corretor', 'banner'); ?>
	<?php endwhile;
wp_reset_postdata(); ?>

	<?php
	$destaques = get_post_meta(get_the_id(), 'corretor-destaques-group', true);
	$vantagens = get_post_meta(get_the_id(), 'corretor-vantagens-group', true);
	$materiais = get_post_meta(get_the_id(), 'corretor-materiais-group', true);
	$links = get_post_meta(get_the_id(), 'corretor-links-group', true);

	?>



	   

	<section class="mq-section mq-section--simulador">
		<div class="container">
			<div class="seletor-formulario">
				<button class="wq-btn active" data-anchor="fisica">Pessoa Fisica</button>
				<button class="wq-btn" data-anchor="juridica">Pessoa Juridica</button>
			</div>
			<div data-form="fisica" class="visivel mq-multistep-form">

				<?php echo do_shortcode('[contact-form-7 id="3810" title="Cadastro - Cliente (Fisica)"]'); ?>
			</div>
			<div data-form="juridica" class="mq-multistep-form">

				<?php echo do_shortcode('[contact-form-7 id="3890" title="Cadastro - Cliente (Juridica)"]'); ?>
			</div>
		</div>
	</section>






	</div>

	<?php

	if ($_GET["pagina"] == "teste") :


	?>

	<?php
	else :
	?>
		<style>
			/* SCSS PARA ADICIONAR AO ASSETS QUANDO POSSIVEL
			==============================================
			==============================================
			==============================================

			.wpcf7cf_multistep .wpcf7cf_steps-dots .dot {
				.step-title {
					margin-top: 10px;
				}

				&.active {
					border-bottom-color: #FF5722;

					.step-index {
						background-color: #FF5722;
					}
				}

				&.completed {
					border-bottom-color: orange;

					.step-index {
						background-color: orange;
					}
				}
			}

			.mq-multistep-form {
				opacity: 0;
				position: absolute;

				&.visivel {
					opacity: 1;
					position: relative;
				}

				.separador-multi {
					margin: 40px 0 10px;
					border: 2px solid #FF5722;
				}

				form {
					width: 100%;
					font-family: Montserrat, Arial, helvetica, sans-serif;
					color: #696969;

					[type="button"] {
						padding: 10px;
						appearance: none;
						display: flex;
						background-color: #FF5722;
						color: #fff;
						text-transform: uppercase;
						border: none;
						cursor: pointer;
						line-height: 1;
					}

					[type="submit"] {
						padding: 10px;
						appearance: none;
						display: flex;
						background-color: #00b348;
						color: #fff;
						text-transform: uppercase;
						border: none;
						cursor: pointer;
						line-height: 1;
					}

					input {
						width: 100%;
						display: flex;
						border: none;
						background-color: rgba(0, 0, 0, 0.04);
						border-bottom: 1px solid rgba(0, 0, 0, 0.25);
						height: 30px;
						padding-left: 10px;

						&:focus {
							border-bottom: 1px solid #FF5722;
						}
					}

				}

				.cad-wrap {
					display: grid;
					grid-template-columns: 1fr;
					grid-gap: 10px 0;
					margin-bottom: 20px;

					&.cad-wrap-2 {
						grid-template-columns: 1fr 1fr;
						grid-gap: 10px 10px;

						@media only screen and (max-width: 767px) {
							grid-template-columns: 1fr;
						}
					}

					&.cad-wrap-3 {
						grid-template-columns: 1fr 1fr 1fr;
						grid-gap: 10px 10px;

						@media only screen and (max-width: 767px) {
							grid-template-columns: 1fr;
						}
					}

					&.cad-wrap-4 {
						grid-template-columns: 1fr 1fr 1fr 1fr;
						grid-gap: 10px 10px;

						@media only screen and (max-width: 767px) {
							grid-template-columns: 1fr 1fr;
						}
					}

					.form-cad {
						width: 100%;

						br {
							display: none;
						}

						h5 {
							margin-bottom: 10px;
						}

						.radio {
							height: 30px;
							display: flex;
							align-items: center;

							span {

								display: flex;
								flex-direction: row;

								label {
									display: flex;
									flex-direction: row-reverse;
									width: 100%;
									align-items: center;

									input {
										height: 15px;
										width: 15px;
										margin: 0 30px 0 10px;
									}
								}
							}
						}
					}
				}
			}

			.cf7mls-invalid [role="alert"] {
				color: red;
			}

			span.wpcf7-form-control-wrap.email.cf7mls-invalid input,
			span.wpcf7-form-control-wrap.emailRepresentante.cf7mls-invalid input {
				border-bottom-color: red;
			}

			.wpcf7cf_repeater {
				.wpcf7cf_repeater_sub:nth-child(1) {

					.separador-multi,
					.step-title-new {
						display: none;
					}

				}
			}

			.seletor-formulario {
				display: flex;
				align-content: center;
				justify-content: center;
				margin-bottom: 30px;

				.wq-btn {
					padding: 10px;
					appearance: none;
					display: flex;
					background-color: #FF572250;
					color: #fff;
					text-transform: uppercase;
					border: none;
					cursor: pointer;
					line-height: 1;
					margin: 0 20px;
					font-size: 20px;

					&.active {
						background-color: #FF5722;
					}
				}

			}

			FIM DO SCSS
			==============================================
			==============================================
			============================================== */


			.wpcf7cf_multistep .wpcf7cf_steps-dots .dot .step-title {
				margin-top: 10px;
			}

			.wpcf7cf_multistep .wpcf7cf_steps-dots .dot.active {
				border-bottom-color: #FF5722;
			}

			.wpcf7cf_multistep .wpcf7cf_steps-dots .dot.active .step-index {
				background-color: #FF5722;
			}

			.wpcf7cf_multistep .wpcf7cf_steps-dots .dot.completed {
				border-bottom-color: orange;
			}

			.wpcf7cf_multistep .wpcf7cf_steps-dots .dot.completed .step-index {
				background-color: orange;
			}

			.mq-multistep-form {
				opacity: 0;
				position: absolute;
			}

			.mq-multistep-form.visivel {
				opacity: 1;
				position: relative;
			}

			.mq-multistep-form .separador-multi {
				margin: 40px 0 10px;
				border: 2px solid #FF5722;
			}

			.mq-multistep-form form {
				width: 100%;
				font-family: Montserrat, Arial, helvetica, sans-serif;
				color: #696969;
			}

			.mq-multistep-form form [type="button"] {
				padding: 10px;
				appearance: none;
				display: flex;
				background-color: #FF5722;
				color: #fff;
				text-transform: uppercase;
				border: none;
				cursor: pointer;
				line-height: 1;
			}

			.mq-multistep-form form [type="submit"] {
				padding: 10px;
				appearance: none;
				display: flex;
				background-color: #00b348;
				color: #fff;
				text-transform: uppercase;
				border: none;
				cursor: pointer;
				line-height: 1;
			}

			.mq-multistep-form form input {
				width: 100%;
				display: flex;
				border: none;
				background-color: rgba(0, 0, 0, 0.04);
				border-bottom: 1px solid rgba(0, 0, 0, 0.25);
				height: 30px;
				padding-left: 10px;
			}

			.mq-multistep-form form input:focus {
				border-bottom: 1px solid #FF5722;
			}

			.mq-multistep-form .cad-wrap {
				display: grid;
				grid-template-columns: 1fr;
				grid-gap: 10px 0;
				margin-bottom: 20px;
			}

			.mq-multistep-form .cad-wrap.cad-wrap-2 {
				grid-template-columns: 1fr 1fr;
				grid-gap: 10px 10px;
			}

			@media only screen and (max-width: 767px) {
				.mq-multistep-form .cad-wrap.cad-wrap-2 {
					grid-template-columns: 1fr;
				}
			}

			.mq-multistep-form .cad-wrap.cad-wrap-3 {
				grid-template-columns: 1fr 1fr 1fr;
				grid-gap: 10px 10px;
			}

			@media only screen and (max-width: 767px) {
				.mq-multistep-form .cad-wrap.cad-wrap-3 {
					grid-template-columns: 1fr;
				}
			}

			.mq-multistep-form .cad-wrap.cad-wrap-4 {
				grid-template-columns: 1fr 1fr 1fr 1fr;
				grid-gap: 10px 10px;
			}

			@media only screen and (max-width: 767px) {
				.mq-multistep-form .cad-wrap.cad-wrap-4 {
					grid-template-columns: 1fr 1fr;
				}
			}

			.mq-multistep-form .cad-wrap .form-cad {
				width: 100%;
			}

			.mq-multistep-form .cad-wrap .form-cad br {
				display: none;
			}

			.mq-multistep-form .cad-wrap .form-cad h5 {
				margin-bottom: 10px;
			}

			.mq-multistep-form .cad-wrap .form-cad .radio {
				height: 30px;
				display: flex;
				align-items: center;
			}

			.mq-multistep-form .cad-wrap .form-cad .radio span {
				display: flex;
				flex-direction: row;
			}

			.mq-multistep-form .cad-wrap .form-cad .radio span label {
				display: flex;
				flex-direction: row-reverse;
				width: 100%;
				align-items: center;
			}

			.mq-multistep-form .cad-wrap .form-cad .radio span label input {
				height: 15px;
				width: 15px;
				margin: 0 30px 0 10px;
			}

			.cf7mls-invalid [role="alert"] {
				color: red;
			}

			span.wpcf7-form-control-wrap.email.cf7mls-invalid input,
			span.wpcf7-form-control-wrap.emailRepresentante.cf7mls-invalid input {
				border-bottom-color: red;
			}

			.wpcf7cf_repeater .wpcf7cf_repeater_sub:nth-child(1) .separador-multi,
			.wpcf7cf_repeater .wpcf7cf_repeater_sub:nth-child(1) .step-title-new {
				display: none;
			}

			.seletor-formulario {
				display: flex;
				align-content: center;
				justify-content: center;
				margin-bottom: 30px;
			}

			.seletor-formulario .wq-btn {
				padding: 10px;
				appearance: none;
				display: flex;
				background-color: #FF572250;
				color: #fff;
				text-transform: uppercase;
				border: none;
				cursor: pointer;
				line-height: 1;
				margin: 0 20px;
				font-size: 20px;
			}

			.seletor-formulario .wq-btn.active {
				background-color: #FF5722;
			}
		</style>
	<?php
	endif;
	?>
	<?php get_template_part('templates/html', 'footer'); ?>
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.mask.js"></script>
	<script>
		jQuery(function($) {
			$(document).ready(function() {

				$('.seletor-formulario button').on('click', function() {
					$('.seletor-formulario button').removeClass('active');
					$(this).addClass('active');
					seletor = $(this).data('anchor');
					console.log(seletor);
					$('.mq-multistep-form').removeClass('visivel');
					$('.mq-multistep-form[data-form="' + seletor + '"]').addClass('visivel');
				});
				$(window).on('load', function() {
					$('.cep-mask').mask('99999-999');
					$('.phone-mask').mask('(99) 9-9999-9999');
					$('.cpf-mask').mask('999.999.999-99', {
						reverse: true
					});
					$('.cnpj-mask').mask('99.999.999/9999-99', {
						reverse: true
					});

					$('.money-mask').mask('999.999.999.999,99', {
						reverse: true
					});


					$('.money2-mask').mask("#.##9,99", {
						reverse: true
					});

					$('.clear-if-not-match').mask("00/00/0000", {
						clearIfNotMatch: true
					});
					$('.placeholder').mask("00/00/0000", {
						placeholder: "__/__/____"
					});
					$('.fallback').mask("00r00r0000", {
						translation: {
							'r': {
								pattern: /[\/]/,
								fallback: '/'
							},
							placeholder: "__/__/____"
						}
					});
					$('.selectonfocus').mask("00/00/0000", {
						selectOnFocus: true
					});
				})
			})
		})
	</script>