<?php
/*
* Configurações do Tema
* Desenvolvedor: Bruno Lima
* Email: bruno@convertte.com.br
*/

//=========================================================================================
// INCLUDE FUNCTIONS
//=========================================================================================

//===================================Painel================================================
require_once locate_template('/_lib/dashboard.php');
//================================Funções Dashboard========================================
require_once locate_template('/_lib/admin.php');//..................STYLE LOGIN/ADMIN
require_once locate_template('/_lib/filtros.php');//................FILTROS FIELDS
//===================================Features==============================================
require_once locate_template('/_lib/_features/social.php');//.......SOCIAL FIELDS
require_once locate_template('/_lib/_features/blog.php');//.........BLOG FUNCTIONS
require_once locate_template('/_lib/_features/remove.php');//.......CLEAN FUNCTIONS
require_once locate_template('/_lib/_features/excerpt.php');//......EXCERPT FUNCTIONS
require_once locate_template('/_lib/_features/share.php');//........SHARE FUNCTIONS
require_once locate_template('/_lib/_features/bem.php');//..........MENU BEM CSS
require_once locate_template('/_lib/_features/breadcrumbs.php');//..BREADCRUMBS
require_once locate_template('/_lib/_features/cforms.php');//.......CF7 SELECTS
require_once locate_template('/_lib/_features/dataformat.php');//...DATA FORMAT EVENTOS
// require_once locate_template('/_lib/_features/select.php');//....SELECT AJAX
require_once locate_template('/_lib/_features/pagnav.php');//.......PAGINATION

require_once locate_template('/_informacon/ajax.php');//.......INFORMACON

//===================================API's===============================================
require_once locate_template('/_lib/_api/youtube/youtube.php');//....YOUTUBE
require_once locate_template('/_lib/_api/facebook/facebook.php');//..FACEBOOK
// require_once locate_template('/_lib/_api/twitter/twitter.php');//....TWITTER
// require_once locate_template('/_lib/_api/instagram/instagram.php');//INSTAGRAM
//===================================Backend===============================================
require_once locate_template('/_lib/posts.php');//..................POST TYPE FUNCTIONS
require_once locate_template('/_lib/taxonomies.php');//.............TAXONOMIES FUNCTIONS
require_once locate_template('/_lib/thumbs.php');//.................THUMBNAIL FUNCTIONS
require_once locate_template('/_lib/shortcodes.php');//.............SHORTCODES FUNCTIONS
//===================================Tema==================================================
require_once locate_template('/_lib/scripts.php');//................SCRIPTS E CSS
require_once locate_template('/_lib/ajax.php');//...................FUNÇÕES AJAX
//===================================Simulador==================================================
require_once locate_template('/_lib/_simulador/painel.php');
require_once locate_template('/_lib/_simulador/simulador.php');

//=========================================================================================
// ADICIONANDO FAVICON
//=========================================================================================

function blog_favicon() {
  echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_template_directory_uri().'/_lib/_admin/favicon.png" />';
}
add_action('wp_head', 'blog_favicon');


//=========================================================================================
// CRIA SESSÃO + REDIRECT USERS
//=========================================================================================

//REDIRECIONA USUÁRIO PARA FORA DO PAINEL
// function user_login_redirect( $redirect_to, $request, $user  ) {
//   return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url().'/orcamento';
// }
// add_filter( 'login_redirect', 'user_login_redirect', 10, 3);

//CRIA SESSÃO DO NAVEGADOR
add_action('init', 'criaSessao', 1);
function criaSessao() {
  if(!session_id()) {
    session_start();
  }
}


//=========================================================================================
// CONFIGURAÇÕES DO TEMA
//=========================================================================================

function tema_setup() {

  // Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
  register_nav_menus(array(
    'menu_1' => 'Menu Header',
    'menu_2' => 'Menu Footer',
    'menu_3' => 'Menu Mobile',

    
  ));

  add_editor_style('/assets/css/editor-style.css');//..Tell the TinyMCE editor to use a custom stylesheet
  add_theme_support('post-thumbnails');//..............Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
  set_post_thumbnail_size(1200, 0, true);

  //***Switch default core markup for search form, comment form, and comments to output valid HTML5.
  //add_theme_support( 'html5', array('search-form','comment-form','comment-list','gallery','caption'));
  //***Add post formats (http://codex.wordpress.org/Post_Formats)
  //add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

}

add_action('after_setup_theme', 'tema_setup');

//=========================================================================================
// METABOX CLASS (Fields + Taxonomies Fields)
//=========================================================================================

if (is_admin()):
  define('RWMBC_URL', trailingslashit( get_stylesheet_directory_uri() . '/_lib/_metabox'));
  define('RWMBC_DIR', trailingslashit( STYLESHEETPATH . '/_lib/_metabox'));
  require_once RWMBC_DIR . 'functions.php';
  require_once RWMBC_DIR . 'campos.php';
  require_once RWMBC_DIR . 'campos-pages.php';
  require_once RWMBC_DIR . 'campos-taxonomy.php';
  require_once RWMBC_DIR . 'settings.php';
endif;





add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'wp-logo' );
}
