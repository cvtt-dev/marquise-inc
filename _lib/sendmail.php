<?php
/**
* SEND MAIL
* Desenvolvedor: Bruno Kiedis
*/

class sendmail_wp {

    /**
    * Send mail to subscriber
    * @param $message string
    * @param $to string
    * @return boolean
    *
    * @author jovanepires
    */

    public function send_mail_reset( $email ) {

      $to       = $email;
      $subject  = "Informacao de Acesso – Área do cliente Marquise";
      $subject  = utf8_decode($subject);

      //To send HTML mail, the Content-type header must be set
      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $email_sistema = 'noreply@marquiseincorporacoes.com.br';
      $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
      $headers .= 'Reply-To: '.$email. "\r\n";
      // $headers .= 'Cc: '.$email. "\r\n";
      //Additional headers
      $headers .= 'From: '.$email_sistema."\r\n";
      $fonts    = "'Open Sans'";
      $username = substr($email, 0, strpos($email, '@'));
      
      //Body
      $body = '
      <!DOCTYPE html>
      <html lang="pt-BR">
      <head>
          <meta charset="UTF-8">
          <title></title>
          <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,700,300,600,400" rel="stylesheet" type="text/css" />
          <style>html {background: #f5f5f5;}</style>
      </head>
      <body style="font-family: '.$fonts.', serif !important;color: #333; margin: 0;">
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" rules="all" style="text-align:center;background: #f5f5f5;">
              <tr>
                  <td height="591" align="center" valign="top"><br>              <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                              <td height="551" valign="top">
                                  <table width="100%" border="0"  align="center" cellpadding="10" cellspacing="0" style="background: #fff;border-top: 5px solid #f26522;border-bottom: 5px solid #f26522;">
                                      <tr>
                                        <td height="39" style="text-align: center">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td height="100" style="text-align: center; padding:0;">
                                        <img src="https://www.marquiseincorporacoes.com.br/wp-content/themes/marquise/assets/images/marquiseincorporacoes-topo.png" alt="Marquise Incorporações" />
                                      </td>
                                    </tr>
                                    <tr>
                                        <td height="100" colspan=2 style="text-align: center;  padding:25px 0; font-size: 14px; font-weight: bold;">
                                            <h3 style="font-weight: 400 !important; font-size: 20px; text-transform: uppercase; max-width: 375px; margin: 0 auto; border-bottom: 2px solid #f26522;">Acesso à área do Cliente</h3>
                                            <h2 style="font-weight: 400 !important; font-size: 40px; ">Olá, '. $username .' tudo bem?</h2>
                                            <p style="font-weight: 400 !important; font-size: 16px; max-width: 330px; margin: 0 auto;">
                                              Para acessar a Área do Cliente Marquise em nosso site, siga as instruções abaixo.
                                            </p>
                                            
                                          </td>
                                      </tr>
                                    <tr>
                                      <td height="150" style="text-align: center; padding:25px 0;">
                                        <img src="https://www.marquiseincorporacoes.com.br/wp-content/themes/marquise/assets/images/body_mail.png" alt="Marquise Incorporações" />
                                      </td>
                                    </tr>
                              		<tr>
                                      <td height="50" style="text-align: center; padding:0 0 25px;">
                                        <a href="https://www.marquiseincorporacoes.com.br" style="width: 250px; height: 50px; margin: 0 auto;background: #ff5722; color: white; display: block; text-align: center; line-height: 50px; text-transform: uppercase; text-decoration: none;">Ir para o site</a> </td>
                                    </tr>
                                       <td height="100" style="text-align: center; padding:0;">
                                        <img src="https://www.marquiseincorporacoes.com.br/wp-content/themes/marquise/assets/images/footer.png" alt="Marquise Incorporações" />
                                      </td>
                                      <tr>
                                        <td height="41" colspan=2 style="text-align: center">&nbsp;</td>
                                    </tr>
                                  </table>
                              </td>
                        </tr>
                      </table>
                    <br>
                  </td>
            </tr>
          </table>
      </body>
      </html>';
      //return $body;

      //Mail it
      return (int) mail($to, $subject, $body, $headers);
    }

}