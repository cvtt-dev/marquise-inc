<?php
/*
* Configurações de Taxonomias
* Desenvolvedor: Bruno Kiedis
*/

//=========================================================================================
// EMPRESAS
//=========================================================================================
$labels = array(
  'name'              => _x( 'Empresas', 'Taxonomy General Name', 'text_domain' ),
  'menu_name'         => __( 'Tipo', 'textdomain' ),
);
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'empresas'), // url
);

register_taxonomy( 'empresas-tipo', array( 'empresas' ), $args ); // slug do post



//=========================================================================================
// EMPREENDIMENTOS - TIPO
//=========================================================================================
$labels = array(
  'name'              => _x( 'Tipo', 'Taxonomy General Name', 'text_domain' ),
  'menu_name'         => __( 'Tipo', 'textdomain' ),
);
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'tipo-empreendimentos'), // url
);

register_taxonomy( 'tipo-empreendimentos', array( 'mar_empreendimentos' ), $args ); // slug do post


//=========================================================================================
// EMPREENDIMENTOS - STATUS
//=========================================================================================
$labels = array(
  'name'              => _x( 'Status', 'Taxonomy General Name', 'text_domain' ),
  'menu_name'         => __( 'Status', 'textdomain' ),
);
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'status-empreendimentos'), // url
);

register_taxonomy( 'status-empreendimentos', array( 'mar_empreendimentos' ), $args ); // slug do post



//=========================================================================================
// EMPREENDIMENTOS - BAIRRO
//=========================================================================================
$labels = array(
  'name'              => _x( 'Bairro', 'Taxonomy General Name', 'text_domain' ),
  'menu_name'         => __( 'Bairro', 'textdomain' ),
);
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'bairro-empreendimentos'), // url
);

register_taxonomy( 'bairro-empreendimentos', array( 'mar_empreendimentos' ), $args ); // slug do post


//=========================================================================================
// EMPREENDIMENTOS - DORMITÓRIOS
//=========================================================================================
$labels = array(
  'name'              => _x( 'Dormitorios', 'Taxonomy General Name', 'text_domain' ),
  'menu_name'         => __( 'Dormitorios', 'textdomain' ),
);
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'qdtdomitorios-empreendimentos'), // url
);

register_taxonomy( 'qdtdomitorios-empreendimentos', array( 'mar_empreendimentos' ), $args ); // slug do post



//=========================================================================================
// FLUSH REWRITE
//=========================================================================================

function custom_taxonomy_flush_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_action('init', 'custom_taxonomy_flush_rewrite');