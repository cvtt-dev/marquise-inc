<?php
/**
* Custom Post Types
* Desenvolvedor: Bruno Kiedis
*/

//=========================================================================================
// POST TYPE SLIDER
//=========================================================================================

function post_type_slider_register() {
    $labels = array(
        'name' => 'Slider',
        'singular_name' => 'Slider',
        'menu_name' => 'Slides',
        'add_new' => _x('Adicionar Slide', 'item'),
        'add_new_item' => __('Adicionar Novo Slide'),
        'edit_item' => __('Editar Slide'),
        'new_item' => __('Novo Slide')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slider'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-archive',
        'supports' => array('title','editor', 'thumbnail')
    );
    register_post_type('slider', $args);
}
add_action('init', 'post_type_slider_register');



//=========================================================================================
// POST TYPE EMPRESAS
//=========================================================================================

function post_type_empresa_register() {
    $labels = array(
        'name' => 'Empresa',
        'singular_name' => 'Empresa',
        'menu_name' => 'Empresas',
        'add_new' => _x('Adicionar Empresa', 'item'),
        'add_new_item' => __('Adicionar Novo Empresa'),
        'edit_item' => __('Editar Empresa'),
        'new_item' => __('Novo Empresa')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'empresas'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-multisite',
        'supports' => array('title','', 'thumbnail')
    );
    register_post_type('empresas', $args);
}
add_action('init', 'post_type_empresa_register');



//=========================================================================================
// POST TYPE EMPREENDIMENTOS
//=========================================================================================

function post_type_empreendimento_register() {
    $labels = array(
        'name' => 'Empreendimento',
        'singular_name' => 'Empreendimento',
        'menu_name' => 'Empreendimentos',
        'add_new' => _x('Adicionar Empreendimento', 'item'),
        'add_new_item' => __('Adicionar Novo Empreendimento'),
        'edit_item' => __('Editar Empreendimento'),
        'new_item' => __('Novo Empreendimento')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
       
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'empreendimentos'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-cart',
        'show_in_rest' => true,
        'supports' => array('title','editor', 'thumbnail')
    );
    register_post_type('mar_empreendimentos', $args);
}
add_action('init', 'post_type_empreendimento_register');



//=========================================================================================
// POST TYPE CAMPANHAS
//=========================================================================================

function post_type_campanha_register() {
    $labels = array(
        'name' => 'Campanha',
        'singular_name' => 'Campanha',
        'menu_name' => 'Campanhas',
        'add_new' => _x('Adicionar Campanha', 'item'),
        'add_new_item' => __('Adicionar Novo Campanha'),
        'edit_item' => __('Editar Campanha'),
        'new_item' => __('Novo Campanha')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'mar_camapanhas'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-excerpt-view',
        'supports' => array('title','editor', 'thumbnail')
    );
    register_post_type('mar_camapanhas', $args);
}
add_action('init', 'post_type_campanha_register');




//=========================================================================================
// POST TYPE CORRETORES
//=========================================================================================

function post_type_corretores_register() {
    $labels = array(
        'name' => 'Corretores',
        'singular_name' => 'Corretor',
        'menu_name' => 'Corretores',
        'add_new' => _x('Adicionar Corretor', 'item'),
        'add_new_item' => __('Adicionar Novo Corretor'),
        'edit_item' => __('Editar Corretor'),
        'new_item' => __('Novo Corretor')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'mar_corretores'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-businessman',
        'supports' => array('title')
    );
    register_post_type('mar_corretores', $args);
}
add_action('init', 'post_type_corretores_register');
