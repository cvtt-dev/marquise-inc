<?php

add_filter('mb_settings_pages', 'prefix_options_page');

function prefix_options_page($settings_pages)
{

  $settings_pages[] = array(
    'id'          => 'theme-options',
    'option_name' => 'options_gerais',
    'menu_title'  => __('Opções do Site', 'textdomain'),
    'parent'      => 'themes.php',
  );

  return $settings_pages;
}

add_filter('rwmb_meta_boxes', 'prefix_options_meta_boxes');

function prefix_options_meta_boxes($meta_boxes)
{

  $meta_boxes[] = array(

    'id'             => 'settings_catalogo',
    'title'          => 'Informações Home Page',
    'context'        => 'normal',
    'settings_pages' => 'theme-options',
    'fields'         => array(

      array(
        'id' => 'popup_img',
        'type' => 'image_advanced',
        'name' => 'Imagem do popup',
        'max_file_uploads' => 1
      ),

      array(
        'type' => 'heading',
        'name' => 'Redes Sociais',
        'id' => ''
      ),

      array(
        'id' => 'group_redes',
        'type' => 'group',
        'clone' => true,
        'sort_clone' => true,
        'max_clone' => '4',
        'fields' => array(

          array(
            'name'       => 'Link da mídia',
            'id'         => 'link_midia',
            'type'       => 'text',
            'required'   => true
          ),

          array(
            'name'       => 'Ícone da mídia',
            'id'         => 'icone_midia',
            'type'       => 'text',
            'required'   => true
          ),
        ),
      ),





    ),
  );

  $meta_boxes[] = array(

    'id'             => 'settings_gtm',
    'title'          => 'Informações Home Page',
    'context'        => 'side',
    'settings_pages' => 'theme-options',
    'fields'         => array(

      array(
        'id' => 'gtm_head',
        'type' => 'textarea',
        'name' => 'Codigo do GTM (HEAD)',
      ),

      array(
        'id' => 'gtm_body',
        'type' => 'textarea',
        'name' => 'Codigo do GTM (BODY)',
      ),
   
    ),
  );
  $meta_boxes[] = array(

    'id'             => 'settings_client',
    'title'          => 'Informações Área do Cliente',
    'context'        => 'side',
    'settings_pages' => 'theme-options',
    'fields'         => array(

      array(
        'id' => 'client_form_shortcode',
        'type' => 'text',
        'name' => 'Shortcode gerado no contact form 7',
      ),

   
   
    ),
  );
  return $meta_boxes;
}
