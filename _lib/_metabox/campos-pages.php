<?php

add_filter('rwmb_meta_boxes', 'wpcf_meta_boxes_pages');
function wpcf_meta_boxes_pages($meta_boxes)
{

  //=========================================================================================
  //  PAGE: Corretor
  //=========================================================================================
  $meta_boxes[] = array(
    'id'             => 'info_info',
    'title'          => 'Informações da Página',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'template'  => array('page-corretor.php'),
    ),
    'fields' => array(
			array (
				'id' => $prefix . 'corretor-destaques-group',
				'type' => 'group',
				'name' => esc_html__( 'Destaques', 'text-domain' ),
				'fields' => array(
					array (
						'id' => $prefix . 'corretor-destaques-capa',
						'type' => 'image_advanced',
						'name' => esc_html__( 'capa', 'text-domain' ),
						'max_file_uploads' => 1,
						'max_status' => false,
					),
					array (
						'id' => $prefix . 'corretor-destaques-url',
						'type' => 'url',
						'name' => esc_html__( 'URL Field', 'text-domain' ),
					),
					array (
						'id' => $prefix . 'corretor-destaques-target',
						'name' => esc_html__( 'Tipo de link', 'text-domain' ),
						'type' => 'radio',
						'std' => 'Externo',
						'options' => array(
							'_blank' => esc_html__( 'Externo', 'text-domain' ),
							'_self' => esc_html__( 'Interno', 'text-domain' ),
						),
					),
				),
				'clone' => 1,
				'sort_clone' => 1,
				'default_state' => 'collapsed',
				'collapsible' => true,
				'group_title' => '{corretor-destaques-url}',
				'tab' => 'tab-corretor-destaques',
			),
			array (
				'id' => $prefix . 'corretor-vantagens-group',
				'type' => 'group',
				'name' => esc_html__( 'Vantagens', 'text-domain' ),
				'fields' => array(
					array (
						'id' => $prefix . 'corretor-vantagens-icon',
						'type' => 'image_advanced',
						'name' => esc_html__( 'Icone', 'text-domain' ),
						'max_file_uploads' => 1,
						'max_status' => false,
					),
					array (
						'id' => $prefix . 'corretor-vantagens-titulo',
						'type' => 'text',
						'name' => esc_html__( 'Titulo', 'text-domain' ),
					),
					array (
						'id' => $prefix . 'corretor-vantagens-content',
						'type' => 'textarea',
						'name' => esc_html__( 'Descrição', 'text-domain' ),
					),
				),
				'clone' => 1,
				'sort_clone' => 1,
				'default_state' => 'collapsed',
				'collapsible' => true,
				'group_title' => '{corretor-links-title} - {corretor-vantagens-titulo}',
				'tab' => 'tab-corretor-vantagens',
      ),
      

 
			array (
				'id' => $prefix . 'corretor-materiais-group',
				'type' => 'group',
				'name' => esc_html__( 'Links', 'text-domain' ),
				'fields' => array(
       
          




          array (
						'id' => $prefix . 'corretor-materiais-icon',
						'type' => 'image_advanced',
						'name' => esc_html__( 'Icone', 'text-domain' ),
						'max_file_uploads' => 1,
						'max_status' => false,
					),
					array (
						'id' => $prefix . 'corretor-materiais-titulo',
						'type' => 'text',
						'name' => esc_html__( 'Titulo', 'text-domain' ),
					),
					array (
						'id' => $prefix . 'corretor-materiais-content',
						'type' => 'textarea',
						'name' => esc_html__( 'Descrição', 'text-domain' ),
					),
          











				),
				'clone' => 1,
				'sort_clone' => 1,
				'default_state' => 'collapsed',
				'collapsible' => true,
				'group_title' => '{corretor-materiais-url}',
				'tab' => 'tab-corretor-material',
      ),
      









			array (
				'id' => $prefix . 'corretor-links-group',
				'type' => 'group',
				'name' => esc_html__( 'Links', 'text-domain' ),
				'fields' => array(
          array (
						'id' => $prefix . 'corretor-links-title',
						'type' => 'text',
						'name' => esc_html__( 'Titulo', 'text-domain' ),
					),
					array (
						'id' => $prefix . 'corretor-links-icon',
						'type' => 'image_advanced',
						'name' => esc_html__( 'Icone', 'text-domain' ),
						'max_file_uploads' => 1,
						'max_status' => false,
					),
					array (
						'id' => $prefix . 'corretor-links-url',
						'type' => 'url',
						'name' => esc_html__( 'Link', 'text-domain' ),
					),
					array (
						'id' => $prefix . 'corretor-links-target',
						'name' => esc_html__( 'Tipo de link', 'text-domain' ),
						'type' => 'radio',
						'std' => 'Externo',
						'options' => array(
							'_blank' => esc_html__( 'Externo', 'text-domain' ),
							'_self' => esc_html__( 'Interno', 'text-domain' ),
						),
					),
				),
				'clone' => 1,
				'sort_clone' => 1,
				'default_state' => 'collapsed',
				'collapsible' => true,
				'group_title' => '{corretor-links-url}',
				'tab' => 'tab-corretor-links',
      ),
      


      array (
        'id' => $prefix . 'corretor-form',
        'type' => 'textarea',
        'name' => esc_html__( 'Descrição', 'text-domain' ),
        'tab' => 'tab-corretor-form',
      ),
    ),
    

		'tab_style' => 'default',
		'tab_wrapper' => true,
		'tabs' => array(
      'tab-corretor-vantagens' => array(
        'label' => 'Vantagens',
        'icon' => 'dashicons-star-empty',
      ),
			'tab-corretor-destaques' => array(
				'label' => 'Destaques',
				'icon' => 'dashicons-editor-code',
			),
      'tab-corretor-material' => array(
        'label' => 'Material',
        'icon' => 'dashicons-category',
      ),
			'tab-corretor-links' => array(
				'label' => 'Links',
				'icon' => 'dashicons-admin-links',
      ),
      
      'tab-corretor-form' => array(
				'label' => 'Form',
				'icon' => 'dashicons-format-aside',
			),
		),
	);
  //=========================================================================================
  //  PAGE: INSTITUCIONAL
  //=========================================================================================
  $meta_boxes[] = array(
    'id'             => 'banner_info',
    'title'          => 'Banner Destaque',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'relation'  => 'OR',
      'template'  => array('page-institucional.php'),
    ),
    'fields'     => array(

      array(
        'name'       => 'Titulo Destaque',
        'id'         => "title_destaque",
        'type'       => 'text'
      ),

      array(
        'name'       => 'Link',
        'id'         => "linkBtn",
        'type'       => 'text',
        'required'   => true
      ),

      array(
        'name'       => 'Texto Botão',
        'id'         => "txtBtn",
        'type'       => 'text'
      ),

    ),
  );

  // SOBRE
  $meta_boxes[] = array(
    'id'             => 'sobre',
    'title'          => 'Informações Gerais',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'relation'  => 'OR',
      'template'  => array('page-institucional.php'),
    ),
    'fields'     => array(

      array(
        'id' => 'img_destaque',
        'name' => 'Imagem Destaque',
        'type' => 'image_advanced',
        'max_file_uploads' => 1

      ),

      array(
        'name'       => 'ID Vídeo',
        'id'         => 'id_video',
        'type'       => 'text',
        'required'   => false
      ),

      array(
        'name'       => 'Titulo',
        'id'         => "title_sobre",
        'type'       => 'text'
      ),

      array(
        'name'       => 'Descrição',
        'id'         => 'desc_sobre',
        'type'       => 'wysiwyg',
        'required'   => false
      ),

    ),
  );

  // CERTIFICAÇÕES
  $meta_boxes[] = array(
    'id'             => 'sobre_cert',
    'title'          => 'Prêmios e Certificações',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'relation'  => 'OR',
      'template'  => array('page-institucional.php'),
    ),
    'fields'     => array(









      array (
				'id' => 'group_certificados',
				'type' => 'group',
        'name' => '',
				'fields' => array(
          // array (
          //   'id' => 'titulo',
          //   'type' => 'text',
          //   'name' => 'Titulo do Certificado',
          //   'desc' => '',
          // ), 
          array (
            'id' => 'medalha',
            'type' => 'single_image',
            'name' => 'Medalha de certificação',
            'desc' => 'Caso não tenha uma medalha com o logo da marquise será adicionada',
          ),
          
					array (
            'id' => 'texto',
						'name' => 'Descrição da certificação',
            'type' => 'wysiwyg',
            
            'options' => array(
              'textarea_rows' => 2,
              'teeny'         => true,
          ),
					),
				),
        'group_title' => 'Item',
				'clone' => 1,
				'sort_clone' => 1,
				'default_state' => 'expanded',
        'collapsible' => true,
        'add_button' => 'Adicionar Certificado',
			),








      
      array(
        'name'       => '',
        'id'         => "sobre_certificacao",
        'type'       => 'wysiwyg',
      ),


    ),
  );


  // OBJETIVOS
  $meta_boxes[] = array(
    'id'             => 'sobre_objetivo',
    'title'          => 'Objetivos',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'relation'  => 'OR',
      'template'  => array('page-institucional.php'),
    ),
    'fields'     => array(

      array(
        'id' => 'img_missao',
        'name' => 'Imagem',
        'type' => 'image_advanced',
        'max_file_uploads' => 1

      ),

      array(
        'id'         => "missao_titulo",
        'name'       => 'Titulo',
        'type'       => 'text'
      ),

      array(
        'id'         => "sobre_missao",
        'name'       => 'Missão',
        'type'       => 'textarea'
      ),


      array(
        'id' => 'img_visao',
        'name' => 'Imagem',
        'type' => 'image_advanced',
        'max_file_uploads' => 1

      ),

      array(
        'id'         => "visao_titulo",
        'name'       => 'Titulo',
        'type'       => 'text'
      ),

      array(
        'id'         => "sobre_visao",
        'name'       => 'Visão',
        'type'       => 'textarea'
      ),

      array(
        'id' => 'img_valores',
        'name' => 'Imagem',
        'type' => 'image_advanced',
        'max_file_uploads' => 1

      ),

      array(
        'id'         => "valores_titulo",
        'name'       => 'Titulo',
        'type'       => 'text'
      ),

      array(
        'id'         => "sobre_valores",
        'name'       => 'Valores',
        'type'       => 'wysiwyg'
      ),



    ),
  );


  //=========================================================================================
  //  PAGE: FALE CONOSCO
  //=========================================================================================
  $meta_boxes[] = array(
    'id'             => 'fale_conosco',
    'title'          => 'Formulário',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'relation'  => 'OR',
      'template'  => array('page-contato.php'),
    ),
    'fields'     => array(

      array(
        'name'       => 'Short Code do formulário',
        'id'         => "short_code",
        'type'       => 'text'
      ),

    ),
  );

  //=========================================================================================
  //  PAGE: TRABALHE CONOSCO
  //=========================================================================================
  $meta_boxes[] = array(
    'id'             => 'trab_conosco',
    'title'          => 'Formulário',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'relation'  => 'OR',
      'template'  => array('page-trabalhe-conosco.php'),
    ),
    'fields'     => array(

      array(
        'name'       => 'Short Code do formulário',
        'id'         => "short_code_form",
        'type'       => 'text'
      ),

    ),
  );

  //=========================================================================================
  //  PAGE: VENDA SEU TERRENO 
  //=========================================================================================
  $meta_boxes[] = array(
    'id'             => 'venda_terreno',
    'title'          => 'Formulário',
    'context'        => 'normal',
    'pages' => array('page'),
    'include' => array(
      'relation'  => 'OR',
      'relation'  => 'OR',
      'template'  => array('page-venda-terreno.php'),
    ),
    'fields'     => array(

      array(
        'name'       => 'Short Code do formulário',
        'id'         => "short_code_venda",
        'type'       => 'text'
      ),

    ),
  );
  //=========================================================================================
  // END DEFINITION OF META BOXES
  //=========================================================================================
  return $meta_boxes;
}
