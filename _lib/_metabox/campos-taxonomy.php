<?php

//=========================================================================================
// START DEFINITION OF META BOXES
//=========================================================================================

add_filter('rwmb_meta_boxes', 'wpcf_meta_boxes_tax');
function wpcf_meta_boxes_tax($meta_boxes)
{

    //=========================================================================================
    // TAXONOMIES FIELDS
    //=========================================================================================

    $meta_boxes[] = array(
        'title'      => '',
        'taxonomies' => array('category'), // List of taxonomies. Array or string
        'fields' => array(
            array(
                'name' => 'Cor Destaque',
                'id'   => 'cat_color',
                'type' => 'color',
            ),
        ),
    );


    $meta_boxes[] = array(
        'title'      => 'Campos padrões',
        'taxonomies' => array('solucao'), // List of taxonomies. Array or string
        'fields' => array(
            array(
                'name' => 'Imagem Destaque',
                'id'   => 'tax_image',
                'type' => 'image_advanced',
            ),
            array(
                'name' => 'Icone Destaque',
                'id'   => 'tax_image_icone',
                'type' => 'image_advanced',
            ),
            array(
                'name' => 'Descrição Home',
                'id'   => 'tax_desc_home',
                'type' => 'textarea',
            ),
        ),
    );

    //=========================================================================================
    // END DEFINITION OF META BOXES
    //=========================================================================================
    return $meta_boxes;
}
