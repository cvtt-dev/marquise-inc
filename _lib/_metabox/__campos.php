<?php

$prefix = 'wpcf_';
add_filter('rwmb_meta_boxes', 'wpcf_meta_boxes');
function wpcf_meta_boxes($meta_boxes)
{

    //=========================================================================================
    // SLIDER
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'slider_info',
        'title'          => 'Slide',
        'context'        => 'normal',
        'post_types' => array('slider'),
        'fields'     => array(

            array(
                'name'       => 'Link',
                'id'         => "slide_link",
                'type'       => 'text',
                'required'   => false,
                'columns' => 4,
                'admin_columns' => 'after title',
            ),

            array(
                'name'       => 'Texto Botão',
                'id'         => "slide_txt_btn",
                'type'       => 'text',
                'columns' => 4,
                'admin_columns' => 'after title',
            ),

            array(
				'id' =>'slide_pos_link',
                'name' =>'Posição do link',
				'type' => 'radio',
                'placeholder' => '',
                'admin_columns' => 'after title',
				'options' => array(
					'btn' => 'botão',
					'img' => 'Imagem',
				),
                'inline' => 'true',
                'std' => 'btn',
			),

        ),
    );

    //=========================================================================================
    // EMPREENDIMENTO IMAGEM DESTAQUE
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'img_thumb_emp',
        'title'          => 'Imagem do banner',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(


            array(
                'id'         => "img_thumb",
                'name'       => '',
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),

        ),
    );


    $meta_boxes[] = array(
        'id'             => 'tag_percentual',
        'title'          => 'Home - Percentual de Vendas',
        'context'        => 'side',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(


            array(
                'id'         => "emp_percentual",
                'name'       => 'Exemplo: 100% vendido',
                'type' => 'text'
            ),

        ),
    );

    //=========================================================================================
    // CARACTERISTICAS DO EMPREENDIMENTO
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'condicoes',
        'title'          => 'Condições Especiais',
        'context'        => 'side',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(


            array(
                'id'        => "condicoes_link",
                'name'      => 'Condições Especiais',
                'type'      => 'radio',
                'options'   => array(1 => 'Sim', 0 => 'Não'),
                'std'       => 0,
                'admin_columns' => 'after title',
            ),
        ),
    );


    //=========================================================================================
    // CARACTERISTICAS DO EMPREENDIMENTO
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'caracteristicas',
        'title'          => 'Caracteristicas do Empreendimento',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(

            array(
                'id'         => 'stand_valor',
                'name'       => 'Valor',
                'type'       => 'text',
                'required'   => false
            ),

            array(
                'id'         => 'txt_juridico',
                'name'       => 'Texto Juridico',
                'type'       => 'text',
                'required'   => false
            ),

            array(
                'id'         => 'descricao',
                'name'       => 'Breve Descrição',
                'type'       => 'textarea',
                'required'   => false
            ),

            array(
                'id'         => 'tamanho',
                'name'       => 'Tamanho',
                'type'       => 'text',
                'required'   => false
            ),

            array(
                'id'         => 'qtdquartos',
                'name'       => 'Quantidade de Quartos',
                'type'       => 'text',
                'required'   => false
            ),

            array(
                'id'         => 'qtdgaragem',
                'name'       => 'Vagas de Garagem',
                'type'       => 'text',
                'required'   => false
            ),

            array(
                'id'         => 'bairro',
                'name'       => 'Bairro - Cidade',
                'type'       => 'text',
                'required'   => false
            ),

        ),
    );


    //=========================================================================================
    // GALERIA DO EMPREENDIMENTO
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'galeria_emp',
        'title'          => 'Galeria do empreendimento',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(

            array(
                'id'         => "img_emp_galeria",
                'name'       => 'Imagens',
                'type' => 'image_advanced',
                'max_file_uploads' => -1
                
            ),
            

        ),
    );

    //=========================================================================================
    // GALERIA DE PLANTAS DO EMPREENDIMENTO
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'gal_plantas',
        'title'          => 'Galeria de Plantas',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(

            array(
                'id'     => 'plantas',
                'type'   => 'group',
                'clone' => true,
                'fields' => array(

                    array(
                        'id'         => "emp_plantas",
                        'name'       => 'Imagem da Planta',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1
                    ),

                    array(
                        'id'         => "emp_plantas_title",
                        'name'       => 'Titulo de Destaque',
                        'type' => 'text'
                    ),

                    array(
                        'id'         => "emp_plantas_desc",
                        'name'       => 'Descrição da Planta',
                        'type' => 'textarea'
                    ),
                ),
            ),
        ),
    );

    //=========================================================================================
    // GALERIA IMPLANTAÇÃO
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'gal_implantacao',
        'title'          => 'Galeria Implantação',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(


            array(
                'id'         => "emp_implantacao",
                'name'       => 'Galeria Implantação',
                'type' => 'image_advanced',
                'max_file_uploads' => 6
            ),


        ),
    );

    //=========================================================================================
    // GALERIA DECORADO
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'gal_decorado',
        'title'          => 'Galeria Decorado',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(


            array(
                'id'         => "emp_decorado",
                'name'       => 'Decorado',
                'type' => 'image_advanced',
                'max_file_uploads' => 6
            ),


        ),
    );

    //=========================================================================================
    // GALERIA ANDAMENTO DA OBRA
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'gal_andamento',
        'title'          => 'Galeria Andamento da Obra',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(


            array(
                'id'         => "emp_and_obra",
                'name'       => 'Andamento',
                'type' => 'image_advanced',
                'max_file_uploads' => 6
            ),


        ),
    );



    //=========================================================================================
    // STAND DE VENDAS DO EMPREENDIMENTO
    //=========================================================================================
    $meta_boxes[] = array(
        'id' => 'emp_caract',
        'title' => 'Stand de Vendas',
        'post_types' => array('mar_empreendimentos'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(

            array(
                'id'     => 'empreendimento_caracteristicas',
                'type'   => 'group',
                'clone' => false,
                'fields' => array(

                    array(
                        'id'         => 'stand_ende',
                        'name'       => 'Endereço',
                        'type'       => 'text',
                        'required'   => false
                    ),

                    array(
                        'id'         => 'stand_tell',
                        'name'       => 'Telefone',
                        'type'       => 'text',
                        'required'   => false
                    ),

                    array(
                        'id'         => 'ficha_tecnica',
                        'name'       => 'Ficha técnica',
                        'type'       => 'wysiwyg',
                        'required'   => false
                    ),

                    array(
                        'id'         => 'info_empreendimeto',
                        'name'       => 'Informações do Empreendimento',
                        'type'       => 'wysiwyg',
                        'required'   => false
                    ),

                ),
            ),

        )
    );


    


    //=========================================================================================
    // TOUR 360
    //=========================================================================================
    $meta_boxes[] = array(
        'id' => 'galeria',
        'title' => 'Tour 360 º',
        'post_types' => array('mar_empreendimentos'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(

            array(
                'id'         => 'tour',
                'name'       => 'Link do Tour 360º',
                'type'       => 'text',
                'required'   => false
            ),

        )
    );

    //=========================================================================================
    // VÍDEO 
    //=========================================================================================
    $meta_boxes[] = array(
        'id' => 'emp_video',
        'title' => 'Vídeo',
        'post_types' => array('mar_empreendimentos'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(

            array(
                'name'       => 'ID Vídeo Youtube',
                'id'         => 'play_video',
                'type'       => 'text',
            ),

            array(
                'id' => 'img_trab',
                'type' => 'image_advanced',
                'name' => 'Insira uma imagem',
                'desc' => 'Imagem de fundo',
                'max_file_uploads' => 1,
            ),

            array(
                'id'         => 'title-conceito',
                'name'       => 'Título Destaque',
                'type'       => 'textarea',
                'required'   => false
            ),

            array(
                'id'         => 'desc_conceito',
                'name'       => 'Conceito',
                'type'       => 'wysiwyg',
                'required'   => false
            ),
        )
    );

    // PAGE DETALHES EMPREENDIMENTO
    $meta_boxes[] = array(
        'id'             => 'emp_localizacao',
        'title'          => 'Localização',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(

            array(
                'id' => 'logradouro_emp',
                'type' => 'text',
                'name' => 'Logradrou/Rua/Av',
                'tab' => 'info_tab',
            ),

            array(
                'id' => 'estado_emp',
                'type' => 'text',
                'name' => 'Estado',
                'tab' => 'info_tab',
            ),

            array(
                'id' => 'cidade_emp',
                'type' => 'text',
                'name' => 'Cidade',
                'tab' => 'info_tab',
            ),

            array(
                'id' => 'bairro_emp',
                'type' => 'text',
                'name' => 'Bairro',
                'tab' => 'info_tab',
            ),

            array(
                'id' => 'lat_long_emp',
                'type' => 'text',
                'name' => 'Latitude/Longitude',
                'tab' => 'info_tab',
                'admin_columns' => array('position' => 'after title', 'title' => 'Latitude/Longitude', 'sort' => false),
            ),
        ),
    );

    // AREA DO CLIENTE
    $meta_boxes[] = array(
        'id' => 'planilha-emp',
        'title' => 'Área do Cliente/Simulador',
        'post_types' => array('mar_empreendimentos'),
        'context' => 'side',
        'priority' => 'high',

        // List of meta fields
        'fields' => array(

            array(
                'type' => 'heading',
                'name' => 'Área do Cliente',
            ),

            array(
                'name'	=> 'Código do empreendimento',
                'desc'  => 'Atenção: Necessário adicionar código do empreendimento(INFORMACON) para funcionamento dos manuais.',
                'id'	=> "wpcf_cod_empreendimento",
                'type' => 'text',
            ),

            array(
                'name'             => 'Planilha (Proprietário)',
                'id'               => 'emp_planilha',
                'type'             => 'file_advanced',
                'max_file_uploads' => 1,
            ),

            array(
                'name'             => 'Planilha (Síndico)',
                'id'               => 'emp_planilha_sind',
                'type'             => 'file_advanced',
                'max_file_uploads' => 1,
            ),

            array(
                'type' => 'heading',
                'name' => 'Dados Simulador',
            ),

            array(
                'name'	=> 'Faixa de renda',
                'id'	=> "faixa_renda",
                'type' => 'text',
            ),

            array(
                'name'	=> 'Desconto Colaboradores',
                'id'	=> "wpcf_desconto_renda",
                'type' => 'range',
                'min'  => 0,
                'max'  => 100,
                'std'  => '0',
            ),
        )
    );

    
    //=========================================================================================
    // EMPREENDIMENTO BOOK
    //=========================================================================================
    $meta_boxes[] = array(
        'id'             => 'img_book',
        'title'          => 'Arquivo do Book',
        'context'        => 'normal',
        'post_types' => array('mar_empreendimentos'),
        'fields'     => array(


            array(
                'id'         => "arquivo_book",
                'name'       => '',
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),

        ),
    );


    // DADOS DO CRM
    $meta_boxes[] = array(
        'id' => 'dados_crm',
        'title' => 'Dados do CRM',
        'post_types' => array('mar_empreendimentos'),
        'context' => 'side',
        'priority' => 'high',

        // List of meta fields
        'fields' => array(

             array(
                'id'     => 'empreendimento_dados_crm',
                'type'   => 'group',
                'clone' => false,
                'fields' => array(


                    array(
                        'id'         => 'filial',
                        'name'       => 'Filial',
                        'type'       => 'text',
                        'required'   => false
                    ),

                    array(
                        'id'         => 'cod_emp',
                        'name'       => 'Codigo do emp.',
                        'type'       => 'text',
                        'required'   => false
                    ),

                    array(
                        'id'         => 'color',
                        'name'       => 'color',
                        'type'       => 'text',
                        'required'   => false
                    ),
                ),
            ),

        )
    );

    $meta_boxes[] = array(
        'id'             => 'emp_destaque',
        'title'          => 'Destacar Empreendimento',
        'context'        => 'side',
        'post_types' => array( 'mar_empreendimentos' ),
        'fields'     => array(
        
            array(
                'id'        => "emp_destaque",
                'name'      => 'Destaque',
                'type'      => 'radio',
                'options'   => array(1 => 'Sim', 0 => 'Não'),
                'std'       => 0,
                'admin_columns' => 'after title',
            ),
            
        ),
    );

    
    $meta_boxes[] = array(
        'id'             => 'empd_group',
        'title'          => 'Info do Empreendimento',
        'context'        => 'side',
        'post_types' => array( 'empresas' ),
        'fields'     => array(
        
            array(
                'name'      => 'Link',
                'id'        => "empd_url",
                'type'      => 'testearea',
            ),
            array(
                'name'       => 'Target do Link',
                'id'         => "empd_target",
                'type'       => 'radio',
                'options'    => array('_self' => 'Interno', '_blank' => 'Externo'),
                'std'        => '_self',
               
                'admin_columns' => 'after title',
            ),

        ),
    );

    //=========================================================================================
    // END DEFINITION OF META BOXES
    //=========================================================================================
    return $meta_boxes;
}
