<?php
/*
* Thumbnail functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// ADICIONANDO TAMANHHO DE IMAGENS
//=========================================================================================

if (function_exists('add_image_size')) {
    add_image_size('thumb-565x350', 565, 350, true );
    add_image_size('thumb-galeria', 1200, 500, true );
    add_image_size('thumb-home-galeria', 595, 260, true );
    add_image_size('thumb-emp', 345, 425, true );
}

//=========================================================================================
// RESETA TAMANHHO DE IMAGENS
//=========================================================================================

add_filter('intermediate_image_sizes','get_sizes');
function get_sizes ($sizes){
    $type = get_post_type($_REQUEST['post_id']);
    foreach($sizes as $key => $value){
        //Gera o tamanho full para o slide
        if($type=='slides'  &&  $value != 'full' && $value != 'thumb-385x180'){
            unset($sizes[$key]);
        }
        if($type=='page'  &&  $value != 'full'){unset($sizes[$key]);} //Gera o tamanhho full para pages
    }
    return $sizes;
}

//=========================================================================================
// FIRST IMAGE THUMB
//=========================================================================================

function get_thumb_image($lazy) {
  global $post, $posts;

  $src_img = '';
  ob_start();
  ob_end_clean();

  if(has_post_thumbnail($post->ID)){
      $src_img = get_the_post_thumbnail_url($post->ID, 'full');
  } else {
      $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
      $src_img = $matches [1] [0];

      if(empty($src_img)){ //Defines a default image
        $src_img = get_template_directory_uri()."/assets/images/noimage.jpg";
      }
  }

    if($lazy == true) :
        echo '<img class="b-lazy fade" data-src="'.$src_img.'" alt="'.get_the_title().'" title="'.get_the_title().'" />';
        echo '<noscript><img src="'.$src_img.'" alt="'.get_the_title().'" title="'.get_the_title().'"></noscript>';
    else:
        echo '<img src="'.$src_img.'" alt="'.get_the_title().'" title="'.get_the_title().'" />';
    endif;
}

// //=========================================================================================
// // LAZY LOAD FUNCTIONS
// //=========================================================================================

// function imglazy($post, $size, $efeito, $alt ) {

//     $thumb = wp_get_attachment_image_src( $post, $size );
//     $url = $thumb['0'];
//     echo '<img class="b-lazy '.$efeito.'" data-src="'.$url.'" alt="'.$alt.'" title="'.$alt.'">';
//     echo '<noscript><img src="'.$url.'" alt="'.$alt.'" title="'.$alt.'"></noscript>';
// }

// function thumblazy($post, $size, $efeito, $alt ) {

//     $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post), $size);
//     $url = $thumb['0'];
//     echo '<img class="b-lazy '.$efeito.'" data-src="'.$url.'" alt="'.$alt.'" title="'.$alt.'">';
//     echo '<noscript><img src="'.$url.'" alt="'.$alt.'" title="'.$alt.'"></noscript>';
// }

// function lazyimage( $atts ) {
//     // Attributes
//     extract( shortcode_atts(
//         array(
//             'post'   => '',
//             'size'   => '',
//             'efeito' => 'fade',
//             'alt'    => '',
//             'tipo'   => 'thumb'
//         ), $atts)
//     );

//     if(empty($post)) return false;
//     if($tipo == 'attachment') {
//         imglazy($post, $size, $efeito, $alt);
//     } else {
//         thumblazy($post, $size, $efeito, $alt);
//     }

// }
// add_shortcode( 'lazy', 'lazyimage');



//=========================================================================================
// LAZY LOAD FUNCTIONS
//=========================================================================================

function imglazy($post, $size, $efeito, $alt ) {
    $thumb = wp_get_attachment_image_src( $post, $size );
    $url = $thumb['0'];
    echo '<img class="b-lazy '.$efeito.'" data-src="'.$url.'" alt="'.$alt.'" title="'.$alt.'">';
    echo '<noscript><img src="'.$url.'" alt="'.$alt.'" title="'.$alt.'"></noscript>';
}

function thumblazy($post, $size, $efeito, $alt ) {
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post), $size);
    $url = $thumb['0'];

    if ( has_post_thumbnail($post) ) :
        echo '<img class="b-lazy '.$efeito.'" data-src="'.$url.'" width="'.$thumb['1'].'" height="'.$thumb['2'].'" alt="'.$alt.'" title="'.$alt.'">';
        echo '<noscript><img src="'.$url.'" alt="'.$alt.'" width="'.$thumb['1'].'" height="'.$thumb['2'].'" title="'.$alt.'"></noscript>';
    else :
        echo '<img src="'.get_template_directory_uri().'/assets/images/no-thumbs.jpg" />';
    endif;
}

function lazyimage( $atts ) {
    // Attributes
    extract( shortcode_atts(
        array(
            'post'   => '',
            'size'   => '',
            'efeito' => 'fade',
            'alt'    => '',
            'tipo'   => 'thumb'
        ), $atts)
    );

    if(empty($post)) return false;
    if($tipo == 'attachment') {
        imglazy($post, $size, $efeito, $alt);
    } else {
        thumblazy($post, $size, $efeito, $alt);
    }
}
add_shortcode( 'lazy', 'lazyimage');