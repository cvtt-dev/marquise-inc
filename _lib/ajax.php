<?php
/*
* AJAX functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/


//=========================================================================================
// GET UF/CIDADES
//=========================================================================================

function getcidadesAjax() {

    global $wpdb;
    $tabela  = $wpdb->prefix."ajaxcidade";
    $estados = $_POST['estado'];

    if($estados == '') : echo '<option value="">Cidade</option>'; else :
        $myrows  = $wpdb->get_results( "SELECT * FROM $tabela WHERE estados_cod_estados=$estados GROUP BY nome ORDER BY nome");

        // print_r($myrows);

        echo '<option value="" selected="selected">Cidade</option>';
        foreach ($myrows as $row) {
            echo '<option value="'.$row->nome.'">'.$row->nome.'</option>';
        }
    endif;
    exit;
}

add_action('wp_ajax_getcidades', 'getcidadesAjax');
add_action('wp_ajax_nopriv_getcidades', 'getcidadesAjax');


define('UPDATE_AJAX_URL', trailingslashit( get_stylesheet_directory_uri() . '/_lib'));
define('UPDATE_AJAX_DIR', trailingslashit( STYLESHEETPATH . '/_lib'));
require_once UPDATE_AJAX_DIR . 'sendmail.php';

function send_reset_password() {

    $sm = new sendmail_wp();

   
    $email      = $_POST['email'];
  
 
    if( !$email ) :

        echo json_encode(
            array('rsp' => 2)
        );

    else :

        echo json_encode(
          array('rsp' => $sm->send_mail_reset( $email ))
        );

    endif;

    exit;
}

add_action('wp_ajax_send_reset_password', 'send_reset_password');
add_action('wp_ajax_nopriv_send_reset_password', 'send_reset_password');