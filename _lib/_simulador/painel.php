<?php //Custom Painel
class CustomPanel {
    /**
    * Holds the values to be used in the fields callbacks
    */
    private $options;

    /**
    * Start up
    */
    public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_plugin_page'));
        add_action( 'admin_init', array( $this, 'panel_assets'));
        add_action( 'admin_init', array( $this, 'remove_menus'));
        // add_action( 'admin_init', array( $this, 'redirects'));

        if(!current_user_can('delete_plugins')) {
            add_action( "in_admin_header", array( $this, 'removeNotifications' ) );
        }
        // add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    // public function redirects() {}
    /**
    * Add options page
    */
    public function add_plugin_page() {
        // This page will be under "Settings"
        //add_options_page(
        add_menu_page(
            'Simulações Marquise',
            'Simulações',
            'administrator',
            'painel-de-controle',
            array( $this, 'create_admin_page' )
        );
    }

    function remove_menus() {
        global $current_user;
        if($_GET['page'] == 'painel-de-controle') :
            remove_menu_page( 'index.php' );                  //Dashboard
            remove_menu_page( 'jetpack' );                    //Jetpack*
            remove_menu_page( 'edit.php' );                   //Posts
            remove_menu_page( 'upload.php' );                 //Media
            remove_menu_page( 'edit.php?post_type=page' );    //Pages
            remove_menu_page( 'edit-comments.php' );          //Comments
            remove_menu_page( 'themes.php' );                 //Appearance
            remove_menu_page( 'plugins.php' );                //Plugins
            remove_menu_page( 'users.php' );                  //Users
            remove_menu_page( 'tools.php' );                  //Tools
            remove_menu_page( 'options-general.php' );        //Settings
        endif;
    }

    /**
    * css, js, images
    */
    public function panel_assets() {
        global $current_user;
        if($_GET['page'] == 'painel-de-controle') :
            add_thickbox();
            wp_register_style('main-css', get_stylesheet_directory_uri() . '/_lib/_simulador/css/main.min.css');
            wp_register_script('main-js', get_stylesheet_directory_uri() . '/_lib/_simulador/js/main.min.js');
            wp_register_script('checkout', get_stylesheet_directory_uri() . '/_lib/_simulador/js/checkout.js');

            wp_enqueue_style('main-css');
            wp_enqueue_script('main-js');
        endif;
    }

    /**
    * Options page callback
    */
    public function create_admin_page() {
        $this->options = get_option( 'my_option_name' );
        include dirname(__FILE__) . '/tpl/painel-de-controle.php';
    }

    public function removeNotifications() {
        global $wp_filter;
        unset($wp_filter['network_admin_notices']);
        unset($wp_filter['user_admin_notices']);
        unset($wp_filter['admin_notices']);
        unset($wp_filter['all_admin_notices']);
    }
}

if( is_admin() ) {
    $my_settings_page = new CustomPanel();
}