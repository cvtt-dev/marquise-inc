<div class="msg">
<h4> <?php echo $msg; ?> </h4>
</div>
<?php $cor = '#ee4e9a'; ?>
<header class="topo">
    <div class="wrapper">
        <h1 class="logo-admin"><a href="<?php echo get_option("home") ?>/wp-admin/admin.php?page=painel-de-controle" class="logo">Marquise Incorporações</a></h1>
        <nav class="box-menu">
            <h2 class="header-titulo -right">Simulações</h2>
            <ul>
                <li><a href="<?php echo site_url().'/wp-admin';?>" class="link">Voltar para painel</a></li>
                <li><a href="<?php echo wp_logout_url( get_option('home') ); ?>" class="link">Sair</a></li>
            </ul>
        </nav>
    </div>
</header>

<section class="container">
    <div class="wrapper">

          <div class="palco" style="width:100%;">
            <div class="bloco">
                <?php
                //conecta com banco
                try {
                    $hostname = "localhost";
                    $dbname   = "marquise_marquiseincorp";
                    $username = "marquise_incorp";
                    $pw       = "XcrL5QKndvOk";
                    $pdo = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
                } catch (PDOException $e) {
                    echo "Erro de Conexão " . $e->getMessage() . "\n";
                    exit;
                }

                $qtd   = '0';
                $tr    = '';
                $sql   = "SELECT * FROM simulacoes ORDER BY data_cadastro DESC";
                $query = $pdo->prepare($sql);
                $query->execute();

                //monta tabela
                foreach( $query as $row ){
                    $qtd++;
                    $createDate = new DateTime($row['data_cadastro']);
                    $cadastro   = $createDate->format('d/m/Y');
                    $matricula  = ($row['matricula'] == '' ? '-' : $row['matricula']);

                    $tr .= '
                    <tr>
                        <td style="width: 20%; padding: 10px;">'.$row['nome'].'</td>
                        <td style="width: 20%; padding: 10px;">'.$row['email'].'</td>
                        <td style="width: 13%; padding: 10px;">'.$row['telefone'].'</td>
                        <td style="width: 12%; padding: 10px;">'.$row['cpf'].'</td>
                        <td style="width: 10%; padding: 10px;">'.$row['perfil'].'</td>
                        <td style="width: 5%; padding: 10px;">'.$matricula.'</td>
                        <td style="width: 10%; padding: 10px;">'.$row['renda'].'</td>
                        <td style="width: 10%; padding: 10px; text-align: right;">'.$cadastro.'</td>
                    </tr>';

                }

                unset($pdo);
                unset($query);
                ?>
                <h3 class="titulo"><?php echo ($qtd != 1 ? $qtd." Simulações Realizadas" : $qtd." Simulação Realizada");?></h3>
                <table>
                <thead>
                    <th style="width: 20%;">Nome</th>
                    <th style="width: 20%;">E-mail</th>
                    <th style="width: 13%;">Telefone</th>
                    <th style="width: 12%;">CPF</th>
                    <th style="width: 10%;">Perfil</th>
                    <th style="width: 5%;">Matricula</th>
                    <th style="width: 10%;">Renda</th>
                    <th style="width: 10%; text-align: right;">Simulado em</th>
                </thead>
                <tbody>
                    <?php echo $tr;?>
                </tbody>
                </table>
            </div>
        </div>

        <!-- <div class="col">
            <div class="box">
                <h3 class="titulo">Suporte | Dúvidas</h3>
                <div class="desc">
                    Se você tiver qualquer dúvida, entre em <a target="_blank" href="http://www.convertte.com.br/contato">contato</a> conosco em  ou mande um e-mail para contato@convertte.com.br.
                </div>

                <span class="box-info"><b>Telefone:</b> (85) 3065.7273</span>
                <span class="box-info"><b>WhatsApp:</b> (85) 9814.2286</span>
            </div>
        </div> -->
    </div>
</section>
