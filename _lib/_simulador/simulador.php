<?php

/////////////////////////////
//REST API
////////////////////////////

//IMAGE

add_action( 'rest_api_init', 'insert_thumbnail_url' );
function insert_thumbnail_url() {
    register_rest_field( 'mar_empreendimentos',
    'featured_image',  //key-name in json response
        array(
            'get_callback'    => 'get_thumbnail_url',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_thumbnail_url($post){
    if(has_post_thumbnail($post['id'])){
        $imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post['id'] ), 'home-empreendimento' ); // replace 'full' with 'thumbnail' to get a thumbnail
        $imgURL = $imgArray[0];
        return $imgURL;
    } else {
        return false;
    }
}

 //POSTTYPE
add_action( 'init', 'post_type_rest_support', 25 );
function post_type_rest_support() {
    global $wp_post_types;

    //be sure to set this to the name of your post type!
    $post_type_name = 'mar_empreendimentos';
    if( isset( $wp_post_types[ $post_type_name ] ) ) {
        $wp_post_types[$post_type_name]->show_in_rest = true;
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
    }
}

//TAXONOMY

add_action( 'init', 'wp_rest_api_tax');
function wp_rest_api_tax() {
    register_rest_field( 'mar_empreendimentos',
        'status_emp',
        array(
            'get_callback'    => 'get_taxonomies_rest_api',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_taxonomies_rest_api($object) {
    $status = get_the_terms($object['id'], 'status-empreendimentos');
    return $status[0]->name;
}

//METAKEY
add_action( 'rest_api_init', 'meta_faixa_renda' );
function meta_faixa_renda() {
    register_rest_field( 'mar_empreendimentos',
        'wpcf_faixa_renda',
        array(
            'get_callback'    => 'get_renda',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

add_action( 'rest_api_init', 'meta_desconto_renda' );
function meta_desconto_renda() {
    register_rest_field( 'mar_empreendimentos',
        'wpcf_desconto_renda',
        array(
            'get_callback'    => 'get_renda',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_renda( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], $field_name, true );
    // $post_views = carbon_get_the_post_meta('_crb_post_number_of_views');
}

add_action( 'rest_api_init', 'meta_caracteristicas' );
function meta_caracteristicas() {
    register_rest_field( 'mar_empreendimentos',
        'empreendimento_caracteristicas',
        array(
            'get_callback'    => 'get_caracteristicas',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_caracteristicas( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], $field_name, true );
    // $post_views = carbon_get_the_post_meta('_crb_post_number_of_views');
}